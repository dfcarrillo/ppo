'use strict';

module.exports = function(Programa) {

    var server = require('../../server/server');
    var ds = server.datasources.mysql;

    Programa.exec = function(id, cb) {
        var stmt = 'CALL GET_all_Programa('+ id +');';
        ds.connector.execute(stmt, function (err, data) {
            if(err) return err;
            console.log(err);
            console.log("data", data);
            cb(null, data[0]);
        });
    };

    Programa.remoteMethod(
        'exec',
        {
            accepts: [{arg: 'id', type: 'number'}],
            returns: [{arg: 'data', type: ['Programa']}],
            http: {path: '/exec', verb: 'post'},
            description: 'Get programa by id'
        }
    );

};
