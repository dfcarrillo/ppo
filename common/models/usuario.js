'use strict';

module.exports = function(Usuario) {

  Usuario.idrol = function (user, passwd, cb) {

    var filter = {
      where:
        {
          user: user,
          passwd: passwd
        }
    };
    Usuario.findOne(filter, function (err, instance){
      if(instance == null){
        cb(null, "Login Invalido", "Usuario o clave incorrectos");
      }
      else{
        var response_id = instance.id;
        var response_id_rol = instance.id_rol;
        cb(null, response_id,response_id_rol);
      }
    });
  };

  Usuario.remoteMethod(
    'idrol',
    {
      http: {
        path: '/idrol',
        verb: 'post'
      },
      description:'Login, recibe user y passwd, si OK retorna id_rol',
      accepts: [
        {
          arg: 'user',
          type: 'string',
          description: 'Nombre de Usuario'
        },
        {
          arg: 'passwd',
          type: 'string',
          description: 'Clave del usuario'
        }
      ],
      returns:[
        {
          arg: 'id',
          type: 'number'
        },
        {
          arg: 'id_rol',
          type: 'number'
        }
      ]
    }
  );
};
