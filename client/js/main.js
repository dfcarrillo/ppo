angular.module('app',
  ['ui.router',
    'app.controllers',
    'app.directives',
    'app.services',
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.pinning',
    'ui.grid.moveColumns',
    'ui.grid.exporter',
    'ui.grid.selection',
    'ui.grid.cellNav',
    'ui.grid.edit',
    'moment-picker',
  ])

.constant('endpoint', {
    API: "http://localhost:8080/api"
})


angular.module('app')

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('home', {
        url: '/',
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl',
      })
      .state('perfil_admin', {
        url: '/admin',
        templateUrl: 'templates/admin/home_admin.html',
        controller: 'homeAdminCtrl',
      })
      .state('perfil_admin_usuarios', {
        url: '/admin/usuarios',
        templateUrl: 'templates/admin/usuarios_admin.html',
        controller: 'adminUsuariosCtrl',
      })
      .state('perfil_admin_proyectos_cerrados', {
        url: '/admin/obrascerrados',
        templateUrl: 'templates/admin/proyectos_cerrados.html',
        controller: 'adminProyectosCerrados',
      })
      .state('perfil_admin_backlog', {
        url: '/admin/backlog/:id',
        templateUrl: 'templates/admin/admin_backlog.html',
        controller: 'adminBacklogCtrl',
      })
      .state('cambiar_passwd', {
        url: '/cambiar_contrasena?access_token',
        templateUrl: 'templates/admin/admin_reset_pass.html',
        controller: 'adminResetPassCtrl',
      })
      .state('perfil_cdc', {
        url: '/cdc',
        templateUrl: 'templates/cdc/cdc_home.html',
        controller: 'cdcHomeCtrl',
      })
      .state('perfil_cdc_crear', {
        url: '/cdc/CrearProyectos',
        templateUrl: 'templates/cdc/cdc_crear.html',
        controller: 'cdcCrearCtrl',
      })
      .state('perfil_cdc_asignar', {
        url: '/cdc/AsignarProyectos',
        templateUrl: 'templates/cdc/cdc_asignar.html',
        controller: 'cdcAsignarCtrl',
      })
      .state('perfil_cdc_editar', {
        url: '/cdc/EditarProyectos',
        templateUrl: 'templates/cdc/cdc_editar.html',
        controller: 'cdcEditarCtrl',
      })
      .state('perfil_js', {
        url: '/ja',
        templateUrl: 'templates/ja/ja_home.html',
        controller: 'jsHomeCtrl',
      })
      .state('perfil_js_propuesta', {
        url: '/ja/propuesta',
        templateUrl: 'templates/ja/ja_propuesta.html',
        controller: 'jsPropuestaCtrl',
      })
      .state('perfil_js_asignaGP', {
        url: '/ja/asignarGP',
        templateUrl: 'templates/ja/ja_asignaGP.html',
        controller: 'jsAsignarGPCtrl',
      })
      .state('perfil_js_cambiaGP', {
        url: '/ja/cambiarGP',
        templateUrl: 'templates/ja/ja_cambiaGP.html',
        controller: 'jsCambiarGPCtrl',
      })
      .state('perfil_js_trabajos', {
        url: '/ja/trabajos',
        templateUrl: 'templates/ja/ja_asigna_trabajos.html',
        controller: 'jsAsignaTrabajosCtrl',
      })
      .state('perfil_js_obras', {
        url: '/ja/obras',
        templateUrl: 'templates/ja/ja_detalle_obra.html',
        controller: 'jsDetalleObrasCtrl',
      })
      .state('perfil_js_compromiso', {
        url: '/ja/compromisos',
        templateUrl: 'templates/ja/ja_crea_compromiso.html',
        controller: 'jsCompromisoCtrl',
      })
      .state('perfil_js_valida', {
        url: '/ja/validar',
        templateUrl: 'templates/ja/ja_valida_compromiso.html',
        controller: 'jsValidarCtrl',
      })
      .state('perfil_gp', {
        url: '/gp',
        templateUrl: 'templates/gp/gestion_gp.html',
        controller: 'gestionGpCtrl',
      })
      .state('perfil_ec', {
        url: '/ec',
        templateUrl: 'templates/ec/gestion_ec.html',
        controller: 'gestionEcCtrl',
      })
      .state('programador_ec', {
          url: '/ec/programador',
          templateUrl: 'templates/ec/programador_ec.html',
          controller: 'programarEcCtrl'
      })
      .state('programador_cdc', {
          url: '/cdc/programador',
          templateUrl: 'templates/cdc/programador_cdc.html',
          controller: 'programarCdcCtrl'
      })
      .state('backlog', {
        url: '/backlog/:id',
        templateUrl: 'templates/general/backlog.html',
        controller: 'backlogCtrl',
      });

    $urlRouterProvider.otherwise('/');
  });

angular.module('app.directives', [])

  .directive('footerHome', [function() {
    return {
      templateUrl: 'templates/footer.html',
    };
  }])

  .directive('headerAdmin', [function() {
    return {
      templateUrl: 'templates/admin/adminHeader.html',
    };
  }])

  .directive('headerCdc', [function() {
    return {
      templateUrl: 'templates/cdc/header_cdc.html',
    };
  }])

  .directive('headerJs', [function() {
    return {
      templateUrl: 'templates/ja/ja_header.html',
    };
  }])

  .directive('contratistaHeader', [function () {
    return {
      templateUrl: 'templates/ec/header_contratista.html'
    };
  }])

  .directive("datepicker", function () {

      function link(scope, element) {
          element.datepicker({
              dateFormat: "yy-mm-dd"
          });
      }
      return {
          require: 'ngModel',
          link: link
      };
  })

  .directive('fileread', [function() {
    return {
      scope: {
        opts: '=',
      },
      link: function($scope, $elm, $attrs) {
        $elm.on('change', function(changeEvent) {
          var reader = new FileReader();

          reader.onload = function(evt) {
            $scope.$apply(function() {
              var data = evt.target.result;

              var workbook = XLSX.read(data, {type: 'binary'});

              workbook.Sheets["Programacion"]['!ref'] = "C20:ET250"; //Formato zona Metropolitana
              workbook.Sheets["Seguimiento"]['!ref'] = "F8:FS250"; // Formato zona metropolitana

              var headerNames = ['Supervisor', 'x', 'Area_servicio', 'x', 'x', 'x', 'Zona', 'x', 'x', 'Codigo',
                'Nombre', 'x', 'x', 'Fecha_Fin', 'x', 'x', 'x', 'x', 'Comentarios', 'x', 'x', 'x', 'x',
                'uucc', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'Sem_Ini', 'Sem_Fin', 'ec1', 'x', 'ec2', 'x',
                'x', 'x', 'x', 'Estado', 'x', 'x', 'x', 'ec1s1', 'ec2s1', 'ec1s2', 'ec2s2',
                'ec1s3', 'ec2s3', 'ec1s4', 'ec2s4', 'ec1s5', 'ec2s5', 'ec1s6', 'ec2s6', 'ec1s7', 'ec2s7',
                'ec1s8', 'ec2s8', 'ec1s9', 'ec2s9', 'ec1s10', 'ec2s10', 'ec1s11', 'ec2s11', 'ec1s12', 'ec2s12',
                'ec1s13', 'ec2s13', 'ec1s14', 'ec2s14', 'ec1s15', 'ec2s15', 'ec1s16', 'ec2s16', 'ec1s17', 'ec2s17',
                'ec1s18', 'ec2s18', 'ec1s19', 'ec2s19', 'ec1s20', 'ec2s20', 'ec1s21', 'ec2s21', 'ec1s22', 'ec2s22',
                'ec1s23', 'ec2s23', 'ec1s24', 'ec2s24', 'ec1s25', 'ec2s25', 'ec1s26', 'ec2s26', 'ec1s27', 'ec2s27',
                'ec1s28', 'ec2s28', 'ec1s29', 'ec2s29', 'ec1s30', 'ec2s30', 'ec1s31', 'ec2s31', 'ec1s32', 'ec2s32',
                'ec1s33', 'ec2s33', 'ec1s34', 'ec2s34', 'ec1s35', 'ec2s35', 'ec1s36', 'ec2s36', 'ec1s37', 'ec2s37',
                'ec1s38', 'ec2s38', 'ec1s39', 'ec2s39', 'ec1s40', 'ec2s40', 'ec1s41', 'ec2s41', 'ec1s42', 'ec2s42',
                'ec1s43', 'ec2s43', 'ec1s44', 'ec2s44', 'ec1s45', 'ec2s45', 'ec1s46', 'ec2s46', 'ec1s47', 'ec2s47',
                'ec1s48', 'ec2s48', 'ec1s49', 'ec2s49', 'ec1s50', 'ec2s50', 'ec1s51', 'ec2s51', 'ec1s52', 'ec2s52'];

              var headersUucc = ['codigo_SAP', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
                'uc1', 'ruc1', 'x', 'uc2', 'ruc2', 'x', 'uc3', 'ruc3', 'x', 'uc4', 'ruc4', 'x', 'uc5', 'ruc5', 'x',
                'uc6', 'ruc6', 'x', 'uc7', 'ruc7', 'x', 'uc8', 'ruc8', 'x', 'uc9', 'ruc9', 'x', 'uc10', 'ruc10', 'x',
                'uc11', 'ruc11', 'x', 'uc12', 'ruc12', 'x', 'uc13', 'ruc13', 'x', 'uc14', 'ruc14', 'x', 'uc15', 'ruc15',
                'x', 'uc16', 'ruc16', 'x', 'uc17', 'ruc17', 'x', 'uc18', 'ruc18', 'x', 'uc19', 'ruc19', 'x', 'uc20',
                'ruc20', 'x', 'uc21', 'ruc21', 'x', 'uc22', 'ruc22', 'x', 'uc23', 'ruc23', 'x', 'uc24', 'ruc24', 'x',
                'uc25', 'ruc25', 'x', 'uc26', 'ruc26', 'x', 'uc27', 'ruc27', 'x', 'uc28', 'ruc28', 'x', 'uc29', 'ruc29',
                'x', 'uc30', 'ruc30', 'x', 'uc31', 'ruc31', 'x', 'uc32', 'ruc32', 'x', 'uc33', 'ruc33', 'x', 'uc34',
                'ruc34', 'x', 'uc35', 'ruc35', 'x', 'uc36', 'ruc36', 'x', 'uc37', 'ruc37', 'x', 'uc38', 'ruc38', 'x',
                'uc39', 'ruc39', 'x', 'uc40', 'ruc40', 'x', 'uc41', 'ruc41', 'x', 'uc42', 'ruc42', 'x', 'uc43', 'ruc43',
                'x', 'uc44', 'ruc44', 'x', 'uc45', 'ruc45', 'x', 'uc46', 'ruc46', 'x', 'uc47', 'ruc47', 'x', 'uc48',
                'ruc48', 'x', 'uc49', 'ruc49', 'x', 'uc50', 'ruc50', 'x', 'uc51', 'ruc51', 'x', 'uc52', 'ruc52', 'x'];

              //Formato zona Bio Bio
              /*workbook.Sheets["Programacion"]['!ref'] = "B21:EO250";
              workbook.Sheets["Seguimiento"]['!ref'] = "F8:FS250";

                var headerNames = ['Supervisor', 'x', 'Area_servicio', 'x', 'x', 'x', 'Zona', 'x', 'x', 'Codigo',
                    'Nombre', 'x', 'x', 'Fecha_Fin', 'x', 'x', 'x', 'x', 'Comentarios', 'x', 'x', 'x', 'x',
                    'uucc', 'x', 'x', 'x', 'x', 'x', 'Sem_Ini', 'Sem_Fin', 'ec1', 'x', 'ec2', 'x',
                    'Estado', 'x', 'x', 'x', 'x', 'ec1s1', 'ec2s1', 'ec1s2', 'ec2s2',
                    'ec1s3', 'ec2s3', 'ec1s4', 'ec2s4', 'ec1s5', 'ec2s5', 'ec1s6', 'ec2s6', 'ec1s7', 'ec2s7',
                    'ec1s8', 'ec2s8', 'ec1s9', 'ec2s9', 'ec1s10', 'ec2s10', 'ec1s11', 'ec2s11', 'ec1s12', 'ec2s12',
                    'ec1s13', 'ec2s13', 'ec1s14', 'ec2s14', 'ec1s15', 'ec2s15', 'ec1s16', 'ec2s16', 'ec1s17', 'ec2s17',
                    'ec1s18', 'ec2s18', 'ec1s19', 'ec2s19', 'ec1s20', 'ec2s20', 'ec1s21', 'ec2s21', 'ec1s22', 'ec2s22',
                    'ec1s23', 'ec2s23', 'ec1s24', 'ec2s24', 'ec1s25', 'ec2s25', 'ec1s26', 'ec2s26', 'ec1s27', 'ec2s27',
                    'ec1s28', 'ec2s28', 'ec1s29', 'ec2s29', 'ec1s30', 'ec2s30', 'ec1s31', 'ec2s31', 'ec1s32', 'ec2s32',
                    'ec1s33', 'ec2s33', 'ec1s34', 'ec2s34', 'ec1s35', 'ec2s35', 'ec1s36', 'ec2s36', 'ec1s37', 'ec2s37',
                    'ec1s38', 'ec2s38', 'ec1s39', 'ec2s39', 'ec1s40', 'ec2s40', 'ec1s41', 'ec2s41', 'ec1s42', 'ec2s42',
                    'ec1s43', 'ec2s43', 'ec1s44', 'ec2s44', 'ec1s45', 'ec2s45', 'ec1s46', 'ec2s46', 'ec1s47', 'ec2s47',
                    'ec1s48', 'ec2s48', 'ec1s49', 'ec2s49', 'ec1s50', 'ec2s50', 'ec1s51', 'ec2s51', 'ec1s52', 'ec2s52'];

                var headersUucc = ['codigo_SAP', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
                    'uc1', 'ruc1', 'x', 'uc2', 'ruc2', 'x', 'uc3', 'ruc3', 'x', 'uc4', 'ruc4', 'x', 'uc5', 'ruc5', 'x',
                    'uc6', 'ruc6', 'x', 'uc7', 'ruc7', 'x', 'uc8', 'ruc8', 'x', 'uc9', 'ruc9', 'x', 'uc10', 'ruc10', 'x',
                    'uc11', 'ruc11', 'x', 'uc12', 'ruc12', 'x', 'uc13', 'ruc13', 'x', 'uc14', 'ruc14', 'x', 'uc15', 'ruc15',
                    'x', 'uc16', 'ruc16', 'x', 'uc17', 'ruc17', 'x', 'uc18', 'ruc18', 'x', 'uc19', 'ruc19', 'x', 'uc20',
                    'ruc20', 'x', 'uc21', 'ruc21', 'x', 'uc22', 'ruc22', 'x', 'uc23', 'ruc23', 'x', 'uc24', 'ruc24', 'x',
                    'uc25', 'ruc25', 'x', 'uc26', 'ruc26', 'x', 'uc27', 'ruc27', 'x', 'uc28', 'ruc28', 'x', 'uc29', 'ruc29',
                    'x', 'uc30', 'ruc30', 'x', 'uc31', 'ruc31', 'x', 'uc32', 'ruc32', 'x', 'uc33', 'ruc33', 'x', 'uc34',
                    'ruc34', 'x', 'uc35', 'ruc35', 'x', 'uc36', 'ruc36', 'x', 'uc37', 'ruc37', 'x', 'uc38', 'ruc38', 'x',
                    'uc39', 'ruc39', 'x', 'uc40', 'ruc40', 'x', 'uc41', 'ruc41', 'x', 'uc42', 'ruc42', 'x', 'uc43', 'ruc43',
                    'x', 'uc44', 'ruc44', 'x', 'uc45', 'ruc45', 'x', 'uc46', 'ruc46', 'x', 'uc47', 'ruc47', 'x', 'uc48',
                    'ruc48', 'x', 'uc49', 'ruc49', 'x', 'uc50', 'ruc50', 'x', 'uc51', 'ruc51', 'x', 'uc52', 'ruc52', 'x'];*/

              var data = XLSX.utils.sheet_to_json(workbook.Sheets["Programacion"],  {header: headerNames});
              var uucc = XLSX.utils.sheet_to_json(workbook.Sheets["Seguimiento"],  {header: headersUucc});

              //data.forEach(function(v) { delete v.x; });

              //data = _.filter(data, {'Estado': 'PROGRAMADO'}) //BIO BIO
              data = _.filter(data, {'Estado': 'Programación_completa'}) // San Bernardo
              data = _.filter(data, function(s) { return s.Sem_Fin >= (moment().week() - 1); });
              //data = _.filter(data, function(s) { return s.Sem_Ini <= /*(moment().week() - 1)*/26; });

              $scope.opts.data = data;
              $scope.opts.data.uucc = uucc;

              $elm.val(null);
            });
          };

          reader.readAsBinaryString(changeEvent.target.files[0]);
        });
      },
    };
  }]);


angular.module('app.controllers', [])

.controller('homeCtrl', ['$scope', '$state', '$stateParams', 'homeServ', 'usuariosServ', '$http',
  function($scope, $state, $stateParams, homeServ, usuariosServ){

    $scope.login= function(user, passwd){
      usuariosServ.login(user, passwd)
        .then(loginSuccess, loginError)
    };

    function loginError(res){
        var $toastContent = $('<span>La cuenta o la contraseña es incorrecta.</span>');
        Materialize.toast($toastContent, 3000);
    }

    function loginSuccess(res){
        $scope.id_user = res.data.userId;
        $scope.token = res.data.id;
        window.localStorage.setItem('token', $scope.token);
        usuariosServ.getUsuarioById($scope.id_user)
            .then(setPerfil);
    }

    function setPerfil(res) {
      $scope.role = res.data.role;
      window.localStorage.setItem('id_user', $scope.id_user);
      window.localStorage.setItem('id_locacion', res.data.id_locacion);
      if($scope.role.name == 'admin'){
        $state.go('perfil_admin');
        window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'cdc'){
        $state.go('perfil_cdc');
        window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'js'){
          $state.go('perfil_js');
          window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'gp'){
          $state.go('perfil_gp');
          window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'eecc'){
          $state.go('perfil_ec');
          window.localStorage.setItem('rol', $scope.role.name);
      }
    }

    /*function loadRoles(){
      $http.get('files/data.json')
        .then(function(res){
          homeServ.addModel("roles", res.data.roles)
            .then(function(res){
              console.log("http code Roles" + res.status);
            });

          homeServ.addModel("usuarios", res.data.users)
            .then(function(res){
              console.log("http code Users" + res.status);
            })
        })
    }

    loadRoles();*/
  }
])

angular.module('app.services', [])

.factory('backlogsServ', function($http){
    var url = "/api/backlogs";

    function getBacklogs() {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_b = url + '?filter={"where":{"id_backlog":"0"},"include":["sons"]}'
        return $http.get(url_b,header);
    }

    function getBacklogsById(id) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_b = url + '/' + id + '?filter={"include":["sons","user"]}'
        return $http.get(url_b,header);
    }

    function postBacklogs(backlog) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        return $http.post(url, backlog, header);
    }

    function putBacklogs(backlog) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        return $http.put(url, backlog, header);
    }

    function deleteBacklog(id) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_d = url + '/' + id;
        return $http.delete(url_d, header);
    }

    function deleteSonsBacklogs(id) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_d = url + '/' + id + '/sons';
        return $http.delete(url_d, header);
    }

    return {
        getBacklogs: getBacklogs,
        getBacklogsById: getBacklogsById,
        postBacklogs: postBacklogs,
        putBacklogs: putBacklogs,
        deleteBacklog: deleteBacklog,
        deleteSonsBacklogs: deleteSonsBacklogs
    }
})

.factory('generalFunc', ['$state', 'usuariosServ',
    function($state, usuariosServ){

        //-------------------------Funciones Generales Retornos sincronos---------------------------//

        function routeAdmin() {
            if (window.localStorage['rol'] == "admin") {
                //$state.go("perfil_admin");
            } else if (window.localStorage['rol'] == 'cdc') {
                $state.go("perfil_cdc");
            } else if (window.localStorage['rol'] == 'js') {
                $state.go("perfil_js");
            } else if (window.localStorage['rol'] == 'gp'){
                $state.go("perfil_gp");
            } else if (window.localStorage['rol'] == 'eecc'){
                $state.go("perfil_ec");
            } else {
                $state.go("home");
            }
        }

        function routeCdc() {
            if (window.localStorage['rol'] == "admin") {
                $state.go("perfil_admin");
            } else if (window.localStorage['rol'] == 'cdc') {
                //$state.go("perfil_cdc");
            } else if (window.localStorage['rol'] == 'js') {
                $state.go("perfil_js");
            } else if (window.localStorage['rol'] == 'gp'){
                $state.go("perfil_gp");
            } else if (window.localStorage['rol'] == 'eecc'){
                $state.go("perfil_ec");
            } else {
                $state.go("home");
            }
        }

        function routeJs() {
            if (window.localStorage['rol'] == "admin") {
                $state.go("perfil_admin");
            } else if (window.localStorage['rol'] == 'cdc') {
                $state.go("perfil_cdc");
            } else if (window.localStorage['rol'] == 'js') {
                //$state.go("perfil_js");
            } else if (window.localStorage['rol'] == 'gp'){
                $state.go("perfil_gp");
            } else if (window.localStorage['rol'] == 'eecc'){
                $state.go("perfil_ec");
            } else {
                $state.go("home");
            }
        }

        //Evaluar el estado de un proyecto dado las fechas de propuesta
        function evaluarEstadoObra(est) {
            var estado;
            if (est == 0){
                estado = 'Sin Propuesta';
            }else if(est == 1){
                estado = 'Proyecto Sin GO';
            }else if(est == 2){
                estado = 'Con Propuesta';
            }else if(est == 3){
                estado = 'En Ejecución';
            }else{
                estado = 'Ejecutada';
            }
            return estado;
        }

        //Retorna el nombre del estado de una obra
        function getEstadoObra(estado){
            var nombre;
            if (estado == 0) {
                nombre = 'Sin Programar';
            }else if(estado == 1) {
                nombre = 'Programada';
            }else if(estado == 2) {
                nombre = 'Contrapropuesta';
            }else if(estado == 3) {
                nombre = 'Por Ejecutar'
            }else if(estado == 4){
                nombre = 'En Ejecución';
            }else{
                nombre = 'Ejecutada'
            }
            return nombre;
        }

        //Retorna el nombre del estado de una obra
        function getEstadoTrabajo(estado){
            var nombre;
            if (estado == 0) {
                nombre = 'Programado';
            }else if(estado == 1) {
                nombre = 'En Ejecución';
            }else if(estado == 2) {
                nombre = 'En Liquidación';
            }else if(estado == 3) {
                nombre = 'En Facturación'
            }else{
                nombre = 'Ejecutado'
            }
            return nombre;
        }

        //Funcion que retorna un string "procesable" de la fecha que retorna mysql
        function getFormatDate(fecha){
            var x = new Date(fecha);
            var anio = x.getFullYear();
            var mes = (x.getMonth()<10?'0':'') + (x.getMonth()+1)
            var day = (x.getDate()<10?'0':'') + (x.getDate())
            var hora = (x.getHours()<10?'0':'') + (x.getHours())
            var mins = (x.getMinutes()<10?'0':'') + (x.getMinutes())
            var date = anio+'-'+mes+'-'+day+' '+hora+':'+mins;

            return date
        }

        //Funcion para asignar maxima hora del dia a una fecha
        function getMaxHour(fecha){
            var date = moment(fecha).format('YYYY-MM-DD')
            var hora = " 23:59:59";
            var maxFecha = moment(date + hora).toDate();
            return maxFecha;
        }

        //funcion que construye el JSON de las fases de un determinado trabajo
        function buildJsonFases(trabajo){
            var fases = [], ejecucion = materiales = pago = facturacion = {};
            ejecucion = {"nombre": "Ejecución", "estado_programa": "2"}; materiales = {"nombre": "Cuadratura de Materiales"};
            pago = {"nombre": "Estado de Pago"}; facturacion = {"nombre": "Facturación"};
            ejecucion.tipo = materiales.tipo = pago.tipo = facturacion.tipo = 'fase';
            ejecucion.id_raiz = materiales.id_raiz = pago.id_raiz = facturacion.id_raiz = trabajo.id;
            ejecucion.id_locacion = materiales.id_locacion = pago.id_locacion = facturacion.id_locacion = trabajo.id_locacion;
            ejecucion.nombre_locacion = materiales.nombre_locacion = pago.nombre_locacion = facturacion.nombre_locacion = trabajo.nombre_locacion;
            ejecucion.id_usuario_creacion = materiales.id_usuario_creacion = pago.id_usuario_creacion = facturacion.id_usuario_creacion = trabajo.id_usuario_creacion;
            ejecucion.id_js_asignado = materiales.id_js_asignado = pago.id_js_asignado = facturacion.id_js_asignado = trabajo.id_js_asignado;
            ejecucion.id_gp_asignado = materiales.id_gp_asignado = pago.id_gp_asignado = facturacion.id_gp_asignado = trabajo.id_gp_asignado;
            ejecucion.id_eecc_asignado = materiales.id_eecc_asignado = pago.id_eecc_asignado = facturacion.id_eecc_asignado = trabajo.id_eecc_asignado;
            ejecucion.area_servicio = materiales.area_servicio = pago.area_servicio = facturacion.area_servicio = trabajo.area_servicio;
            fases.push(ejecucion); fases.push(facturacion); fases.push(pago);
            if(trabajo.materiales == '0'){
                fases.push(materiales)
            }
            return fases;
        }

        function cerrarSesion(){
            usuariosServ.logout()
                .then(function(res) {
                    $state.go("home");
                })
        }

        return{
            routeAdmin: routeAdmin,
            routeCdc: routeCdc,
            routeJs: routeJs,
            evaluarEstadoObra: evaluarEstadoObra,
            getEstadoObra: getEstadoObra,
            getEstadoTrabajo: getEstadoTrabajo,
            getFormatDate: getFormatDate,
            getMaxHour: getMaxHour,
            buildJsonFases: buildJsonFases,
            cerrarSesion: cerrarSesion
        }

    }])

.factory('generalServ', function($http){
  var api_url_siver = "http://contratistassendas.us-west-2.elasticbeanstalk.com/api";

  //obtiene locaciones de SIVER
  function getLocaciones() {
    var url = api_url_siver + "/localizaciones";
    return $http.get(url);
  }

  //Obtiene locaciones de SIVER por id
  function getLocacionesbyId(id) {
    var url = api_url_siver + "/localizaciones/" + id;
    return $http.get(url);
  }

  function getBrigadesContratista(idc){

    var url=api_url_siver + '/brigades?filter={"include":["servicio","localizacion"],"where":{"id_contratista":'+idc+'}}';
    return $http.get(url);

  }

  function getContratistas() {
    var url = api_url_siver + '/contratistas';
    return $http.get(url)
  }

    function getBrigade(idb){
      var url=api_url_siver +'/brigades/'+idb+'?filter={"include":["servicio","localizacion"]}';
      return $http.get(url);
    }

  return{
    getLocaciones: getLocaciones,
    getLocacionesbyId: getLocacionesbyId,
    getBrigadesContratista: getBrigadesContratista,
    getContratistas: getContratistas,
    getBrigade: getBrigade
  }
})

.factory('gestionServ', function ($http){
    var api = '/api';
    var etapatipo="obra";
    var asignatipo="asignacion";

    function getFasesEstadoEc(est,ec){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas?filter={"include":["parent","pasos","js","gp"], "where":{"tipo":"fase", "estado_programa":'+est+', "id_eecc_asignado":'+ec+'}}';
        return $http.get(url, header);
    }

    function getFasesEstadoGp(est,gp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas?filter={"include":["parent","pasos","js","eecc"], "where":{"tipo":"fase", "estado_programa":'+est+', "id_gp_asignado":'+gp+'}}';
        return $http.get(url, header);
    }

    function getFasesEc(ec){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + '/programas?filter={"include":["parent","pasos","js","gp"], "where":{"tipo":"fase", "id_eecc_asignado":'+ec+'}}';
        return $http.get(url, header);
    }

    function getEtapasEc(ec){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + '/programas?filter={"include":["parent","sons"], "where":{"tipo":"'+etapatipo+'", "id_eecc_asignado":'+ec+'}}';
        return $http.get(url, header);
    }

    function getAsignaciones(){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + '/programas?filter={"include":["parent","sons","eecc"], "where":{"tipo":"'+asignatipo+'"}}';
        return $http.get(url,header);
    }

    function postPrograma(fase){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + "/programas";
        return $http.post(url, fase, header);
    }

    function putPrograma(fase){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url = api + "/programas";
        return $http.put(url, fase, header);
    }

    function postPaso(paso){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url = api + "/pasos";
        return $http.post(url,paso, header);
    }

    function getProgramaMedioId(idp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas/'+idp+'?filter={"include":["parent","pasos","sons"]}';
        return $http.get(url, header);
    }

    function getProyecto(idp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas/'+idp+'?filter={"include":["sons"]}';
        return $http.get(url, header);
    }

    function getCausas(){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/causas_reprogs';
        return $http.get(url, header);
    }

    function getPasosPrograma(idp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/pasos?filter={"where":{"id_programa":'+idp+'}}';
        return $http.get(url, header);
    }

    return{
        getFasesEstadoEc: getFasesEstadoEc,
        getFasesEstadoGp: getFasesEstadoGp,
        putPrograma: putPrograma,
        postPaso: postPaso,
        getProgramaMedioId: getProgramaMedioId,
        getProyecto: getProyecto,
        getCausas: getCausas,
        getPasosPrograma: getPasosPrograma,
        getFasesEc: getFasesEc,
        getEtapasEc: getEtapasEc,
        postPrograma: postPrograma,
        getAsignaciones: getAsignaciones
    }

})


.factory('homeServ', function($http){

  //función para cargar roles del JSON
  function addModel(model, objs){
    var url = "/api/" + model;
    return $http.post(url, objs);
  }
  return{
    addModel: addModel
  }
})

.factory('pasosServ', function ($http) {
    var urlPasos = "/api/pasos";

    function getPasosByIdPrograma(id_programa){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url = urlPasos + "?filter[where][id_programa]=" + id_programa;
        return $http.get(url, header);
    }


    function postPaso(paso){
      var token = window.localStorage.token;
      var header = {headers: {'Authorization': token}};
      return $http.post(urlPasos, paso, header);
    }

    return {
        getPasosByIdPrograma: getPasosByIdPrograma,
        postPaso: postPaso
    }


  }
)

.factory('programasServ', function ($http){
  var urlProgramas =  "/api/programas";

  //Get todos los programas por id_raiz (programas pertenecientes a un nivel superior)
  function getProgramasById(id){
      var token = window.localStorage.token;
      var header = {headers: {'Authorization': token}};
      var url = urlProgramas + '?filter={"where":{"id":' + id + '},"include":["sons","parent"]}';
      return $http.get(url, header);
  }

  //Get todos los programas por id_raiz (programas pertenecientes a un nivel superior)
  function getProgramasByIdRaiz(id_raiz){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"id_raiz":' + id_raiz + '},"include":["sons","parent"]}';
    return $http.get(url, header);
  }

  //Get todos los programas por id_raiz (programas pertenecientes a un nivel superior)
  function getProgramasByCodSap(cod_sap){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"codigo_SAP":"' + cod_sap + '"},"include":["sons"]}';
    return $http.get(url, header);
  }

    /**
     * SERVICIOS PERFIL ADMIN
     */
  //Get Proyectos cerrados
  function getProyectoCerrados (est){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"estado_programa":'+est+'},{"tipo":"proyecto"}]},"include":["sons"]}';
    return $http.get(url, header)
  }

  //Count proyectos cerrados
  function countProyectoCerrados (est) {
      var token = window.localStorage.token;
      var header = {headers: {'Authorization': token}};
      var url = urlProgramas + '/count?where={"estado_programa":'+est+',"tipo":"proyecto"}';
      return $http.get(url, header);
  }
    /**
     * SERVICIOS PERFIL CDC
     */
  /*Count proyecto en general por id_user_creacion, estado_programa.
  Aplica para CDC porque filtra por user de creacion
  PERFIL:CDC Cuenta unicamente proyecto creados por el cdc 'user'
  */
  function countProyectoByCDCEstado (cdc, est) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '/count?where={"id_usuario_creacion":'+cdc+',"estado_programa":'+est+',"tipo":"proyecto"}';
    return $http.get(url, header);
  }

  //get  Programa by id_usuario_creacion, estado_programa. Include sons
  //PERFIL:CDC Obtiene el proyecto del cdc 'usr' con estado 'est'
  function getProyectoByCDCEstado (cdc, est){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_usuario_creacion":'+cdc+'},{"estado_programa":'+est+'},{"tipo":"proyecto"}]},"include":["sons"]}';
    return $http.get(url, header)
  }

  //get  Programa by id_usuario_creacion, estado_programa, tipo. Include js y sons:js sons:ec
  //PERFIL:CDC Obtiene el proyecto del cdc 'usr' con estado 'est'
  function getProyectoByCDCEstado2 (cdc, est){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter[where][id_usuario_creacion]='+cdc+'&filter[where][estado_programa]='+est+'&filter[where][tipo]=proyecto&filter[include][sons]=js&filter[include][sons]=eecc';
    return $http.get(url, header)
  }

    /**
     * SERVICIOS PERFIL JS
     */
  //Count programa en general dado el id_js, estado y tipo
  function countProgramaByJSEstadoTipo (js, est, tipo) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '/count?where={"id_js_asignado":'+js+',"estado_programa":'+est+',"tipo":"'+tipo+'"}';
    return $http.get(url, header);
  }

  //getPrograma dado js, estado, tipo incluye eecc
  function getProgramaByJSEstadoTipo(js, est, tipo){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_js_asignado":'+js+'},{"estado_programa":'+est+'},{"tipo":"'+tipo+'"}]},"include":["eecc","sons"]}';
    return $http.get(url, header)
  }

  //getPrograma dado js, estado, tipo, incluye pasos, sons y eecc
  function getProgramaByJSEstadoTipo2 (js, est, tipo){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_js_asignado":'+js+'},{"estado_programa":'+est+'},{"tipo":"'+tipo+'"}]},"include":["parent","sons","pasos","eecc","js"]}';
    return $http.get(url, header)
  }

    //getPrograma dado js, estado, tipo, incluye pasos, parent y eecc
  function getProgramaByJSEstadoTipo3(id_js, estado, tipo){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_js_asignado":'+id_js+'},{"estado_programa":'+estado+'},{"tipo":"'+tipo+'"}]},"include":["parent","pasos","eecc","gp"]}';
    return $http.get(url, header)
  }

    /**
     * SERVICIOS COMUNES CDC Y JS
     */

  //Post un programa, Función general para crear un programa (proyecto, obra, trabajo o fase, etc)
  function postPrograma(programa){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.post(urlProgramas, programa, header)
  }

  //put programa
  function putPrograma(programa){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.put(urlProgramas, programa, header)
  }
  //Update un programa. Funcion para actualizar un programa dado el id
  function updatePrograma(programa){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '/update?where={"id": '+ programa.id+ '}';
    return $http.post(url, programa, header)
  }


  return{
    getProgramasById: getProgramasById,
    getProgramasByIdRaiz: getProgramasByIdRaiz,
    getProgramasByCodSap: getProgramasByCodSap,
    getProyectoCerrados: getProyectoCerrados,
    countProyectoCerrados: countProyectoCerrados,
    countProyectoByCDCEstado: countProyectoByCDCEstado,
    getProyectoByCDCEstado: getProyectoByCDCEstado,
    getProyectoByCDCEstado2: getProyectoByCDCEstado2,
    countProgramaByJSEstadoTipo: countProgramaByJSEstadoTipo,
    getProgramaByJSEstadoTipo: getProgramaByJSEstadoTipo,
    getProgramaByJSEstadoTipo2: getProgramaByJSEstadoTipo2,
    getProgramaByJSEstadoTipo3: getProgramaByJSEstadoTipo3,
    postPrograma: postPrograma,
    putPrograma: putPrograma,
    updatePrograma: updatePrograma
  }

})

.factory('usuariosServ', function ($http, endpoint){
  var api_usuarios = "/api/users";
  var api_roles = "/api/roles"

  //----USUARIOS----//

  //función para obtener login de usuario
  function login(username, password){
      var api_user = api_usuarios + '/login';
      var login = {username: username, password: password};
      return $http.post(api_user, login);
  }

  //función para logout (elimina el token)
  function logout(){
    var token = window.localStorage.token;
    var api_logout = api_usuarios + '/logout?access_token=' + token;
    return $http.post(api_logout);
  }

  // get de todos los usuarios
  function getUsuarios() {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + "?filter[include]=role";
    return $http.get(url, header);
  }

  // get Usuario by Id (con role)
  function getUsuarioById (id) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + "/" + id + "?filter[include]=role";
    return $http.get(url, header);
  }

  //count número de usuarios creados
  function getNumeroUsuarios() {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + "/count"
    return $http.get(url, header);
  }

  // Post de un usuario
  function postUsuario(user) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.post(api_usuarios, user, header);
  }

  // Delete User
  function deleteUser(id) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + '/' + id;
    return $http.delete(url, header);
  }

  // Update User
  function updateUser(user) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.put(api_usuarios, user, header);
  }

  //request reset passwd
  function resetPasswd(email) {
    var url = api_usuarios + '/reset'
    return $http.post(url, email)
  }

  // Change Passwd
  function changePasswd(newPassword) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var data = { 'newPassword': newPassword }
    var url = api_usuarios + '/reset-password'
    return $http.post(url, data, header)
  }

  //----ROLES----//

  //get de todos los roles
  function getRoles() {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.get(api_roles, header);
  }

  //get id del rol por su nombre include users
  function getRolByName(name) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_roles + '?filter={"where":{"name": "' + name + '"}, "include":["user"]}';
    return $http.get(url, header);
  }


  return{
    login: login,
    logout: logout,
    getUsuarios: getUsuarios,
    getUsuarioById: getUsuarioById,
    getNumeroUsuarios: getNumeroUsuarios,
    postUsuario: postUsuario,
    deleteUser: deleteUser,
    updateUser: updateUser,
    resetPasswd: resetPasswd,
    changePasswd: changePasswd,
    getRoles: getRoles,
    getRolByName: getRolByName
  }
})

.controller('cdcAsignarCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalServ', 'generalFunc', 'usuariosServ',
  function($scope, $state, programasServ, usuariosServ, generalServ, generalFunc, usuariosServ) {

    $('.modal').modal({
        dismissible: false
    });
    $('select').material_select();

    loadInfo();

    //Definición Ui-Grid
    $scope.tablaOp={
          enableFiltering: true,
          enableRowSelection: true,
          multiSelect: false,
          enableColumnResizing:true,
          enableGridMenu:true,
          showColumnFooter:true,
          showGridFooter: true,
          enableColumnMoving:true,
          exporterExcelFilename: 'proyectos.xlsx',
          exporterExcelSheetName: 'proyectos',
          columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:130},
                {field:'nombre', displayName:'Nombre', minWidth:350},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:100},
                {field:'fecha_ingreso', displayName:'Fecha de Creación', minWidth:200, type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:190},
                { name: 'Asignar', width:180, headerCellTemplate: '<br><div class ="center">Asignar Y<br>Balancear Obras</div>',
                  cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openAsignarObrasModal(row.entity)"><i class="material-icons right">input</i>ASIGNAR</button>' }
          ]
    };

    //Cargue info necesaria
    function loadInfo() {
        generalFunc.routeCdc();
        loadProyectosAssigned();
        load();
    }

    //Carga info ui-grid
    function loadProyectosAssigned() {
        var id = window.localStorage['id_user'];
        programasServ.getProyectoByCDCEstado(id, 0, 'proyecto')
            .then(function(res){
                $scope.tablaOp.data = res.data
                $scope.detalleProyecto = [];
            });
    }

    function load() {
        usuariosServ.getRolByName("eecc")
            .then(function(res) {
                $scope.eeccs = res.data[0].user;
            });

        usuariosServ.getRolByName("js")
            .then(function(res) {
                $scope.jefesServicio = res.data[0].user;
            })

        generalServ.getLocaciones()
            .then(function(res) {
                $scope.locaciones = res.data;
            });

        moment.locale('es');
        $scope.hoy = moment().format("YYYY-MM-DD");
    }

    //Modal
    $scope.openAsignarObrasModal = function(project) {
        $scope.proyecto = project;
        $scope.validateSetLocacion(project.id_locacion)
        $scope.proyecto.obras = [{}];
        $('#proyecto').modal('open');
    };

    $scope.closeAsignarObrasModal = function() {
        $('#proyecto').modal('close');
        $scope.proyecto = [];
        $scope.proyecto.obras = [{}];
    }

    //Actualizar proyecto
    $scope.updateProyecto = function(proyecto) {
        $scope.id_raiz_obras = proyecto.id;
        var maxUUCC = _.maxBy(proyecto.obras, 'uucc');
        proyecto.uucc = _.sumBy(proyecto.obras, 'uucc');
        proyecto.id_js_asignado = maxUUCC.id_js_asignado;
        proyecto.estado_programa = proyecto.id_gp_asignado?2:1
        programasServ.putPrograma(proyecto)
            .then(postObras);
    }

    //Crear Obras con id_raiz proyecto anterior
    function postObras(res) {
        if ($scope.proyecto.obras[0].nombre) {
            _.forEach($scope.proyecto.obras, buildJSONObras);
            crearObras($scope.proyecto.obras);
        } else {
            $scope.proyecto = [];
            $scope.proyecto.obras = [{}];
            console.log("No se agregaron obras al proyecto");
        }
    }

    function buildJSONObras(obra, i) {
        $scope.proyecto.obras[i].tipo = 'obra';
        $scope.proyecto.obras[i].fecha_limite = generalFunc.getMaxHour($scope.proyecto.obras[i].fecha_limite);
        $scope.proyecto.obras[i].id_raiz = $scope.id_raiz_obras;
        $scope.proyecto.obras[i].id_usuario_creacion = window.localStorage['id_user'];
        if($scope.proyecto.id_gp_asignado) {
            $scope.proyecto.obras[i].estado_programa = 1;
            $scope.proyecto.obras[i].id_gp_asignado = $scope.proyecto.id_gp_asignado;
        } else {
            $scope.proyecto.obras[i].estado_programa = 0;
        }

        if ($scope.tipoLocacionProyecto == "Establecimiento") {
            $scope.proyecto.obras[i].id_locacion = $scope.proyecto.id_locacion;
            $scope.proyecto.obras[i].nombre_locacion = $scope.proyecto.nombre_locacion;
        }else{
            var locacion = _.find($scope.locaciones, {'id': Number($scope.proyecto.obras[i].id_locacion)})
            $scope.proyecto.obras[i].nombre_locacion = locacion.nombre;
        }
    }

    function crearObras(obras) {
        programasServ.postPrograma(obras)
            .then(function(res) {
                var $toastContent = $('<span>Etapas de obra asignadas éxitosamente!</span>');
                Materialize.toast($toastContent, 3000);
                $scope.closeAsignarObrasModal();
            })
        $scope.proyecto = [];
        $scope.proyecto.obras = [{}];
        loadProyectosAssigned()
    }

    //Validar Locacion del Proyecto
    $scope.validateSetLocacion = function(locacion) {
        var locacion = _.find($scope.locaciones, {'id': Number(locacion)});
        $scope.tipoLocacionProyecto = locacion.tipo
        $scope.proyecto.nombre_locacion = locacion.nombre
        if ($scope.tipoLocacionProyecto == "Zona") {
            $scope.establecimientos = _.filter($scope.locaciones, {'id_zona': locacion.id})
            $scope.showLocacion = true;
        } else if ($scope.tipoLocacionProyecto == "Establecimiento") {
            $scope.showLocacion = false;
        }
    }

    $scope.cerrarSession = function() {
        generalFunc.cerrarSesion()
    }

  }
])
.controller('cdcCrearCtrl', ['$scope', '$state', '$filter','usuariosServ', 'programasServ', 'generalServ', 'generalFunc', 'pasosServ',
    function($scope, $state, $filter, usuariosServ, programasServ, generalServ, generalFunc, pasosServ) {

        $('.modal').modal({
            dismissible: false
        });

        load();

        //Definición Ui-Grid
        $scope.tablaOp = {
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing: true,
            enableGridMenu: true,
            showColumnFooter: true,
            showGridFooter: true,
            enableColumnMoving: true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs: [
                {field: 'Supervisor', displayName: 'Responsable', width: '*'},
                {field: 'Area_servicio', displayName: 'Area Servicio', width: '*'},
                {field: 'Zona', displayName: 'Zona', width: '*'},
                {field: 'Codigo', displayName: 'Codigo', width: '*'},
                {field: 'Nombre', displayName: 'Nombre', width: '*'},
                {field: 'Fecha_Fin', displayName: 'Fecha Fin', width: '*'},
                {field: 'Comentarios', displayName: 'Comentarios', width: '*'}
            ]
        };

        //Cargue Info necesaria
        function load() {
            generalFunc.routeCdc();
            loadInfo();
            setHoy();
        }

        function loadInfo() {
            $scope.loading = true;
            //load eecc
            usuariosServ.getRolByName("eecc")
                .then(function(res) {
                    $scope.eeccs = res.data[0].user;
                });
            //load js
            usuariosServ.getRolByName("js")
                .then(function(res) {
                    $scope.jefesServicio = res.data[0].user;
                })
            //load gp
            usuariosServ.getRolByName("gp")
                .then(function(res) {
                    $scope.geos = res.data[0].user;
                })
            //load locaciones
            generalServ.getLocaciones()
                .then(function(res) {
                    $scope.locaciones = res.data;
                });
        }

        function setHoy() {
            moment.locale('es');
            $scope.hoy = moment().format("YYYY-MM-DD");
        }

        //Crear Proyecto Individual
        $scope.crearProyecto = function(proyecto) {
            var maxUUCC = _.maxBy(proyecto.obras, 'uucc');
            proyecto.tipo = 'proyecto';
            proyecto.id_usuario_creacion = window.localStorage['id_user'];
            proyecto.id_raiz = '0';
            proyecto.uucc = _.sumBy(proyecto.obras, 'uucc');
            proyecto.id_locacion = proyecto.locacion.id;
            proyecto.nombre_locacion = proyecto.locacion.nombre;
            //if ($scope.proyecto.obras[0].nombre) {
            proyecto.id_js_asignado = maxUUCC.id_js_asignado; //Js lider = asignado en obra con mayor uucc
            proyecto.estado_programa = '1'; //estado de proyecto con obras sin gp
            /*} else {
                proyecto.estado_programa = '0'; //estado de proyecto sin obras
            }*/ //ESTE IF ES VALIDO SE SE PERMITE CREACION DE PROYECTO SIN OBRAS
            programasServ.postPrograma(proyecto)
                .then(getProyecto);
            $('#proyecto').modal('close');
        }

        function getProyecto(res) {
            $scope.id_raiz_obras = res.data.id;
            //if ($scope.proyecto.obras[0].nombre) {
            _.forEach($scope.proyecto.obras, buildJSONObras);
            crearObras($scope.proyecto.obras);
            /*} else { //ESTE IF ES NECESARIO SI PERMITE CREAR PROYECTO SIN OBRAS
                $scope.proyecto = [];
                $scope.proyecto.obras = [{}];
                console.log("No se agregaron obras al proyecto");
            }*/
        }

        function buildJSONObras(obra, i) {
            $scope.proyecto.obras[i].tipo = 'obra';
            $scope.proyecto.obras[i].fecha_limite = generalFunc.getMaxHour($scope.proyecto.obras[i].fecha_limite);
            $scope.proyecto.obras[i].id_raiz = $scope.id_raiz_obras;
            $scope.proyecto.obras[i].id_usuario_creacion = window.localStorage['id_user'];
            if ($scope.tipoLocacionProyecto == "Establecimiento") {
                $scope.proyecto.obras[i].id_locacion = $scope.proyecto.id_locacion;
                $scope.proyecto.obras[i].nombre_locacion = $scope.proyecto.nombre_locacion;
            }else{
                var locacion = _.find($scope.locaciones, {'id': Number($scope.proyecto.obras[i].id_locacion)})
                $scope.proyecto.obras[i].nombre_locacion = locacion.nombre;
            }
        }

        function crearObras(obras) {
            programasServ.postPrograma(obras)
                .then(function(res) {
                    var $toastContent = $('<span>Obra creada éxitosamente!</span>');
                    Materialize.toast($toastContent, 6000);
                })
            $scope.proyecto = [];
            $scope.proyecto.obras = [{}];
        }

        //Validar Locación Proyecto
        $scope.validateSetLocacion = function(locacion) {
            $scope.tipoLocacionProyecto = locacion.tipo
            $scope.proyecto.nombre_locacion = locacion.nombre
            if ($scope.tipoLocacionProyecto == "Zona") {
                $scope.establecimientos = _.filter($scope.locaciones, {'id_zona': locacion.id})
                $scope.showLocacion = true;
            } else if ($scope.tipoLocacionProyecto == "Establecimiento") {
                $scope.showLocacion = false;
            }
        }

        //Cerrar modal creacion Individual
        $scope.closeCrearProyectoModal = function() {
            $scope.proyecto = [];
            $scope.proyecto.obras = [{}];
            $('#proyecto').modal('close');
        };

        //Creación Masiva de Proyectos
        $scope.uploadMasivo = function() {
            $scope.por_uucc = $scope.tablaOp.data.uucc
            delete $scope.tablaOp.data['uucc'];
            $scope.codigosExistentes = []
            $scope.codigosRegistrados = []
            $scope.zonas = _.filter($scope.locaciones, {'tipo': 'Zona'});
            $scope.proyectos = $scope.tablaOp.data;
            //$scope.proyectos = _.uniqBy($scope.tablaOp.data, 'Codigo');
            _.forEach($scope.proyectos, buildJSONProyectos);
            //$scope.proyectos = _.uniqBy($scope.proyectos, 'codigo_SAP');
            _.forEach($scope.proyectos, function(proyecto) {
                $scope.loading = false;
                programasServ.postPrograma(proyecto)
                    .then(getSuccess, getError);
            })
            $scope.tablaOp.data = [];
        }

        // Se construye Json del proyecto
        function buildJSONProyectos(proyecto) {
            proyecto.id_raiz = '0';
            proyecto.tipo = 'proyecto';
            proyecto.id_usuario_creacion = window.localStorage['id_user'];
            if ( proyecto['Codigo'].slice(-2).split('-')[1] ) {
                proyecto['codigo_SAP'] = proyecto['Codigo'].slice(0, proyecto['Codigo'].length - 2)
            } else if ( proyecto['Codigo'].slice(-2).split('_')[1] ) {
                proyecto['codigo_SAP'] = proyecto['Codigo'].split('_')[0]
            }
            else {
                proyecto['codigo_SAP'] = proyecto['Codigo']
            }
            if ( proyecto['Area_servicio'].substr(0, 1).toLowerCase() == 'm' ) {
                proyecto['area_servicio'] = 'Mantenimiento'
                //proyecto['id_js_asignado'] =  //16//SAN BERNARDO Produccion (crojo)
                //proyecto['id_js_asignado'] = 67 //BIO BIO Produccion (L_Aranguis)
                proyecto['id_js_asignado'] = 87 //ANTOFAGASTA Produccion (rlorca)
            } else if ( proyecto['Area_servicio'].substr(0, 1).toLowerCase() == 'c' ) {
                proyecto['area_servicio'] = 'Construcción'
                // proyecto['id_js_asignado'] =  //17//SAN BERNARDO Produccion (secarrascor)
                //proyecto['id_js_asignado'] = 68 //BIO BIO Produccion (R_Rivas)
                proyecto['id_js_asignado'] = 75 //ANTOFAGASTA Produccion (jfigueroa)
            }
            $scope.zonas.forEach(function (z) {
                if(proyecto['Zona'].toUpperCase().replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, "") == z.nombre.replace(/\s/g, "").substr(0,proyecto['Zona'].length)){
                    proyecto['id_locacion'] = z.id;
                    proyecto['nombre_locacion'] = z.nombre;
                    return false;
                }
            })
            $scope.geos.forEach(function (g) {
                if(proyecto['Supervisor'].split('/')[0].toUpperCase().replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, "") == g.name.toUpperCase().replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, "")) {
                    proyecto['id_gp_asignado'] = g.id;
                    return false;
                }
            })
            if (proyecto['id_gp_asignado']) {
                proyecto.estado_programa = '2';
            } else {
                proyecto.estado_programa = '1';
            }
            proyecto['fecha_inicio'] = proyecto['Fecha_Inicio']
            proyecto['fecha_fin'] = proyecto['Fecha_Fin']
            proyecto['fecha_limite'] = proyecto['Fecha_Fin']
            proyecto['nombre'] = proyecto['Nombre'] || 'Obra sin Nombre'
            proyecto['comentarios'] = proyecto['Comentarios']
            delete proyecto['Area_servicio']
            delete proyecto['Zona']
            delete proyecto['Supervisor']
            delete proyecto['Nombre']
            //delete proyecto['Codigo']
            delete proyecto['Fecha_Fin']
            delete proyecto['Comentarios']
            delete proyecto['x']
        }

        // Si el proyecto (obra) se carga exitosamente
        function getSuccess(res) {
            $scope.codigosRegistrados.push(res.config.data.codigo_SAP);
            //Se cargan etapas
            uploadEtapas(res.data);
            if ($scope.codigosRegistrados.length + $scope.codigosExistentes.length == $scope.proyectos.length - 1) {
                $scope.loading = true;
                $('#cargueMasivo').modal('open');
            }
        }

        // Si el proyecto (obra) ya existía
        function getError(res) {
            $scope.codigosExistentes.push(res.config.data);
            var data = res.config.data;
            programasServ.getProgramasByCodSap(res.config.data.codigo_SAP)
                .then(function(res){
                    //Se actualizan etapas
                    updateEtapas(res.data, data)
                })
            if ($scope.codigosRegistrados.length + $scope.codigosExistentes.length == $scope.proyectos.length - 1) {
                $scope.loading = true;
                $('#cargueMasivo').modal('open');
            }

        }

        function uploadEtapas(r) {
            var etapas = [];
            var ini = Number(r.Sem_Ini)
            var fin = Number(r.Sem_Fin)
            var nextWeek = moment().week() - 1;
            var i;
            var per = _.find($scope.por_uucc, ['codigo_SAP', r.Codigo])
            if(r.ec1){
                for (i=nextWeek; i<=fin; i++) {
                    var sem = 'ec1s' + i
                    if ( r[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = r.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = r.id_locacion
                        e['nombre_locacion'] = r.nombre_locacion
                        e['id_usuario_creacion'] = r.id_usuario_creacion
                        e['id_js_asignado'] = r.id_js_asignado
                        e['id_gp_asignado'] = r.id_gp_asignado
                        e['comentarios'] = r.comentarios
                        e['area_servicio'] = r.area_servicio
                        var v = (nextWeek-ini > 0)?(i - ini):1
                        e['nombre'] = r.ec1.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', r.ec1.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        if (r.codigo_SAP=='CGED-18005430'){
                            console.log(per)
                        }
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(r.ec2){
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        etapas.push(e)
                    }
                }
            }
            if(r.ec2){
                for (i=nextWeek; i<=fin; i++) {
                    var sem = 'ec2s' + i
                    if ( r[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = r.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = r.id_locacion
                        e['nombre_locacion'] = r.nombre_locacion
                        e['id_usuario_creacion'] = r.id_usuario_creacion
                        e['id_js_asignado'] = r.id_js_asignado
                        e['id_gp_asignado'] = r.id_gp_asignado
                        e['comentarios'] = r.comentarios
                        e['area_servicio'] = r.area_servicio
                        var v = (nextWeek-ini > 0)?(i - ini):1
                        e['nombre'] = r.ec2.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', r.ec2.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(r.ec1){
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        etapas.push(e)
                    }
                }
            }
            programasServ.postPrograma(etapas)
                .then(etapasSuccess, etapasError);
        }

        function etapasSuccess(res) {
            r = res.data;
        }

        function etapasError(res) {
            r = res.data;
        }

        function updateEtapas(obra, data) {
            var o = obra[0];
            var etapas = o.sons;
            var existentes = [];
            var nuevas = [];
            var ini = Number(data.Sem_Ini)
            var fin = Number(data.Sem_Fin)
            var lastWeek = moment().week() - 1;
            var i;
            var per = _.find($scope.por_uucc, ['codigo_SAP', data.Codigo])
            if(data.ec1){
                for (i=lastWeek; i<=fin; i++) {
                    var sem = 'ec1s' + i
                    if ( data[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = o.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = o.id_locacion
                        e['nombre_locacion'] = o.nombre_locacion
                        e['id_usuario_creacion'] = data.id_usuario_creacion
                        e['id_js_asignado'] = o.id_js_asignado
                        e['id_gp_asignado'] = o.id_gp_asignado
                        e['comentarios'] = o.comentarios
                        e['area_servicio'] = o.area_servicio
                        var v = ((lastWeek+2)-ini > 0)?(i - ini):1
                        e['nombre'] = data.ec1.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', data.ec1.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(data.ec2){
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        var etapa = _.find(etapas, ['nombre', e['nombre']])
                        if (etapa && etapa.id_eecc_asignado == e.id_eecc_asignado){
                            e['id'] = etapa.id
                            e['estado_programa'] = etapa.estado_programa
                            //e['estado_programa']=i<moment().week()?4:1
                            existentes.push(e)
                        } else if(etapa && etapa.id_eecc_asignado != e.id_eecc_asignado) {
                            nuevas.push(e)
                        } else if (!etapa && i>=(lastWeek+2)){
                            nuevas.push(e)
                        }
                    }
                }
            }
            if(data.ec2){
                for (i=lastWeek; i<=fin; i++) {
                    var sem = 'ec2s' + i
                    if ( data[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = o.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = o.id_locacion
                        e['nombre_locacion'] = o.nombre_locacion
                        e['id_usuario_creacion'] = o.id_usuario_creacion
                        e['id_js_asignado'] = o.id_js_asignado
                        e['id_gp_asignado'] = o.id_gp_asignado
                        e['comentarios'] = o.comentarios
                        e['area_servicio'] = o.area_servicio
                        var v = ((lastWeek+2)-ini > 0)?(i - ini):1
                        e['nombre'] = data.ec2.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', data.ec2.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(data.ec1){
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        var etapa = _.find(etapas, ['nombre', e['nombre']])
                        if (etapa && etapa.id_eecc_asignado == e.id_eecc_asignado){
                            e['id'] = etapa.id
                            e['estado_programa'] = etapa.estado_programa
                            //e['estado_programa']=i<moment().week()?4:1
                            existentes.push(e)
                        } else if(etapa && etapa.id_eecc_asignado != e.id_eecc_asignado) {
                            nuevas.push(e)
                        } else if (!etapa && i>=(lastWeek+2)){
                            nuevas.push(e)
                        }
                    }
                }
            }
            if(nuevas){
                programasServ.postPrograma(nuevas)
                    .then(etapasSuccess, etapasError);
            }
            _.forEach(existentes, putEtapas)
        }

        function putEtapas(etapa){
            programasServ.updatePrograma(etapa)
                .then(function(res){
                    r = res.data;
                })
        }

        $scope.closeModal = function() {
            $('#cargueMasivo').modal('close');
        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])

.controller('cdcEditarCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalServ', 'generalFunc',
  function($scope, $state, programasServ, usuariosServ, generalServ, generalFunc) {

    $('.modal').modal();
    $('.collapsible').collapsible();
    $('select').material_select();

    loadInfo();

    //Definición Ui-Grid
    $scope.tablaOp={
          enableFiltering: true,
          enableRowSelection: true,
          multiSelect: false,
          enableColumnResizing:true,
          enableGridMenu:true,
          showColumnFooter:true,
          showGridFooter: true,
          enableColumnMoving:true,
          exporterExcelFilename: 'proyectos.xlsx',
          exporterExcelSheetName: 'proyectos',
          columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:110},
                {field:'nombre', displayName:'Nombre', minWidth:280},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:100},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:170},
                {field:'porcentaje_avance', displayName:'Porcentaje Avance', minWidth:150},
                { name: 'Asignar', width:160, headerCellTemplate: '<br><div class ="center">Editar<br> Obras</div>',
                  cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openAsignarObrasModal(row.entity)"><i class="material-icons right">mode_edit</i>EDITAR</button>' }
            ]
    };

    //Cargue info necesaria
    function loadInfo() {
        generalFunc.routeCdc();
        loadProyectosAssigned();
        getLocaciones();
        loadEECC();
        loadJS();
    }

    function loadEECC() {
      usuariosServ.getRolByName("eecc")
          .then(function(res) {
              $scope.eeccs = res.data[0].user;
          });
    }

    function loadJS() {
      usuariosServ.getRolByName("js")
          .then(function(res) {
              $scope.jefesServicio = res.data[0].user;
          })
    }

    function getLocaciones() {
      generalServ.getLocaciones()
          .then(function(res) {
              $scope.locaciones = res.data;
          });
    }

   //Cargue info ui-grid
    function loadProyectosAssigned() {
        var id = window.localStorage['id_user'];
        programasServ.getProyectoByCDCEstado2(id, 1, 'proyecto')
            .then(function(res){
                $scope.tablaOp.data = res.data
                $scope.detalleProyecto = [];
            });
    }

    //Modal
    $scope.openAsignarObrasModal = function(project) {
      $scope.proyecto = project;
      $scope.validateSetLocacion(project.id_locacion)
      $scope.proyecto.obras = project.sons;
      _.forEach(project.sons, setObras)
      $('#proyecto').modal('open');
    };

    function setObras(obra,i){
      $scope.proyecto.obras[i].uucc = Number(obra.uucc)
      $scope.proyecto.obras[i].fecha_limite = generalFunc.getFormatDate(obra.fecha_limite)
    }

    $scope.closeAsignarObrasModal = function() {
      $('#proyecto').modal('close');
      $scope.proyecto = [];
      $scope.proyecto.obras = [{}];
    }

   //Aactualiza proyecto
    $scope.updateProyecto = function(proyecto){
        proyecto.uucc = _.sumBy(proyecto.obras, 'uucc');
        var maxUUCC = _.maxBy(proyecto.obras, 'uucc');
        proyecto.id_js_asignado = maxUUCC.id_js_asignado;
        programasServ.updatePrograma(proyecto)
            .then(updateObras)
    }

    function updateObras(res){
        if (res.data.count == 1){
            _.forEach($scope.proyecto.obras, function(obra){
                programasServ.updatePrograma(obra)
                    .then(function (res){
                        $scope.proyecto = [];
                        $scope.proyecto.obras = [{}];
                        $('#proyecto').modal('close');
                        var $toastContent = $('<span>Actualización Exitosa!</span>');
                        Materialize.toast($toastContent, 4000);
                    })
            })
        }
    }

    //Valida Locacion Proyecto
    $scope.validateSetLocacion = function(locacion) {
      var locacion = _.find($scope.locaciones, {'id': Number(locacion)});
      $scope.tipoLocacionProyecto = locacion.tipo
      $scope.proyecto.nombre_locacion = locacion.nombre
      if ($scope.tipoLocacionProyecto == "Zona") {
          $scope.establecimientos = _.filter($scope.locaciones, {'id_zona': locacion.id})
          $scope.showLocacion = true;
      } else if ($scope.tipoLocacionProyecto == "Establecimiento") {
          $scope.showLocacion = false;
      }
    }

    //Detalle
    $scope.tablaOp.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(row) {
            $scope.detalleProyecto = row.entity;
            usuariosServ.getUsuarioById(row.entity.id_js_asignado)
                .then(function(res){
                    $scope.detalleProyecto.js = res.data.name
                })
            $scope.detalleObras = row.entity.sons;
            _.forEach(row.entity.sons, function (obra){
                obra.estado_programa = generalFunc.evaluarEstadoObra(obra.estado_programa)
                obra.fecha_ingreso = generalFunc.getFormatDate(obra.fecha_ingreso)
                obra.fecha_limite = generalFunc.getFormatDate(obra.fecha_limite)
            })
            $('#detalleProyecto').modal('open');
        })
    }

    $scope.cerrarSession = function() {
        generalFunc.cerrarSesion()
    }

  }
])
.controller('cdcHomeCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalServ', 'generalFunc',
  function($scope, $state, programasServ, usuariosServ, generalServ, generalFunc){

    $('.modal').modal({
      dismissible: false
    });
    $('select').material_select();

    generalFunc.routeCdc();
    countCDCProjects();
    loadUser();

    //Contador de Proyectos
    function countCDCProjects() {
        var id = window.localStorage['id_user'];
        programasServ.countProyectoByCDCEstado(id, 0, 'proyecto')
            .then(function (res) {
                $scope.unassigned = res.data.count;
            })
        programasServ.countProyectoByCDCEstado(id, 1, 'proyecto')
            .then(function (res) {
                $scope.assigned = res.data.count;
            })
    }

    //Cargue info usuario
    function loadUser(){
      var cdc = window.localStorage['id_user']
      usuariosServ.getUsuarioById(cdc)
          .then(setZona)
    }

    function setZona(res){
      $scope.usuario = res.data;
      generalServ.getLocacionesbyId(res.data.id_locacion)
          .then(function(res){
              $scope.usuario.zona = res.data.nombre;
          })
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion()
    }
  }
])

.controller('gestionEcCtrl', ['$scope', '$state', 'gestionServ', 'usuariosServ','generalServ', 'generalFunc', 'uiGridConstants',
    function($scope, $state, gestionServ, usuariosServ, generalServ, generalFunc, uiGridConstants){

        $('.modal').modal(
            {
                dismissible: false
            }
        );
        $('.collapsible').collapsible();
        $('select').material_select();

        if(window.sessionStorage['rol'] == "admin"){
            // $state.go("perfil_admin");
        }else if( window.sessionStorage['rol'] == 'cdc'){
            $state.go("perfil_cdc");
        }else{
            //$state.go("home");
        }

        $scope.user = window.localStorage.getItem("id_user");
        $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
        $scope.reporte={tipo:"reporte", avance:0};
        $scope.fase={};
        $scope.ejecucion="Ejecución";
        $scope.liquidacion="Liquidación";
        $scope.facturacion="Facturación";
        $scope.avanceparcial="Avance parcial";
        $scope.brigas=false;

        //loadInfo();
        testTemp();

        $scope.tablaOp={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosContraproponer.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'geo', displayName:'Supervisor', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date', sort: {direction: uiGridConstants.ASC}},
                { name: 'Accion', width:150,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openContra(row.entity)">Contraproponer</button>' }
            ],
            onRegisterApi: function( gridApi ) {
                $scope.gridcApi = gridApi;
            }
        };

        $scope.tablaOpr={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosReportar.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'geo', displayName:'Supervisor', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date', sort: {direction: uiGridConstants.ASC}},
                { name: 'Accion', width:150,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openReporte(row.entity)">Reportar</button>' }
            ],
            onRegisterApi: function( gridApi ) {
                $scope.gridrApi = gridApi;
            }
        };

        $scope.tablaBri={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'brigadasContratista.xlsx',
            exporterExcelSheetName: 'brigadas',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'id', displayName:'Id', minWidth:70},
                {field:'nombre', displayName:'Nombre', minWidth:160},
                {field:'sigla', displayName:'Servicio', minWidth:100},
                { name: 'Añadir', width:80,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.addBrigada(row.entity)">Elegir</button>' }
            ]
        };

        loadInfo();

        function loadInfo(){
            getUsuario();
            var eje={id:1, name:"Ejecutado"};
            var rep={id:2, name:"Reprogramado"};
            $scope.results=[eje,rep];
            $scope.resultado=$scope.results[0];

            gestionServ.getCausas().then(function(rea){
                $scope.causas=rea.data;
                $scope.causas.splice(0,0,{id:0, causa:"Sin causa"});
                _.forEach($scope.causas,function(cauz,index){
                    cauz.disa=false;
                });
                habilitarCausas();
            });

            gestionServ.getFasesEstadoEc(2,$scope.user).then(function(res){
                $scope.proyectosc=res.data;
                $scope.tablaOp.data=$scope.proyectosc;
                _.forEach($scope.proyectosc,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.geo=pro.gp.name;
                    prioridadP(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });
            gestionServ.getFasesEstadoEc(4,$scope.user).then(function(res){
                $scope.proyectosr=res.data;
                $scope.tablaOpr.data=$scope.proyectosr;
                _.forEach($scope.proyectosr,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.geo=pro.gp.name;
                    prioridadC(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });
        }

        function getUsuario(){
            usuariosServ.getUsuarioById($scope.user).then(function(res){
                $scope.nombre=res.data.name;
                $scope.contratista=res.data.id_contratista;
                loadBrigadas($scope.contratista);
            });
        }

        function loadBrigadas(idc){
            generalServ.getBrigadesContratista(idc).then(function(res){
                $scope.brigadas=res.data;
                $scope.tablaBri.data=$scope.brigadas;
                _.forEach($scope.brigadas,function(bri){
                    bri.sigla=bri.servicio.sigla;
                });
            });
        }

        function prioridadC(fase){
            fase.fecha_limite=new Date();
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            if (compromiso.id>0){
                fase.fecha_limite=new Date(compromiso.fecha_fin);
            }
            $scope.gridrApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
            filtroSemanas($scope.proyectosr,fase);
        }

        function prioridadP(fase){
            fase.fecha_limite=new Date();
            gestionServ.getPasosPrograma(fase.parent.id).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];
                    fase.fecha_limite=new Date(propuesta.fecha_fin);
                }
                $scope.gridcApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
                filtroSemanas($scope.proyectosc,fase);
            });
        }

        function filtroSemanas(lista,fase){
            var fecha = new Date();
            fecha.setDate(fecha.getDate()+15);
            if(fase.fecha_limite>fecha){
                deleteByIdd(lista,fase.id);
            }
        }

        function obraProyecto(fase){
            gestionServ.getProgramaMedioId(fase.parent.id_raiz).then(function(res){
                var obra=res.data;
                console.log(obra);
                fase.obra=obra.nombre;
                fase.codigo_SAP=obra.parent.codigo_SAP;
                fase.description=obra.parent.nombre;
            });
        }

        function habilitarCausas(){
            if ($scope.resultado.id==1){
                $scope.causa=$scope.causas[0];
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=true;
                    }else{
                        cauz.disa=false;
                    }
                });
            }else{
                $scope.causa=$scope.causas[1];
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=false;
                    }else{
                        cauz.disa=true;
                    }
                });
            }
        }

        $scope.habCausas=function(){
            if ($scope.resultado.id==1){
                $scope.causa=$scope.causas[0];
                $scope.reporte.avance=100;
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=true;
                    }else{
                        cauz.disa=false;
                    }
                });
            }else{
                if($scope.causa.id==0){
                    $scope.causa=$scope.causas[1];
                }
                $scope.avance();
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=false;
                    }else{
                        cauz.disa=true;
                    }
                    if(cauz.causa==$scope.avanceparcial && $scope.fase.nombre!=$scope.ejecucion){
                        cauz.disa=true;
                    }
                });
            }
        };

        $scope.avance=function(){
            if($scope.causa.causa!=$scope.avanceparcial){
                $scope.reporte.avance=0;
            }
        };

        function ultimoPasoTipo(pasos, tipo){
            var pasostipo=_.filter(pasos,function(step){
                return step.tipo==tipo;
            });
            var ultimo={id:0};
            if (pasostipo.length>0){
                ultimo=pasostipo[pasostipo.length-1];
            }
            return ultimo;
        }

        function leerPrevio(paso){
            $scope.previo.titulo="Propuesta previa";
            $scope.previo.tipo=paso.tipo;
            var ini=new Date(paso.fecha_inicio);
            var fin=new Date(paso.fecha_fin);
            $scope.previo.fecha_inicio=ini;
            $scope.previo.fecha_fin=fin;
            $scope.previo.comentario=paso.comentario;
            if (paso.tipo=="validacions"){
                $scope.reporte.id_compromiso=paso.id;
                $scope.previo.titulo="Validacion previa";
            }
            if (paso.tipo=="compromiso"){
                $scope.reporte.id_compromiso=paso.id;
                $scope.previo.titulo="Compromiso previo";
                $scope.previo.id_brigada=paso.id_brigada;
                leerBrigada(paso.id_brigada);
            }
        }

        function leerBrigada(idb){
            generalServ.getBrigade(idb).then(function(res){
                $scope.previo.nombre_brigada=res.data.nombre;
                $scope.previo.servicio_brigada=res.data.servicio.sigla;
            });
        }

        function previoError(){
            $scope.previo={};
            $scope.previo.titulo="Paso previo";
            $scope.previo.tipo="Otro";
            $scope.reporte.id_compromiso=0;
            $scope.previo.fecha_inicio="Error";
            $scope.previo.fecha_fin="Error";
            $scope.previo.comentario="Error: paso previo no encontrado";
        }

        function previoContrapropuesta(fase){
            $scope.previo={};
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            if (compromiso.id>0){
                leerPrevio(compromiso);
            }else{
                var propuesta=ultimoPasoTipo(fase.pasos, "propuesta");
                if (propuesta.id>0){
                    leerPrevio(propuesta);
                }else{
                    previoError();
                }
            }
        }

        function previoReporte(fase){
            $scope.previo={};
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            var validacion=ultimoPasoTipo(fase.pasos, "validacions");
            if (validacion.id>0){
                leerPrevio(validacion);
                $scope.previo.role="Supervisor";
                $scope.previo.quien=fase.geo;
            }else if(compromiso.id>0){
                leerPrevio(compromiso);
                $scope.previo.role="JA";
                $scope.previo.quien=fase.jefe;
            }else{
                previoError();
            }

        }

        function deleteByIdd(lista,id){

            _.each(lista, function(tra,index){

                if(index<lista.length){
                    if(tra.id==id){

                        lista.splice(index,1);
                    }
                }
            });
        }

        function copiarPrograma(program){
            var nuevo={id:program.id};
            if (typeof program.tipo != "undefined"){
                nuevo.tipo=program.tipo;
            }
            if (typeof program.nombre != "undefined"){
                nuevo.nombre=program.nombre;
            }
            if (typeof program.estado_programa != "undefined"){
                nuevo.estado_programa=program.estado_programa;
            }
            if (typeof program.codigo_SAP != "undefined"){
                nuevo.codigo_SAP=program.codigo_SAP;
            }
            if (typeof program.uucc != "undefined"){
                nuevo.uucc=program.uucc;
            }
            if (typeof program.porcentaje_avance != "undefined"){
                nuevo.porcentaje_avance=program.porcentaje_avance;
            }
            if (typeof program.comentarios != "undefined"){
                nuevo.comentarios=program.comentarios;
            }
            if (typeof program.area_servicio != "undefined"){
                nuevo.area_servicio=program.area_servicio;
            }
            if (typeof program.num_reprog != "undefined"){
                nuevo.num_reprog=program.num_reprog;
            }else{
                nuevo.num_reprog=0;
            }

            if (typeof program.id_raiz != "undefined"){
                nuevo.id_raiz=program.id_raiz;
            }
            if (typeof program.id_pasos != "undefined"){
                nuevo.id_pasos=program.id_pasos;
            }
            if (typeof program.id_locacion != "undefined"){
                nuevo.id_locacion=program.id_locacion;
            }
            if (typeof program.fecha_limite != "undefined"){
                nuevo.fecha_limite=program.fecha_limite;
            }
            if (typeof program.fecha_cierre != "undefined"){
                nuevo.fecha_cierre=program.fecha_cierre;
            }
            if (typeof program.fecha_ingreso != "undefined"){
                nuevo.fecha_ingreso=program.fecha_ingreso;
            }

            if (typeof program.id_usuario_creacion != "undefined"){
                nuevo.id_usuario_creacion=program.id_usuario_creacion;
            }
            if (typeof program.id_js_asignado != "undefined"){
                nuevo.id_js_asignado=program.id_js_asignado;
            }
            if (typeof program.id_gp_asignado != "undefined"){
                nuevo.id_gp_asignado=program.id_gp_asignado;
            }
            if (typeof program.id_eecc_asignado != "undefined"){
                nuevo.id_eecc_asignado=program.id_eecc_asignado;
            }
            if (typeof program.id_brigada_asignada != "undefined"){
                nuevo.id_brigada_asignada=program.id_brigada_asignada;
            }
            if (typeof program.nombre_locacion != "undefined"){
                nuevo.nombre_locacion=program.nombre_locacion;
            }
            return nuevo;
        }

        //DatePicker
        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Aceptar',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: true, // Creates a dropdown to control year
            selectTime: true,
            container: 'body',
            format: 'yyyy-mm-dd'
            //onClose: function(){
            //  $("#mdlTrainingDetail form input:eq(1)").focus();
            //}
        });

        //$('.timepicker').pickatime({});

        function testTemp(){
            $scope.nombre="Felipe Ascencio";
            $scope.perfil="Empresa Contratista";

            $scope.previo={};
            $scope.previo.fecha_inicio=new Date(2018,3,20);
            $scope.previo.fecha_fin=new Date(2018,3,29);
            $scope.previo.comentario="El trabajo debe hacerse en horas de la mañana";
            $scope.previo.id_brigada=10;
            $scope.previo.nombre_brigada="Lineas Sendas 1";
            $scope.previo.servicio_brigada="Solicitudes comerciales e internas (nuevas conexiones y otros)";
        }

        $scope.modal1=function(){
            $('#contrapropuesta').modal('open');
        };

        $scope.modal2=function(){
            $('#reporte').modal('open');
        };

        $scope.openContra=function(fase){
            $('#contrapropuesta').modal('open');
            previoContrapropuesta(fase);
            $scope.paso.id_programa=fase.id;
            $scope.paso.tipo="contrapropuesta";
            $scope.fase=copiarPrograma(fase);
            $scope.job={};
            $scope.job.codigo_SAP=fase.codigo_SAP;
            $scope.job.description=fase.description;
            $scope.job.nom_tra=fase.parent.nombre;
            propuestaTrabajo(fase.parent.id);
        };

        function propuestaTrabajo(idt){

            $scope.job.inicio=new Date(2010,1,1);
            $scope.job.fin=new Date(2058,1,1);

            gestionServ.getPasosPrograma(idt).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];

                    $scope.job.inicio=new Date(propuesta.fecha_inicio);
                    $scope.job.fin=new Date(propuesta.fecha_fin);
                    $scope.job.comentario=propuesta.comentario;
                }
                console.log($scope.job);
            });
        }

        $scope.openReporte=function(fase){
            $('#reporte').modal('open');
            //$scope.gridrApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
            previoReporte(fase);
            $scope.reporte.id_programa=fase.id;
            $scope.reporte.tipo="reporte";
            $scope.fase=copiarPrograma(fase);
            $scope.previo.trabajo=fase.parent;
            $scope.previo.num_reprog=fase.num_reprog;
            $scope.job={};
            $scope.job.codigo_SAP=fase.codigo_SAP;
            $scope.job.description=fase.description;
            $scope.job.nom_tra=fase.parent.nombre;
            propuestaTrabajo(fase.parent.id);
            $scope.habCausas();
        };

        $scope.cancelarC=function(){
            $('#contrapropuesta').modal('close');
            $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
            $scope.fase={};
            $scope.previo={};
        };

        $scope.cancelarReporte=function(){
            $('#reporte').modal('close');
            $scope.reporte={tipo:"reporte", avance:0};
            $scope.fase={};
            $scope.previo={};
            $scope.resultado=$scope.results[0];
            habilitarCausas();
        };

        $scope.contraproponer= function(){

            validarC();

            if ($scope.errores.length==0){
                $('#contrapropuesta').modal('close');
                $scope.paso.fecha_creacion=new Date();
                var ms=$scope.paso.fecha_inicio._d;
                var msf=$scope.paso.fecha_fin._d;
                $scope.paso.fecha_inicio=ms;
                $scope.paso.fecha_fin=msf;
                $scope.fase.codigo_SAP=undefined;
                console.log($scope.paso.fecha_inicio);
                gestionServ.postPaso($scope.paso).then(function(res){
                    $scope.fase.estado_programa=3;
                    gestionServ.putPrograma($scope.fase).then(function(rea){
                        $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
                        $scope.fase={};
                        $scope.previo={};
                        loadInfo();
                    });
                });
            }else{
                $('#contrapropuesta').modal('close');
                $('#errores').modal('open');
            }

        };

        function buscarNombre(lista,nombre){
            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.nombre==nombre){
                    obj=objeto;
                }
            });
            return obj;
        }

        function verificarEstados(lista,estado){
            var resp=true;
            _.forEach(lista,function(program){
                if(program.estado_programa!=estado){
                    resp=false;
                }
            });
            return resp;
        }

        function reloadInfo(){
            $scope.reporte={tipo:"reporte", avance:0};
            $scope.resultado=$scope.results[0];
            $scope.fase={};
            $scope.previo={};
            loadInfo();
        }

        function validarC(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.paso.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.paso.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.paso.fecha_fin;
                res1=validarCampo($scope.paso.fecha_inicio,"less",info);
                if (res1=="ok"){
                    info.max=$scope.job.fin;
                    info.min=$scope.job.inicio;
                    res1=validarCampo($scope.paso.fecha_inicio,"inrange",info);
                    res2=validarCampo($scope.paso.fecha_fin,"inrange",info);
                    if (res1=="ok" && res2=="ok"){

                    }else{
                        //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    }
                    info.min=new Date();
                    res1=validarCampo($scope.paso.fecha_inicio,"greater",info);
                    res2=validarCampo($scope.paso.fecha_fin,"greater",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser posteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.paso.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.paso.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

            res=validarCampo($scope.paso.id_brigada,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
            }else{
                info.min=1;
                res=validarCampo($scope.paso.id_brigada,"greater",info);
                if (res!="ok"){
                    $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
                }
            }

            res=validarCampo($scope.paso.supervisor,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Supervisor: "+res);
            }else{
                res=validarCampo($scope.paso.supervisor,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Supervisor: "+res);
                }
            }

        }

        function validarR(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.reporte.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.reporte.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.reporte.fecha_fin;
                res1=validarCampo($scope.reporte.fecha_inicio,"less",info);
                if (res1=="ok"){
                    info.max=$scope.job.fin;
                    info.min=$scope.job.inicio;
                    res1=validarCampo($scope.reporte.fecha_inicio,"inrange",info);
                    res2=validarCampo($scope.reporte.fecha_fin,"inrange",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    }
                    info.max=new Date();
                    res1=validarCampo($scope.reporte.fecha_inicio,"less",info);
                    res2=validarCampo($scope.reporte.fecha_fin,"less",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser anteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.reporte.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.reporte.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

            res=validarCampo($scope.reporte.avance,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Avance: "+res);
            }else{
                if($scope.causa.causa==$scope.avanceparcial){
                    info.min=1;
                    info.max=95;
                    res=validarCampo($scope.reporte.avance,"inrange",info);
                    if (res!="ok"){
                        $scope.errores.push("Avance: "+res);
                    }
                }

            }

        }

        function validarCampo(valor,tipo,info){

            var resul="Error de tipo de validacion";

            if (tipo=="undefined"){
                if (typeof valor!="undefined"){
                    resul="ok";
                }else{
                    resul="El campo no puede ser vacio";
                }
            }else if(tipo=="emptystring"){
                if (valor==""){
                    resul="El campo no puede ser vacio";
                }else{
                    resul="ok";
                }
            }else if(tipo=="different"){
                if (valor==info.igual){
                    resul="El campo no puede ser igual a "+info.igual;
                }else{
                    resul="ok";
                }
            }else if(tipo=="equal"){
                if (valor==info.igual){
                    resul="ok";
                }else{
                    resul="El campo debe ser igual a "+info.igual;
                }
            }else if(tipo=="greater"){
                if(valor>=info.min){
                    resul="ok";
                }else{
                    resul="El campo debe ser mayor a "+info.min;
                }
            }else if(tipo=="less"){
                if(valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe ser menor a "+info.max;
                }
            }else if(tipo=="inrange"){
                if(info.min<=valor && valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe estar entre "+info.min+" y "+info.max;
                }
            }else if(tipo=="datatype"){
                if(typeof valor==info.tipo){
                    resul="ok";
                }else{
                    resul="Se espera que el valor sea tipo "+info.tipo;
                }
            }
            return resul;
        }

        $scope.closeErrores=function (){

            $('#errores').modal('close');
            if ($scope.fase.estado_programa==2){
                $('#contrapropuesta').modal('open');
            }else if($scope.fase.estado_programa==4){
                $('#reporte').modal('open');
            }
        };

        function reportard(){

            $('#reporte').modal('close');
            $scope.reporte.fecha_creacion=new Date();
            var ms=$scope.reporte.fecha_inicio._d;
            var msf=$scope.reporte.fecha_fin._d;
            $scope.reporte.fecha_inicio=ms;
            $scope.reporte.fecha_fin=msf;
            $scope.reporte.estado=$scope.resultado.id;
            $scope.fase.codigo_SAP=undefined;
            if($scope.causa.id>0){
                $scope.reporte.id_causa=$scope.causa.id;
            }
            //guardo el reporte
            gestionServ.postPaso($scope.reporte).then(function(ree){

                if($scope.reporte.estado==2){
                    if($scope.fase.nombre==$scope.ejecucion){
                        $scope.fase.estado_programa=3;
                        $scope.fase.num_reprog=$scope.fase.num_reprog+1;
                    }else{
                        $scope.fase.estado_programa=5;
                    }
                    //si es reprogamado siempre vamos a compromiso
                    gestionServ.putPrograma($scope.fase).then(function(rea){
                        reloadInfo();
                    });
                }else{
                    if($scope.fase.nombre==$scope.ejecucion){
                        $scope.fase.estado_programa=5;
                        gestionServ.putPrograma($scope.fase).then(function(reb){
                            reloadInfo();
                        });
                    }else{
                        //Siempre hay validacion!
                        $scope.fase.estado_programa=5;
                        gestionServ.putPrograma($scope.fase).then(function(reb){
                            reloadInfo();
                        });
                    }
                }
            });
        }

        $scope.reportar= function(){

            validarR();
            if ($scope.errores.length==0){
                reportard();
            }else{
                $('#reporte').modal('close');
                $('#errores').modal('open');
            }

        };

        $scope.logAlgo= function(objeto){
            console.log(objeto);
        };

        $scope.cancelarB=function(){
            $('#brigadas').modal('close');
            $('#contrapropuesta').modal('open');
            $scope.brigas=false;
        };

        $scope.openBrigadas=function(){
            $('#contrapropuesta').modal('close');
            $('#brigadas').modal('open');
            $scope.brigas=true;
        };

        $scope.addBrigada=function(briga){
            $('#brigadas').modal('close');
            $('#contrapropuesta').modal('open');
            $scope.paso.id_brigada=briga.id;
            $scope.previo.name=briga.nombre;
            $scope.previo.sigla=briga.sigla;
            $scope.brigas=false;
        };

    }
])
.controller('programarEcCtrl', ['$scope', '$state', 'gestionServ', 'usuariosServ','generalServ', 'generalFunc', 'uiGridConstants',
    function($scope, $state, gestionServ, usuariosServ, generalServ, generalFunc, uiGridConstants){

        $('.modal').modal(
            {
                dismissible: false
            }
        );
        $('.collapsible').collapsible();
        $('select').material_select();

        if(window.sessionStorage['rol'] == "admin"){
            // $state.go("perfil_admin");
        }else if( window.sessionStorage['rol'] == 'cdc'){
            $state.go("perfil_cdc");
        }else{
            //$state.go("home");
        }

        $scope.user = window.localStorage.getItem("id_user");
        $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
        $scope.job={};
        $scope.previo={};
        $scope.etapa={id:0};
        $scope.brigas=false;
        //NOMBRE ESTANDARES DE LAS FASES
        $scope.replanteo="Replanteo";
        $scope.ejecucion="Ejecución";
        $scope.cuadratura="Cuadratura de Materiales";
        $scope.estado="Estado de Pago";
        $scope.facturacion="Facturación";
        //OTROS ESTADOS DE LA TABLA PROGRAMA
        $scope.eoclosed=4;//Etapa de obra cerrada
        $scope.oclosed=3;//Obra cerrada
        //ESTADOS DEL TRABAJO SEGUN FASE ACTUAL
        $scope.jobej=1;//En ejecucion
        $scope.jobep=3;//En estado de pago
        $scope.jobfa=4;//En facturacion
        $scope.jobclosed=5;//Trabajo cerrado

        $scope.tablaEta={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'EtapasEC.xlsx',
            exporterExcelSheetName: 'etapas',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'numtra', displayName:'# Trabajos', minWidth:160},
                {field:'nombre_locacion', displayName:'Localizacion', minWidth:160},
                {field:'servicio', displayName:'Servicio', minWidth:160},
                {field:'uucc', displayName:'UUCC', minWidth:160},
                {field:'fecha_limite', displayName:'Plazo', minWidth:160, type:"date", cellFilter: 'date'},
                {field:'semana', displayName:'Semana', minWidth:150},
                { name: 'Ver Trabajos', width:150,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.verTrabajos(row.entity)">Ver</button>' }
            ]
        };

        $scope.tablaTra={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'Trabajos.xlsx',
            exporterExcelSheetName: 'trabajos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'nombre', displayName:'Descripcion', minWidth:160},
                {field:'uucc', displayName:'UUCC', minWidth:160},
                {field:'fase', displayName:'Fase', minWidth:160},
                {field:'paso', displayName:'Paso actual', minWidth:160},
                {field:'inicio', displayName:'Fecha inicio', minWidth:160, type:"date", cellFilter: 'date:\'dd-MM-yyyy  h:mma\''},
                {field:'fin', displayName:'Fecha fin', minWidth:160, type:"date", cellFilter: 'date:\'dd-MM-yyyy  h:mma\''},
                {field:'comment', displayName:'Comentario', minWidth:150}
            ]
        };

        $scope.tablaBri={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'brigadasContratista.xlsx',
            exporterExcelSheetName: 'brigadas',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'id', displayName:'Id', minWidth:70},
                {field:'nombre', displayName:'Nombre', minWidth:160},
                {field:'sigla', displayName:'Servicio', minWidth:100},
                { name: 'Añadir', width:80,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.addBrigada(row.entity)">Elegir</button>' }
            ]
        };

        loadData();

        function loadData(){
            getUsuario();
            gestionServ.getFasesEc($scope.user).then(function(res){
                $scope.fases=res.data;
                gestionServ.getEtapasEc($scope.user).then(function(rea){
                    $scope.etapas=_.filter(rea.data,filtroEtapas);
                    $scope.tablaEta.data=$scope.etapas;
                    _.forEach($scope.etapas,completarEtapa);
                    reloadEtapa();
                });
            });
        }

        function reloadEtapa(){
            if($scope.etapa.id>0){
                var stage=buscarId($scope.etapas,$scope.etapa.id);
                $scope.verTrabajos(stage);
            }
        }

        function getUsuario(){
            usuariosServ.getUsuarioById($scope.user).then(function(res){
                $scope.nombre=res.data.name;
                $scope.contratista=res.data.id_contratista;
                loadBrigadas($scope.contratista);
            });
        }

        function loadBrigadas(idc){
            generalServ.getBrigadesContratista(idc).then(function(res){
                $scope.brigadas=res.data;
                $scope.tablaBri.data=$scope.brigadas;
                _.forEach($scope.brigadas,function(bri){
                    bri.sigla=bri.servicio.sigla;
                });
            });
        }

        function completarEtapa(etap){
            if (etap.parent.tipo=="proyecto"){
                etap.codigo_SAP=etap.parent.codigo_SAP;
            }else{
                gestionServ.getProgramaMedioId(etap.parent.id_raiz).then(function(res){
                    etap.codigo_SAP=res.data.codigo_SAP;
                });
            }
            etap.numtra=etap.sons.length;
            etap.servicio="OTRO"; //revisar --> Leo la relacion con el servicio de la brigada cuando esta exista
            etap.semana=semanaFecha(etap.fecha_limite); //revisar --> Como se la semana de la etapa actual
            _.forEach(etap.sons,completarTrabajo);

        }

        function completarTrabajo(traba){
            traba.fases=_.filter($scope.fases,function(fase){
                return fase.id_raiz==traba.id;
            });
            if(traba.estado_programa==0 || traba.estado_programa==1){
                traba.fase=$scope.ejecucion;
            }else if(traba.estado_programa==2){
                traba.fase=$scope.cuadratura;
            }
            else if(traba.estado_programa==3){
                traba.fase=$scope.estado;
            }else{
                traba.fase=$scope.facturacion;
            }
            var current=buscarNombre(traba.fases,traba.fase);
            console.log(current);
            if (current.id>0){
                if(current.pasos.length>0){
                    var paso=current.pasos[current.pasos.length-1];
                    traba.paso=paso.tipo;
                    traba.inicio=paso.fecha_inicio;
                    traba.fin=paso.fecha_fin;
                    traba.comment=paso.comentario;
                }else{
                    traba.paso="Sin pasos";
                    traba.comment="Sin programacion";
                }
            }else{
                traba.paso="Error: Fase acual no encontrada";
            }
        }

        function buscarNombre(lista,nombre){
            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.nombre==nombre){
                    obj=objeto;
                }
            });
            return obj;
        }

        function buscarId(lista,id){

            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.id==id){
                    obj=objeto;
                }

            });

            return obj;
        }

        function semanaFecha(fecha){
            var temp=new Date(fecha);
            var year = temp.getFullYear();
            var month = temp.getMonth();
            var date = temp.getDate();
            var d = new Date(Date.UTC(year,month,date));
            var dayNum = d.getUTCDay() || 7;
            d.setUTCDate(d.getUTCDate() + 4 - dayNum);
            var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
            var week = Math.ceil((((d - yearStart) / 86400000) + 1)/7);
            return week;
        }

        function filtroEtapas(etap){
            var res=true;
            if (etap.estado_programa==0){
                res=false;
            }
            if (etap.estado_programa==$scope.eoclosed){
                res=false;
            }
            //revisar --> Aca mirar si tenemos etapa grande o pequeña y cual queremos
            return res;
        }

        $scope.verTrabajos=function(etap){

            $scope.trabajos=etap.sons;
            $scope.tablaTra.data=$scope.trabajos;
            $scope.etapa=etap;
            $scope.etapa.idaux=etap.id;
        };

        $scope.nuevo=function(){
            $('#trabajo').modal('open');
        }

        $scope.cancelar=function(){
            $('#trabajo').modal('close');
            $scope.job={};
            $scope.previo={};
            $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};

        };

        $scope.openBrigadas=function(){
            $('#trabajo').modal('close');
            $('#brigadas').modal('open');
            $scope.brigas=true;
        };

        $scope.addBrigada=function(briga){
            $('#brigadas').modal('close');
            $('#trabajo').modal('open');
            $scope.paso.id_brigada=briga.id;
            $scope.previo.name=briga.nombre;
            $scope.previo.sigla=briga.sigla;
            $scope.brigas=false;
        };

        $scope.cancelarB=function(){
            $('#brigadas').modal('close');
            $('#trabajo').modal('open');
            $scope.brigas=false;
        };

        $scope.setFin=function(){
            $scope.paso.fecha_fin=moment($scope.paso.fecha_inicio);
            console.log($scope.paso.fecha_inicio);
            console.log($scope.paso.fecha_fin);
        };

        function buildTrabajo() {

            $scope.job.tipo = 'trabajo';
            $scope.job.comentarios = "Trabajo creado por la EC";
            $scope.job.estado_programa = 1;
            $scope.job.porcentaje_avance = 0;
            $scope.job.fecha_ingreso = new Date();
            $scope.job.id_usuario_creacion = $scope.user;
            var fecha=new Date($scope.paso.fecha_fin._d);
            fecha.setDate(fecha.getDate()+30);
            $scope.job.fecha_limite =fecha;

            $scope.job.id_raiz = $scope.etapa.id;
            $scope.job.id_locacion = $scope.etapa.id_locacion;
            $scope.job.nombre_locacion = $scope.etapa.nombre_locacion;
            $scope.job.id_js_asignado = $scope.etapa.id_js_asignado;
            $scope.job.id_gp_asignado = $scope.etapa.id_gp_asignado;
            $scope.job.id_eecc_asignado = $scope.etapa.id_eecc_asignado;
            $scope.job.area_servicio = $scope.etapa.area_servicio;

        }

        function buildFases(trabajo){
            var fases = [], ejecucion = materiales = pago = facturacion = {};
            ejecucion = {"nombre": $scope.ejecucion, "estado_programa": "3"}; materiales = {"nombre": $scope.cuadratura};
            pago = {"nombre": $scope.estado}; facturacion = {"nombre": $scope.facturacion};
            ejecucion.tipo = materiales.tipo = pago.tipo = facturacion.tipo = 'fase';
            ejecucion.id_raiz = materiales.id_raiz = pago.id_raiz = facturacion.id_raiz = trabajo.id;
            ejecucion.id_locacion = materiales.id_locacion = pago.id_locacion = facturacion.id_locacion = trabajo.id_locacion;
            ejecucion.nombre_locacion = materiales.nombre_locacion = pago.nombre_locacion = facturacion.nombre_locacion = trabajo.nombre_locacion;
            ejecucion.id_usuario_creacion = materiales.id_usuario_creacion = pago.id_usuario_creacion = facturacion.id_usuario_creacion = trabajo.id_usuario_creacion;
            ejecucion.id_js_asignado = materiales.id_js_asignado = pago.id_js_asignado = facturacion.id_js_asignado = trabajo.id_js_asignado;
            ejecucion.id_gp_asignado = materiales.id_gp_asignado = pago.id_gp_asignado = facturacion.id_gp_asignado = trabajo.id_gp_asignado;
            ejecucion.id_eecc_asignado = materiales.id_eecc_asignado = pago.id_eecc_asignado = facturacion.id_eecc_asignado = trabajo.id_eecc_asignado;
            ejecucion.area_servicio = materiales.area_servicio = pago.area_servicio = facturacion.area_servicio = trabajo.area_servicio;
            fases.push(ejecucion); fases.push(facturacion); fases.push(pago);
            fases.push(materiales);

            return fases;
        }

        $scope.crear=function(){
            validarC(); //pendiente
            if ($scope.errores.length==0){
                creard();
            }else{
                $('#trabajo').modal('close');
                $('#errores').modal('open');
            }
        };

        function buildPaso(fase){
            $scope.paso.id_programa=fase.id;
            $scope.paso.tipo="contrapropuesta";
            $scope.paso.fecha_creacion=new Date();
            var ms=$scope.paso.fecha_inicio._d;
            var msf=$scope.paso.fecha_fin._d;
            $scope.paso.fecha_inicio=ms;
            $scope.paso.fecha_fin=msf;
        }

        function creard(){
            buildTrabajo();
            $scope.total=3;
            $scope.count=0;
            gestionServ.postPrograma($scope.job).then(function(res){
                var fases=buildFases(res.data);
                var propuesta={id_programa:res.data.id, tipo:"propuesta"}
                propuesta.fecha_creacion=new Date();
                propuesta.fecha_inicio=$scope.paso.fecha_inicio._d;
                propuesta.fecha_fin=$scope.paso.fecha_fin._d;
                propuesta.comentario=$scope.paso.comentario;
                gestionServ.postPaso(propuesta).then(function(rea){
                    _.forEach(fases,crearFase);
                });
            });
        }

        function crearFase(fase){
            gestionServ.postPrograma(fase).then(function(res){
                if (res.data.nombre==$scope.ejecucion){
                    buildPaso(res.data);
                    gestionServ.postPaso($scope.paso).then(saveDetalles);
                }else{
                    saveDetalles(1);
                }
            });
        }

        function saveDetalles(res){
            $scope.count=$scope.count+1;
            if($scope.count==$scope.total){
                $scope.job={};
                $scope.previo={};
                $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
                loadData();
            }
        }

        function copiarPrograma(program){
            var nuevo={id:program.id};
            if (typeof program.tipo != "undefined"){
                nuevo.tipo=program.tipo;
            }
            if (typeof program.nombre != "undefined"){
                nuevo.nombre=program.nombre;
            }
            if (typeof program.estado_programa != "undefined"){
                nuevo.estado_programa=program.estado_programa;
            }
            if (typeof program.codigo_SAP != "undefined"){
                nuevo.codigo_SAP=program.codigo_SAP;
            }
            if (typeof program.uucc != "undefined"){
                nuevo.uucc=program.uucc;
            }
            if (typeof program.porcentaje_avance != "undefined"){
                nuevo.porcentaje_avance=program.porcentaje_avance;
            }
            if (typeof program.comentarios != "undefined"){
                nuevo.comentarios=program.comentarios;
            }
            if (typeof program.area_servicio != "undefined"){
                nuevo.area_servicio=program.area_servicio;
            }
            if (typeof program.num_reprog != "undefined"){
                nuevo.num_reprog=program.num_reprog;
            }else{
                nuevo.num_reprog=0;
            }

            if (typeof program.id_raiz != "undefined"){
                nuevo.id_raiz=program.id_raiz;
            }
            if (typeof program.id_pasos != "undefined"){
                nuevo.id_pasos=program.id_pasos;
            }
            if (typeof program.id_locacion != "undefined"){
                nuevo.id_locacion=program.id_locacion;
            }
            if (typeof program.fecha_limite != "undefined"){
                nuevo.fecha_limite=program.fecha_limite;
            }
            if (typeof program.fecha_cierre != "undefined"){
                nuevo.fecha_cierre=program.fecha_cierre;
            }
            if (typeof program.fecha_ingreso != "undefined"){
                nuevo.fecha_ingreso=program.fecha_ingreso;
            }

            if (typeof program.id_usuario_creacion != "undefined"){
                nuevo.id_usuario_creacion=program.id_usuario_creacion;
            }
            if (typeof program.id_js_asignado != "undefined"){
                nuevo.id_js_asignado=program.id_js_asignado;
            }
            if (typeof program.id_gp_asignado != "undefined"){
                nuevo.id_gp_asignado=program.id_gp_asignado;
            }
            if (typeof program.id_eecc_asignado != "undefined"){
                nuevo.id_eecc_asignado=program.id_eecc_asignado;
            }
            if (typeof program.id_brigada_asignada != "undefined"){
                nuevo.id_brigada_asignada=program.id_brigada_asignada;
            }
            if (typeof program.nombre_locacion != "undefined"){
                nuevo.nombre_locacion=program.nombre_locacion;
            }
            return nuevo;
        }

        function validarC(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.paso.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.paso.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.paso.fecha_fin;
                res1=validarCampo($scope.paso.fecha_inicio,"less",info);
                if (res1=="ok"){
                    // info.max=$scope.job.fin;
                    // info.min=$scope.job.inicio;
                    // res1=validarCampo($scope.paso.fecha_inicio,"inrange",info);
                    // res2=validarCampo($scope.paso.fecha_fin,"inrange",info);
                    // if (res1=="ok" && res2=="ok"){

                    // }else{
                    //   //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    // }
                    info.min=new Date();
                    res1=validarCampo($scope.paso.fecha_inicio,"greater",info);
                    res2=validarCampo($scope.paso.fecha_fin,"greater",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser posteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.paso.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.paso.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

            res=validarCampo($scope.paso.id_brigada,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
            }else{
                info.min=1;
                res=validarCampo($scope.paso.id_brigada,"greater",info);
                if (res!="ok"){
                    $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
                }
            }

            res=validarCampo($scope.paso.supervisor,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Supervisor: "+res);
            }else{
                res=validarCampo($scope.paso.supervisor,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Supervisor: "+res);
                }
            }

            res=validarCampo($scope.job.nombre,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Nombre trabajo: "+res);
            }else{
                res=validarCampo($scope.job.nombre,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Nombre trabajo: "+res);
                }
            }

            res=validarCampo($scope.job.uucc,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("UUCC: "+res);
            }else{
                info.min=1;
                res=validarCampo($scope.job.uucc,"greater",info);
                if (res!="ok"){
                    $scope.errores.push("UUCC: "+res);
                }
            }

        }

        function validarCampo(valor,tipo,info){

            var resul="Error de tipo de validacion";

            if (tipo=="undefined"){
                if (typeof valor!="undefined"){
                    resul="ok";
                }else{
                    resul="El campo no puede ser vacio";
                }
            }else if(tipo=="emptystring"){
                if (valor==""){
                    resul="El campo no puede ser vacio";
                }else{
                    resul="ok";
                }
            }else if(tipo=="different"){
                if (valor==info.igual){
                    resul="El campo no puede ser igual a "+info.igual;
                }else{
                    resul="ok";
                }
            }else if(tipo=="equal"){
                if (valor==info.igual){
                    resul="ok";
                }else{
                    resul="El campo debe ser igual a "+info.igual;
                }
            }else if(tipo=="greater"){
                if(valor>=info.min){
                    resul="ok";
                }else{
                    resul="El campo debe ser mayor a "+info.min;
                }
            }else if(tipo=="less"){
                if(valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe ser menor a "+info.max;
                }
            }else if(tipo=="inrange"){
                if(info.min<=valor && valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe estar entre "+info.min+" y "+info.max;
                }
            }else if(tipo=="datatype"){
                if(typeof valor==info.tipo){
                    resul="ok";
                }else{
                    resul="Se espera que el valor sea tipo "+info.tipo;
                }
            }
            return resul;
        }

        $scope.closeErrores=function (){
            $('#errores').modal('close');
            $('#trabajo').modal('open');
        };

        $scope.logAlgo= function(objeto){
            console.log(objeto);
        };

        //------------------------------------
        //Fin nuevo controlador
        //------------------------------------

    }
])


.controller('gestionGpCtrl', ['$scope', '$state', 'gestionServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, gestionServ, usuariosServ, generalFunc){

        $('.modal').modal(
            {
                dismissible: false
            }
        );
        $('.collapsible').collapsible();
        $('select').material_select();

        if(window.sessionStorage['rol'] == "admin"){
            // $state.go("perfil_admin");
        }else if( window.sessionStorage['rol'] == 'cdc'){
            $state.go("perfil_cdc");
        }else{
            //$state.go("home");
        }

        $scope.user = window.localStorage.getItem("id_user");
        $scope.validacion={tipo:"validacions"};
        $scope.fase={};
        //ESTADOS DE FASE SEGUN PASO ACTUAL
        $scope.statect=2;//En contrapropuesta
        $scope.stateco=3;//En compromiso
        $scope.stater=4;//En reporte
        $scope.statev=5;//En validacion de supervisor
        $scope.statevj=6;//En validacion de JA
        $scope.staterep=8;//En reprogramacion del CDC
        $scope.stateclosed=7;//Fase cerrada
        //ESTADOS DEL TRABAJO SEGUN FASE ACTUAL
        $scope.jobej=1;//En ejecucion
        $scope.jobep=3;//En estado de pago
        $scope.jobfa=4;//En facturacion
        $scope.jobclosed=5;//Trabajo cerrado
        //OTROS ESTADOS DE LA TABLA PROGRAMA
        $scope.eoclosed=4;//Etapa de obra cerrada
        $scope.oclosed=3;//Obra cerrada
        //NOMBRE ESTANDARES DE LAS FASES
        $scope.replanteo="Replanteo";
        $scope.ejecucion="Ejecución";
        $scope.cuadratura="Cuadratura de Materiales";
        $scope.estado="Estado de Pago";
        $scope.facturacion="Facturación";

        $scope.avanceparcial="Avance parcial";


        //loadInfo();
        testTemp();

        $scope.tablaOp={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosValidarS.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'ec', displayName:'Empresa', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date'},
                { name: 'Accion', width:150,
                    cellTemplate:'<button class="buttonT" ng-click="grid.appScope.openValidacion(row.entity)">Validar</button>' }
            ]
        };

        $scope.tablaOpr={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosReportar.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'ec', displayName:'Empresa', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date'}
            ]
        };

        loadInfo();

        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Aceptar',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: true, // Creates a dropdown to control year
            container: 'body',
            format: 'yyyy-mm-dd'
            //onClose: function(){
            //  $("#mdlTrainingDetail form input:eq(1)").focus();
            //}
        });

        function testTemp(){
            $scope.nombre="Andres Posada";
            $scope.perfil="Gestor Proyecto";

            $scope.previo={};
            $scope.previo.tipo="compromiso";
            $scope.previo.titulo="Reporte previo";
            $scope.previo.fecha_inicio=new Date(2018,3,20);
            $scope.previo.fecha_fin=new Date(2018,3,29);
            $scope.previo.comentario="El trabajo debe hacerse en horas de la mañana";
            $scope.previo.avance=10;
            $scope.previo.resultado="Reprogramado";
            $scope.previo.causa=$scope.avanceparcial;

        }

        function loadInfo(){

            var eje={id:1, name:"Aprobada"};
            var rep={id:2, name:"Reprobada"};
            $scope.results=[eje,rep];
            $scope.resultado=$scope.results[0];

            gestionServ.getCausas().then(function(rea){
                $scope.causas=rea.data;
                $scope.causas.splice(0,0,{id:0, causa:"Sin causa"});
            });

            gestionServ.getFasesEstadoGp($scope.stater,$scope.user).then(function(rew){
                $scope.proyectosr=rew.data;
                $scope.tablaOpr.data=$scope.proyectosr;
                _.forEach($scope.proyectosr,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.ec=pro.eecc.name;
                    prioridadC(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });


            gestionServ.getFasesEstadoGp($scope.statev,$scope.user).then(function(res){
                $scope.proyectosc=res.data;
                $scope.tablaOp.data=$scope.proyectosc;
                _.forEach($scope.proyectosc,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.ec=pro.eecc.name;
                    prioridadP(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });
            getUsuario();
        }

        function getUsuario(){
            usuariosServ.getUsuarioById($scope.user).then(function(res){
                $scope.nombre=res.data.name;
            });
        }

        function prioridadC(fase){
            fase.fecha_limite=new Date();
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            if (compromiso.id>0){
                fase.fecha_limite=new Date(compromiso.fecha_fin);
            }
        }

        function prioridadP(fase){
            fase.fecha_limite=new Date();
            gestionServ.getPasosPrograma(fase.parent.id).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];
                    fase.fecha_limite=new Date(propuesta.fecha_fin);
                }
            });
        }

        function obraProyecto(fase){
            gestionServ.getProgramaMedioId(fase.parent.id_raiz).then(function(res){
                var obra=res.data;
                console.log(obra);
                fase.obra=obra.nombre;
                fase.codigo_SAP=obra.parent.codigo_SAP;
                fase.description=obra.parent.nombre;
            });
        }

        $scope.openValidacion=function(fase){
            $('#validacion').modal('open');
            previoValidacion(fase);
            $scope.validacion.id_programa=fase.id;
            $scope.validacion.tipo="validacions";
            $scope.fase=copiarPrograma(fase);
            $scope.previo.trabajo=fase.parent;
            $scope.previo.num_reprog=fase.num_reprog;
            $scope.job={};
            $scope.job.codigo_SAP=fase.codigo_SAP;
            $scope.job.description=fase.description;
            $scope.job.nom_tra=fase.parent.nombre;
            propuestaTrabajo(fase.parent.id);
        };

        function previoValidacion(fase){
            $scope.previo={};
            var reporte=ultimoPasoTipo(fase.pasos, "reporte");
            if (reporte.id>0){
                leerPrevio(reporte);
                $scope.previo.role="EECC";
                $scope.previo.quien=fase.ec;
            }else{
                previoError();
            }
        }

        function leerPrevio(paso){
            $scope.previo.titulo="Reporte previo";
            $scope.previo.tipo=paso.tipo;
            var ini=new Date(paso.fecha_inicio);
            var fin=new Date(paso.fecha_fin);
            $scope.previo.fecha_inicio=ini;
            $scope.previo.fecha_fin=fin;
            $scope.previo.comentario=paso.comentario;
            if (paso.estado==1){
                $scope.previo.resultado="Ejecutado";
            }else{
                $scope.previo.resultado="Reprogramado";
            }
            if (typeof paso.id_causa=="undefined"){
                paso.id_causa=0;
            }
            var causa=buscarId($scope.causas,paso.id_causa);
            $scope.previo.causa=causa.causa;
            $scope.previo.avance=paso.avance;
        }

        function buscarId(lista,id){

            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.id==id){
                    obj=objeto;
                }

            });

            return obj;
        }

        function previoError(){
            $scope.previo={};
            $scope.previo.titulo="Paso previo";
            $scope.previo.tipo="Otro";
            $scope.previo.fecha_inicio="Error";
            $scope.previo.fecha_fin="Error";
            $scope.previo.comentario="Error: paso previo no encontrado";
        }

        function ultimoPasoTipo(pasos, tipo){
            var pasostipo=_.filter(pasos,function(step){
                return step.tipo==tipo;
            });
            var ultimo={id:0};
            if (pasostipo.length>0){
                ultimo=pasostipo[pasostipo.length-1];
            }
            return ultimo;
        }

        function copiarPrograma(program){
            var nuevo={id:program.id};
            if (typeof program.tipo != "undefined"){
                nuevo.tipo=program.tipo;
            }
            if (typeof program.nombre != "undefined"){
                nuevo.nombre=program.nombre;
            }
            if (typeof program.estado_programa != "undefined"){
                nuevo.estado_programa=program.estado_programa;
            }
            if (typeof program.codigo_SAP != "undefined"){
                nuevo.codigo_SAP=program.codigo_SAP;
            }
            if (typeof program.uucc != "undefined"){
                nuevo.uucc=program.uucc;
            }
            if (typeof program.porcentaje_avance != "undefined"){
                nuevo.porcentaje_avance=program.porcentaje_avance;
            }
            if (typeof program.comentarios != "undefined"){
                nuevo.comentarios=program.comentarios;
            }
            if (typeof program.area_servicio != "undefined"){
                nuevo.area_servicio=program.area_servicio;
            }
            if (typeof program.num_reprog != "undefined"){
                nuevo.num_reprog=program.num_reprog;
            }

            if (typeof program.id_raiz != "undefined"){
                nuevo.id_raiz=program.id_raiz;
            }
            if (typeof program.id_pasos != "undefined"){
                nuevo.id_pasos=program.id_pasos;
            }
            if (typeof program.id_locacion != "undefined"){
                nuevo.id_locacion=program.id_locacion;
            }
            if (typeof program.fecha_limite != "undefined"){
                nuevo.fecha_limite=program.fecha_limite;
            }
            if (typeof program.fecha_cierre != "undefined"){
                nuevo.fecha_cierre=program.fecha_cierre;
            }
            if (typeof program.fecha_ingreso != "undefined"){
                nuevo.fecha_ingreso=program.fecha_ingreso;
            }

            if (typeof program.id_usuario_creacion != "undefined"){
                nuevo.id_usuario_creacion=program.id_usuario_creacion;
            }
            if (typeof program.id_js_asignado != "undefined"){
                nuevo.id_js_asignado=program.id_js_asignado;
            }
            if (typeof program.id_gp_asignado != "undefined"){
                nuevo.id_gp_asignado=program.id_gp_asignado;
            }
            if (typeof program.id_eecc_asignado != "undefined"){
                nuevo.id_eecc_asignado=program.id_eecc_asignado;
            }
            if (typeof program.id_brigada_asignada != "undefined"){
                nuevo.id_brigada_asignada=program.id_brigada_asignada;
            }
            if (typeof program.nombre_locacion != "undefined"){
                nuevo.nombre_locacion=program.nombre_locacion;
            }
            return nuevo;
        }

        function propuestaTrabajo(idt){

            $scope.job.inicio=new Date(2010,1,1);
            $scope.job.fin=new Date(2058,1,1);

            gestionServ.getPasosPrograma(idt).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];

                    $scope.job.inicio=new Date(propuesta.fecha_inicio);
                    $scope.job.fin=new Date(propuesta.fecha_fin);
                    $scope.job.comentario=propuesta.comentario;
                }
                console.log($scope.job);
            });
        }

        function deleteByIdd(lista,id){

            _.each(lista, function(tra,index){

                if(index<lista.length){
                    if(tra.id==id){

                        lista.splice(index,1);
                    }
                }
            });
        }

        //DatePicker
        $scope.modal1=function(){
            if ($scope.previo.causa==$scope.avanceparcial){
                $scope.previo.causa="Clima";
            }else{
                $scope.previo.causa=$scope.avanceparcial;
            }
        };

        $scope.modal2=function(){
            $('#validacion').modal('open');
        };





        $scope.cancelarValidacion=function(){
            $('#validacion').modal('close');
            $scope.validacion={tipo:"validacions"};
            $scope.fase={};
            $scope.previo={};
            $scope.resultado=$scope.results[0];
        };

        function reloadInfo(){
            $scope.validacion={tipo:"validacions"};
            $scope.fase={};
            $scope.previo={};
            $scope.resultado=$scope.results[0];
            loadInfo();
        }

        $scope.sendValidar= function(){

            validar();

            if ($scope.errores.length==0){
                senddummy();
            }else{
                $('#validacion').modal('close');
                $('#errores').modal('open');
            }

        };

        $scope.closeErrores=function (){

            $('#errores').modal('close');
            $('#validacion').modal('open');

        };

        function validar(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.validacion.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.validacion.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.validacion.fecha_fin;
                res1=validarCampo($scope.validacion.fecha_inicio,"less",info);
                if (res1=="ok"){
                    info.max=$scope.job.fin;
                    info.min=$scope.job.inicio;
                    res1=validarCampo($scope.validacion.fecha_inicio,"inrange",info);
                    res2=validarCampo($scope.validacion.fecha_fin,"inrange",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    }
                    info.max=new Date();
                    res1=validarCampo($scope.validacion.fecha_inicio,"less",info);
                    res2=validarCampo($scope.validacion.fecha_fin,"less",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser anteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.validacion.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.validacion.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

        }

        function validarCampo(valor,tipo,info){

            var resul="Error de tipo de validacion";

            if (tipo=="undefined"){
                if (typeof valor!="undefined"){
                    resul="ok";
                }else{
                    resul="El campo no puede ser vacio";
                }
            }else if(tipo=="emptystring"){
                if (valor==""){
                    resul="El campo no puede ser vacio";
                }else{
                    resul="ok";
                }
            }else if(tipo=="different"){
                if (valor==info.igual){
                    resul="El campo no puede ser igual a "+info.igual;
                }else{
                    resul="ok";
                }
            }else if(tipo=="equal"){
                if (valor==info.igual){
                    resul="ok";
                }else{
                    resul="El campo debe ser igual a "+info.igual;
                }
            }else if(tipo=="greater"){
                if(valor>=info.min){
                    resul="ok";
                }else{
                    resul="El campo debe ser mayor a "+info.min;
                }
            }else if(tipo=="less"){
                if(valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe ser menor a "+info.max;
                }
            }else if(tipo=="inrange"){
                if(info.min<=valor && valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe estar entre "+info.min+" y "+info.max;
                }
            }else if(tipo=="datatype"){
                if(typeof valor==info.tipo){
                    resul="ok";
                }else{
                    resul="Se espera que el valor sea tipo "+info.tipo;
                }
            }
            return resul;
        }

        function verificarEstados(lista,estado){
            var resp=true;
            _.forEach(lista,function(program){
                if(program.estado_programa!=estado){
                    resp=false;
                }
            });
            return resp;
        }

        function nextFase(actual){

            var resp=$scope.facturacion;
            if(actual==$scope.replanteo){
                resp=$scope.ejecucion;
            }else if(actual==$scope.ejecucion){
                resp=$scope.cuadratura;
            }else if(actual==$scope.cuadratura){
                resp=$scope.estado;
            }else if(actual==$scope.estado){
                resp=$scope.facturacion;
            }

            return resp;
        }

        function startFase(fase){
            var resp=$scope.stateco;
            if (fase==$scope.ejecucion){
                resp=$scope.statect;
            }
            return resp;
        }

        function buscarNombre(lista,nombre){
            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.nombre==nombre){
                    obj=objeto;
                }
            });
            return obj;
        }

        function estadoTrabajo(fase){

            var resp=$scope.jobej
            if (fase==$scope.ejecucion){
                resp=$scope.jobej;
            }else if(fase==$scope.estado){
                resp=$scope.jobep;
            }else if(fase==$scope.facturacion){
                resp=$scope.jobfa;
            }
            return resp;
        }

        function senddummy(){
            $('#validacion').modal('close');
            $scope.validacion.fecha_creacion=new Date();
            var ms=$scope.validacion.fecha_inicio._d;
            var msf=$scope.validacion.fecha_fin._d;
            $scope.validacion.fecha_inicio=ms;
            $scope.validacion.fecha_fin=msf;
            $scope.validacion.estado=$scope.resultado.id;
            $scope.fase.codigo_SAP=undefined;
            gestionServ.postPaso($scope.validacion).then(function(ree){
                if ($scope.fase.nombre==$scope.ejecucion){
                    if ($scope.validacion.estado==2){//Salio mal
                        $scope.fase.estado_programa=$scope.stateco;
                        $scope.fase.num_reprog=$scope.fase.num_reprog+1;
                        gestionServ.putPrograma($scope.fase).then(reloadInfo);
                    }else{ //Salio bien
                        $scope.fase.estado_programa=$scope.statevj;
                        gestionServ.putPrograma($scope.fase).then(reloadInfo);
                    }

                }else{
                    if ($scope.validacion.estado==2){
                        $scope.fase.estado_programa=$scope.stater;
                        $scope.fase.num_reprog=$scope.fase.num_reprog+1;
                        gestionServ.putPrograma($scope.fase).then(reloadInfo);
                    }else{
                        if ($scope.fase.nombre==$scope.facturacion){
                            //PROCESO DE CIERRE DE TRABAJO, ETAPA DE OBRA Y OBRA

                            $scope.fase.estado_programa=$scope.stateclosed;//Fase cerrada
                            gestionServ.putPrograma($scope.fase).then(function(reb){
                                //cierro la fase
                                $scope.previo.trabajo.estado_programa=$scope.jobclosed;//Trabajo cerrado
                                gestionServ.putPrograma($scope.previo.trabajo).then(function(reba){
                                    //cierro el trabajo
                                    gestionServ.getProgramaMedioId($scope.previo.trabajo.id_raiz).then(function(rew){
                                        var obra=rew.data;//Obra es padre de trabajo: Etapa de obra
                                        //miro si todos los trabajos estan cerrrados
                                        var tcerra=verificarEstados(obra.sons,$scope.jobclosed);
                                        if (tcerra==true){
                                            var obrac=copiarPrograma(obra);
                                            obrac.estado_programa=$scope.eoclosed;//Etapa de obra cerrada
                                            gestionServ.putPrograma(obrac).then(function(reb){
                                                //cierro la etapa obra
                                                gestionServ.getProyecto(obrac.id_raiz).then(function(rez){
                                                    var proyecto=rez.data;//Proyecto es obra ahora
                                                    //miro si todas las etapas obras estan cerradas
                                                    var ocerra=verificarEstados(proyecto.sons,$scope.eoclosed);
                                                    if(ocerra==true){
                                                        var project=copiarPrograma(proyecto);
                                                        project.estado_programa=$scope.oclosed;//Obra cerrada
                                                        gestionServ.putPrograma(project).then(function(reb){
                                                            //cierro la obra
                                                            reloadInfo();
                                                        });
                                                    }else{
                                                        //quedan etapas de obra
                                                        reloadInfo();
                                                    }
                                                });

                                            });
                                        }else{
                                            //quedan trabajos
                                            reloadInfo();
                                        }
                                    });
                                });
                            });

                        }else{
                            $scope.fase.estado_programa=$scope.stateclosed;//Fase cerrada
                            gestionServ.putPrograma($scope.fase).then(function(rec){
                                gestionServ.getProgramaMedioId($scope.fase.id_raiz).then(function(res){
                                    var trabajo=res.data;
                                    var nextf=nextFase($scope.fase.nombre);
                                    var startf=startFase(nextf);
                                    var nfase=buscarNombre(trabajo.sons,nextf);
                                    if(nfase.id>0){
                                        var estj=estadoTrabajo(nextf);
                                        $scope.previo.trabajo.estado_programa=estj;//Trabajo estado nuevo
                                        gestionServ.putPrograma($scope.previo.trabajo).then(function(rexa){
                                            nfase.estado_programa=startf;
                                            gestionServ.putPrograma(nfase).then(function(rea){
                                                reloadInfo();
                                            });
                                        });
                                    }else{
                                        alert("Siguiente fase no encontrada, pide soporte");
                                        reloadInfo();
                                    }
                                });
                            });
                        }
                    }
                }
            });

        }

    }
])


.controller('jsAsignarGPCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, programasServ, usuariosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        loadInfo();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:120},
                {field:'nombre', displayName:'Nombre', minWidth:300},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:150},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:190},
                {name: 'GP', width:160, headerCellTemplate: '<br><div class ="center">Asignar<br>Supervisor</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openAsignarGP(row.entity)"><i class="material-icons right">input</i>Asignar</button>' },
            ]
        };

        //Cargue info necesaria
        function loadInfo() {
            generalFunc.routeJs()
            loadProyectos()
            loadGPs()
        }

        //LoadProyectos sin GP Asignado
        function loadProyectos() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo(js, 1, 'proyecto')
                .then(function(res){
                    $scope.tablaOp.data = res.data
                })
        }

        //Load GPs disponibles
        function loadGPs() {
            usuariosServ.getRolByName('gp')
                .then(function(res) {
                    $scope.GPs = res.data[0].user;
                })
        }

        //Modal
        $scope.openAsignarGP = function(proyecto) {
            $scope.proyecto = proyecto;
            $('#asignarGP').modal('open');
        }

        $scope.closeAsignarGP = function() {
            $scope.proyecto = [];
            $('#asignarGP').modal('close');
        }

        //Asignar Gestor de Proyecto
        $scope.asignarGP = function(proyecto) {
            proyecto.estado_programa = 2;
            _.forEach(proyecto.sons, updateObras)
            programasServ.putPrograma(proyecto)
                .then(successGP)
        }
        //Actualiza GP a las obras
        function updateObras(obra){
            obra.id_gp_asignado = $scope.proyecto.id_gp_asignado;
            obra.estado_programa = 1;
            programasServ.putPrograma(obra)
                .then(function(res){})
        }

        function successGP(res) {
            loadProyectos();
            $scope.proyecto = []
            $('#asignarGP').modal('close');
            var $toastContent = $('<span>Supervisor Asignado éxitosamente!</span>');
            Materialize.toast($toastContent, 3000);
        }

        $scope.cerrarSession = function(){
            generalFunc.cerrarSesion()
        }
    }
])
.controller('jsAsignaTrabajosCtrl', ['$scope', '$state', 'programasServ', 'generalServ', 'pasosServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, programasServ, generalServ, pasosServ, usuariosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs();
        loadObras();

        //Definición ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'obras.xlsx',
            exporterExcelSheetName: 'obras',
            columnDefs:[
                {field:'nombre', displayName:'Nombre', minWidth:250},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'Contratista', minWidth:140},
                {field:'fecha_limite', displayName:'Fecha Límite', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\'', minWidth:190},
                { name: 'AsignarTrabajos', width:160, headerCellTemplate: '<br><div class ="center">Asignar<br>Trabajos a Obra</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openAsignarTrabajo(row.entity)"><i class="material-icons right">input</i>Asignar</button>' },
            ]
        };

        //Cargue info ui-grid
        function loadObras() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo2(js, 1, 'obra')
                .then(function(res){
                    $scope.tablaOp.data = _.orderBy(res.data, ['fecha_limite'], ['asc']);
                })
            moment.locale('es');
            $scope.hoy = moment().format("YYYY-MM-DD");
        }

        //Modal
        $scope.openAsignarTrabajo = function(obra) {
            $scope.obra = obra;
            $scope.obra.trabajos = [{}];
            $scope.obra.trabajos[0].nombre = 'TRABAJO ' + obra.nombre;
            $scope.obra.trabajos[0].uucc = Number(obra.uucc);
            $scope.obra.trabajos[0].fecha_inicio = new Date();
            $scope.obra.trabajos[0].fecha_fin = new Date(obra.fecha_limite);
            $scope.obra.trabajos[0].comentarios = obra.comentarios;
            $('#obraTrabajos').modal('open');
        };

        $scope.closeAsignarTrabajoModal = function() {
            $('#obraTrabajos').modal('close');
        };

        //Crear Trabajos
        $scope.crearTrabajos = function(obra) {
            var uucc_totales = _.sumBy($scope.obra.trabajos, 'uucc');
            if(uucc_totales == Number($scope.obra.uucc)){
                //Se construye json trabajos
                _.forEach(obra.trabajos, buildJSONTrabajos);
                //Se crean trabajos
                programasServ.postPrograma($scope.obra.trabajos)
                    .then(propuestaTrabajo)
                //Se actualiza estado de obra
                obra.estado_programa = 3;
                programasServ.putPrograma(obra)
                    .then(function (res) {loadObras()})
                $('#obraTrabajos').modal('close');
            }else{
                var $toastContent = $('<span>Las UUCC deben corresponder con las UUCC Obra</span>');
                Materialize.toast($toastContent, 3000);
            }
        }

        function buildJSONTrabajos(trabajo, i) {
            $scope.obra.trabajos[i].tipo = 'trabajo';
            $scope.obra.trabajos[i].area_servicio = $scope.obra.area_servicio;
            $scope.obra.trabajos[i].id_raiz = $scope.obra.id;
            $scope.obra.trabajos[i].id_locacion = $scope.obra.id_locacion;
            $scope.obra.trabajos[i].nombre_locacion = $scope.obra.nombre_locacion;
            $scope.obra.trabajos[i].id_usuario_creacion = window.localStorage['id_user'];
            $scope.obra.trabajos[i].id_js_asignado = $scope.obra.id_js_asignado;
            $scope.obra.trabajos[i].id_gp_asignado = $scope.obra.id_gp_asignado;
            $scope.obra.trabajos[i].id_eecc_asignado = $scope.obra.id_eecc_asignado;
            $scope.obra.trabajos[i].estado_programa = 1;
        }

        //Propuesta de cada trabajo creado
        function propuestaTrabajo(res) {
            $scope.propTrabajo = [];
            //Se contruye json de propuesta y se crean fases
            _.forEach(res.data, buildPropuestaYPostFases);
            // Se crean propuestas de trabajos
            pasosServ.postPaso($scope.propTrabajo)
                .then(function(res) {})
        }

        //Construye Json propuesta y crea fases del trabajo
        function buildPropuestaYPostFases(trabajo, i) {
            $scope.propTrabajo = [{}];
            $scope.propTrabajo[i].id_programa = trabajo.id;
            $scope.propTrabajo[i].tipo = 'propuesta';
            $scope.propTrabajo[i].fecha_inicio = trabajo.fecha_inicio;
            $scope.propTrabajo[i].fecha_fin = generalFunc.getMaxHour(trabajo.fecha_fin);
            $scope.propTrabajo[i].comentario = trabajo.comentarios;
            // Se construyen fases para el trabajo
            var fases = generalFunc.buildJsonFases(trabajo)
            // Se crean fases para trabajo
            programasServ.postPrograma(fases)
                .then(function(res) {
                    var $toastContent = $('<span>Trabajos Asignados existosamente</span>');
                    Materialize.toast($toastContent, 3000);
                })
        }

        $scope.validarUUCC = function(obra){
            var uucc_totales = _.sumBy(obra.trabajos, 'uucc');
            $scope.obra.trabajos[obra.trabajos.length-1].uucc = Number(obra.uucc) - uucc_totales;

        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion();
        }
    }
])
.controller('jsCambiarGPCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, programasServ, usuariosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        loadInfo();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:120},
                {field:'nombre', displayName:'Nombre', minWidth:300},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:150},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"',minWidth:190},
                {name: 'Cambiar', width:160, headerCellTemplate: '<br><div class ="center">Cambiar<br>Supervisor</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openCambiarGP(row.entity)"><i class="material-icons right">mode_edit</i>Cambiar</button>' },
            ]
        };

        //Cargue info necesaria
        function loadInfo() {
            generalFunc.routeJs()
            loadProyectos();
            loadGPs();
        }

        //LoadProyectos sin GP Asignado
        function loadProyectos() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo(js, 2, 'proyecto')
                .then(function(res){
                    $scope.tablaOp.data = res.data
                })
        }

        //Load GPs disponibles
        function loadGPs() {
            usuariosServ.getRolByName('gp')
                .then(function(res) {
                    $scope.GPs = res.data[0].user;
                })
        }

        //Modal
        $scope.openCambiarGP = function(proyecto) {
            $scope.proyecto = proyecto;
            loadScopes(proyecto)
            $('#cambiarGP').modal('open');
        }

        function loadScopes(proyecto) {
            usuariosServ.getUsuarioById(proyecto.id_gp_asignado)
                .then(function(res) {
                    $scope.gp_asignado = res.data.name;
                })
        }

        $scope.closeCambiarGP = function() {
            $scope.proyecto = [];
            $('#cambiarGP').modal('close');
        }

        //Cambiar Gestor de Proyecto
        $scope.cambiarGP = function(proyecto) {
            _.forEach(proyecto.sons, updateObrasGP)
            programasServ.putPrograma(proyecto)
                .then(successGP)
        }

        function updateObrasGP(obra){
            obra.id_gp_asignado = $scope.proyecto.id_gp_asignado;
            programasServ.putPrograma(obra)
                .then(function(res){})
        }

        function successGP(res) {
            $scope.proyecto = []
            $('#cambiarGP').modal('close');
            var $toastContent = $('<span>Supervisor Actualizado éxitosamente!</span>');
            Materialize.toast($toastContent, 3000);
        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }
    }
])
.controller('jsCompromisoCtrl', ['$scope', '$state', 'programasServ', 'pasosServ', 'generalFunc', 'generalServ',
    function($scope, $state, programasServ, pasosServ, generalFunc, generalServ) {

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs()
        loadTrabajos();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'trabajos.xlsx',
            exporterExcelSheetName: 'trabajos',
            columnDefs:[
                {field:'parent.nombre', displayName:'Nombre', minWidth:250},
                {field:'parent.nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'parent.area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'parent.uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'Contratista', minWidth:140},
                {field:'parent.fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'dd-MM-yyyy  h:mma\':"UTC"', minWidth:190},
                { name: 'Compromiso', width:160, headerCellTemplate: '<br><div class ="center">Generar<br>Compromiso</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openCrearCompromiso(row.entity)"><i class="material-icons right">input</i>Generar</button>' },
            ]
        };

        //Cargue info necesaria
        function loadTrabajos() {
            var userId = window.localStorage['id_user'];
            programasServ.getProgramaByJSEstadoTipo3(userId, 3, 'fase')
                .then(function(res) {
                    $scope.tablaOp.data = res.data;
                })
            moment.locale('es');
            $scope.hoy = moment();
        }

        //Modal
        $scope.openCrearCompromiso = function(fase) {
            $scope.fase = fase;
            $scope.fase.compromiso = {};
            getPropuestaTrabajo(fase.parent.id);
            getProyecto(fase.parent.id_raiz);
            if(!_.isEmpty(fase.pasos) && fase.nombre == 'Ejecución'){
                $scope.contrapropPrevia = true;
                $scope.validacionPrevia = false;
                //$scope.fase.propuesta = _.find(fase.pasos, {'tipo': 'propuesta'});
                var contrapropuestas = _.filter(fase.pasos, {'tipo': 'contrapropuesta'});
                $scope.fase.contrapropuesta = _.maxBy(contrapropuestas, 'fecha_creacion');
                generalServ.getBrigade(fase.contrapropuesta.id_brigada)
                    .then(function (r) { $scope.fase.contrapropuesta.brigada = r.data.nombre })
                $scope.fase.compromiso.fecha_inicio = new Date($scope.fase.contrapropuesta.fecha_inicio);
                $scope.fase.compromiso.fecha_fin = new Date($scope.fase.contrapropuesta.fecha_fin);
                var validacion_supervisor = _.filter(fase.pasos, {'tipo': 'validacions'});
                if(validacion_supervisor.length != 0){
                    $scope.contrapropPrevia = false;
                    $scope.validacionPrevia = true;
                    $scope.fase.validacion_supervisor = validacion_supervisor[0];
                }
            }else{
                $scope.contrapropPrevia = false;
                $scope.validacionPrevia = false;
            }
            $('#compromiso').modal('open');
        }

        function getPropuestaTrabajo(id_trabajo) {
            pasosServ.getPasosByIdPrograma(id_trabajo)
                .then(function(res){
                    $scope.propTrabajo = res.data[0];
                })
        }

        function getProyecto(id_obra) {
            programasServ.getProgramasById(id_obra)
                .then(function (res) {
                    $scope.proyecto = res.data[0].parent;
                })
        }

        $scope.closeCompromiso = function() {
            $('#compromiso').modal('close');
            $scope.fase = [];
        }

        //Crear Compromiso
        $scope.crearCompromiso = function(fase) {
            fase.compromiso.fecha_inicio = fase.compromiso.fecha_inicio.toDate()
            fase.compromiso.fecha_fin = fase.compromiso.fecha_fin.toDate()
            // Si se aprueba compromiso
            if(fase.compromiso.estado == "1" || fase.compromiso.estado == undefined){
                // Si es ejecucion se asigna brigada de la contrapropuesta
                if(fase.nombre == 'Ejecución'){
                    fase.compromiso.id_brigada = fase.contrapropuesta.id_brigada;
                }
                fase.compromiso.id_programa = fase.id;
                fase.compromiso.estado = 0;
                fase.compromiso.tipo = 'compromiso';
                // Se crea el compromiso
                pasosServ.postPaso(fase.compromiso)
                    .then(successCompromiso)
                // Se actualiza estado de la fase
                updateEstadoFase(fase, 4);
                $('#compromiso').modal('close');
            }else {
                updateEstadoFase(fase, 2);
            }
        }

        function successCompromiso(res) {
            $scope.fase = []
            $('#compromiso').modal('close');
            var $toastContent = $('<span>Compromiso generado éxitosamente!</span>');
            Materialize.toast($toastContent, 3000);
        }

        function updateEstadoFase(fase, est){
            fase.estado_programa = est;
            programasServ.putPrograma(fase)
                .then(function(res){
                    loadTrabajos()
                    $('#compromiso').modal('close');
                    if(est == 2){
                        var $toastContent = $('<span>Contrapropuesta rechazada!</span>');
                        Materialize.toast($toastContent, 3000);
                    }
                })
        }

        $scope.setFechasCompromiso = function(res) {
            if(res == 1) {
                $scope.fase.compromiso.fecha_inicio = new Date($scope.fase.contrapropuesta.fecha_inicio);
                $scope.fase.compromiso.fecha_fin = new Date($scope.fase.contrapropuesta.fecha_fin);
            } else {
                $scope.fase.compromiso.fecha_inicio = null;
                $scope.fase.compromiso.fecha_fin = null;
            }
        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])
.controller('jsDetalleObrasCtrl', ['$scope', '$state', 'programasServ', 'generalServ', 'usuariosServ', 'generalFunc',
  function($scope, $state, programasServ, generalServ, usuariosServ, generalFunc) {

    $('.modal').modal({});
    $('.collapsible').collapsible();

    generalFunc.routeJs()
    loadObras();

    //Definicion ui-grid
    $scope.tablaOp={
        enableFiltering: true,
        enableRowSelection: true,
        multiSelect: false,
        enableColumnResizing:true,
        enableGridMenu:true,
        showColumnFooter:true,
        showGridFooter: true,
        enableColumnMoving:true,
        exporterExcelFilename: 'proyectos.xlsx',
        exporterExcelSheetName: 'proyectos',
        columnDefs:[
            {field:'nombre', displayName:'Nombre', minWidth:300},
            {field:'nombre_locacion', displayName:'Ubicación', minWidth:130},
            {field:'eecc.name', displayName:'Contratista', minWidth:130},
            {field:'uucc', displayName:'UUCC', minWidth:100},
            {field:'fecha_ingreso', displayName:'Fecha Creación', minWidth:180, cellFilter: 'date:\'dd-MM-yyyy  h:mma\':"UTC"'},
            {field:'fecha_limite', displayName:'Fecha Límite', minWidth:180, cellFilter: 'date:\'dd-MM-yyyy  h:mma\''},
        ]
    };

    function loadObras() {
        var js = window.localStorage['id_user']
        programasServ.getProgramaByJSEstadoTipo2(js, 3, 'obra')
            .then(function (res){
                $scope.tablaOp.data = res.data;
            })
    }

    //Seleccion fila ui-grid
    $scope.tablaOp.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(row) {
            $scope.detalleObra = row.entity;
            $scope.detalleTrabajos = row.entity.sons;
            _.forEach($scope.detalleTrabajos, function (trabajo, i){
                $scope.detalleTrabajos[i].estado_programa = generalFunc.getEstadoTrabajo(trabajo.estado_programa)
            })
            $('#detalleObra').modal('open');
            var propuesta = _.filter($scope.detalleObra.pasos, {'tipo': 'propuesta'});
            $scope.detalleObra.propuesta = propuesta[0];
        })
    }

    $scope.cerrarSession = function() {
        generalFunc.cerrarSesion()
    }

  }
])
.controller('jsHomeCtrl', ['$scope', '$state', 'usuariosServ', 'programasServ', 'generalServ', 'generalFunc',
  function($scope, $state, usuariosServ, programasServ, generalServ, generalFunc){

    generalFunc.routeJs()
    countObras();
    loadUser();

    //Contador de las obras en sus diferentes estados
    function countObras(){
        var js = window.localStorage['id_user']
        programasServ.countProgramaByJSEstadoTipo(js, 1, 'proyecto')
            .then(function(res){
                $scope.countSinGP = res.data.count;
            })
        /*programasServ.countProgramaByJSEstadoTipo(js, 1, 'obra')
            .then(function(res){
                $scope.countSinPropuesta = res.data.count;
            })*/
        programasServ.countProgramaByJSEstadoTipo(js, 1, 'obra')
            .then(function(res){
                $scope.countSinTrabajos = res.data.count;
            })
        programasServ.countProgramaByJSEstadoTipo(js, 3, 'obra')
            .then(function(res){
                $scope.countDetalleObras = res.data.count;
            })
        programasServ.countProgramaByJSEstadoTipo(js, 3, 'fase')
            .then(function(res){
                $scope.countCompromiso = res.data.count;
            })
        programasServ.countProgramaByJSEstadoTipo(js, 6, 'fase')
            .then(function(res){
                $scope.countValidar = res.data.count;
            })
    }

    //Carga info usuario
    function loadUser(){
        var js = window.localStorage['id_user']
        usuariosServ.getUsuarioById(js)
            .then(setZona)
    }

    function setZona(res){
        $scope.usuario = res.data;
        generalServ.getLocacionesbyId(res.data.id_locacion)
            .then(function(res){
                $scope.usuario.zona = res.data.nombre;
            })
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion()
    }

  }
])
.controller('jsPropuestaCtrl', ['$state', '$scope', 'usuariosServ', 'programasServ', 'pasosServ', 'generalFunc',
    function($state, $scope, usuariosServ, programasServ, pasosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs();
        loadObras();
        setHoy();

        //Definición ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'obras.xlsx',
            exporterExcelSheetName: 'obras',
            columnDefs:[
                {field:'nombre', displayName:'Nombre', minWidth:250},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'Contratista', minWidth:140},
                {field:'fecha_limite', displayName:'Fecha Límite', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\'',minWidth:190},
                { name: 'Proponer', width:160, headerCellTemplate: '<br><div class ="center">Proponer<br>Fechas y Balancear</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openProponerObra(row.entity)"><i class="material-icons right">input</i>Proponer</button>' },
            ]
        };

        //Cargue info ui-grid
        function loadObras() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo(js, 1, 'obra')
                .then(function(res){
                    $scope.tablaOp.data = _.orderBy(res.data, ['fecha_limite'], ['asc']);
                })
        }

        function setHoy() {
            moment.locale('es');
            $scope.hoy = moment().format("YYYY-MM-DD");
        }

        //Modal
        $scope.openProponerObra = function(obra) {
            $scope.obra = obra;
            $('#asignarObra').modal('open');
        }

        $scope.closeProponerObra = function() {
            $scope.obra = [];
            $('#asignarObra').modal('close');
        }

        //Crear propuesta
        $scope.crearPropuesta = function(obra) {
            var propuesta = obra.propuesta;
            $scope.obra.propuesta.id_programa = obra.id;
            $scope.obra.propuesta.tipo = 'propuesta';
            propuesta.fecha_fin = generalFunc.getMaxHour(propuesta.fecha_fin);
            pasosServ.postPaso(propuesta)
                .then(successPropuesta, errorPropuesta);
            var actualizado = [{"id": obra.id, "estado_programa": 2}];
            programasServ.updatePrograma(actualizado[0])
                .then(function(res){});
            loadObras();
        }

        function successPropuesta(res) {
            var $toastContent = $('<span>Propuesta generada éxitosamente!</span>');
            Materialize.toast($toastContent, 4000);
            $scope.closeProponerObra();
            $scope.tablaOp.data = [];
            loadObras();
        }

        function errorPropuesta(res) {
            var $toastContent = $('<span>Error al generar la propuesta!</span>');
            Materialize.toast($toastContent, 4000);
        }

        //Datepicker
        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Cerrar',
            selectMonths: true,
            selectYears: true,
            container: 'body',
            format: 'yyyy-mm-dd'
        });

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])
.controller('jsValidarCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'pasosServ', 'generalFunc',
    function($scope, $state, programasServ, usuariosServ, pasosServ, generalFunc){

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs();
        loadTrabajos();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'trabajos.xlsx',
            exporterExcelSheetName: 'trabajos',
            columnDefs:[
                {field:'parent.nombre', displayName:'Nombre', minWidth:250},
                {field:'parent.nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'parent.area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'parent.uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'EECC', minWidth:140},
                {field:'parent.fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:190},
                { name: 'Validar', width:160, headerCellTemplate: '<br><div class ="center">Validar<br>Fase</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openValidar(row.entity)"><i class="material-icons right">input</i>Validar</button>' },
            ]
        };

        //Cargue info ui-grid
        function loadTrabajos(){
            var userId = window.localStorage['id_user'];
            programasServ.getProgramaByJSEstadoTipo3(userId, 6, 'fase')
                .then(function(res){
                    $scope.tablaOp.data = res.data;
                })
        }

        //Modal
        $scope.openValidar = function(fase) {
            $scope.trabajo = fase.parent;
            programasServ.getProgramasById(fase.parent.id_raiz)
                .then(function (res) {
                    $scope.proyecto = res.data[0].parent
                })
            $scope.fase = fase;
            //setSiguienteTrabajo(fase.parent.id_raiz, fase.parent.num_reprog);
            var compromisos = _.filter(fase.pasos, {'tipo': 'compromiso'});
            $scope.fase.compromiso = _.maxBy(compromisos, 'fecha_creacion');
            var validacion_supervisor = _.filter(fase.pasos, {'tipo': 'validacions'});
            $scope.fase.validacion_supervisor = _.maxBy(validacion_supervisor, 'fecha_creacion');
            $('#validar').modal('open');
            getObras();
        }

        /*function setSiguienteTrabajo(id_raiz, orden) {
            programasServ.getProgramasByIdRaiz(id_raiz)
                .then(function(res){
                    $scope.siguienteTrabajo = _.find(res.data, {'num_reprog': orden + 1 })
                })
        }*/

        function getObras(){
            programasServ.getProgramasById($scope.trabajo.id_raiz)
                .then(function(res){
                    $scope.obra = res.data;
                    programasServ.getProgramasByIdRaiz(res.data[0].parent.id)
                        .then(function (res){
                            $scope.obras = res.data;
                        })
                })
        }

        $scope.closeValidar = function() {
            $('#validar').modal('close');
            $scope.fase = [];
        }

        //Crear validacion
        $scope.crearValidacion = function(fase) {
            if(fase.validacion.estado == "1"){
                fase.validacion.id_programa = fase.id;
                fase.validacion.tipo = 'validacion';
                fase.validacion.id_compromiso = $scope.fase.compromiso.id;
                pasosServ.postPaso(fase.validacion)
                    .then(successValidacion)
                programasServ.getProgramasByIdRaiz(fase.parent.id)
                    .then(updateEstados)

                updateEstadoFase(fase, 7);
            }else{
                updateEstadoFase(fase.id, 3);
            }
        }

        function successValidacion(res) {
            loadTrabajos()
            $('#validar').modal('close');
            var $toastContent = $('<span>Validación exitosa!</span>');
            Materialize.toast($toastContent, 4000);
        }

        function updateEstados(fases){
            var ejecucion = _.find(fases.data, {'nombre': 'Ejecución'});
            var materiales = _.find(fases.data, {'nombre': 'Cuadratura de Materiales'});
            var pago = _.find(fases.data, {'nombre': 'Estado de Pago'});
            //var facturacion = _.find(fases.data, {'nombre': 'Facturación'});
            if($scope.fase.nombre == ejecucion.nombre && $scope.fase.nombre=='Ejecución'){
                //Se activa siguiente trabajo
                /*if($scope.siguienteTrabajo){
                    updateEstadoTrabajo($scope.siguienteTrabajo.id, 1)
                    var fase = _.find($scope.siguienteTrabajo.sons,{'nombre': 'Ejecución' })
                    updateEstadoFase(fase.id, 1)
                }*/
                // Si tiene liquidacion
                if(!_.isEmpty(materiales)){
                    updateEstadoTrabajo(materiales.parent, 2)
                    updateEstadoFase(materiales, 3)
                }else{
                    updateEstadoTrabajo(pago.parent, 3)
                    updateEstadoFase(pago, 3)
                }
            }/*else if($scope.fase.nombre==materiales.nombre){
                updateEstadoTrabajo(facturacion.id_raiz, 3)
                updateEstadoFase(facturacion.id, 3)
            } else if($scope.fase.nombre==pago.nombre){

            }
            else if($scope.fase.nombre == facturacion.nombre && $scope.fase.nombre=='Facturación'){
                updateEstadoTrabajo(facturacion.id_raiz, 4)
                if(!$scope.siguienteTrabajo){
                    updateEstadoObra($scope.obra[0].id, 4)
                    validarUltimaObra();
                }
            }*/
        }

        function updateEstadoFase(fase, est){
            fase.estado_programa = est;
            programasServ.updatePrograma(fase)
                .then(function(res){})
        }

        function updateEstadoTrabajo(trabajo, est){
            trabajo.estado_programa = est;
            programasServ.updatePrograma(trabajo)
                .then(function(res){})
        }

        /*function updateEstadoObra(id, est){
            var trabajo = [{"id": id, "estado_programa": est}];
            programasServ.updatePrograma(trabajo[0])
                .then(function(res){})
        }

        function validarUltimaObra(){
            var ejecutadas = 0;
            _.forEach($scope.obras, function(obra){
                if(obra.estado_programa == 4){
                    ejecutadas = ejecutadas + 1;
                }
            })
            if(ejecutadas == $scope.obras.length - 1){
                var id_proyecto = $scope.obra[0].parent.id;
                var trabajo = [{"id": id_proyecto, "estado_programa": 3, "fecha_cierre": moment().toDate()}];
                programasServ.updatePrograma(trabajo[0])
                    .then(function(res){})
            }
        }*/

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])
.controller('backlogCtrl', ['$scope', '$stateParams', 'backlogsServ', 'usuariosServ', 'generalFunc',
    function($scope, $stateParams, backlogsServ, usuariosServ, generalFunc){

        $('.modal').modal(
            {
                dismissible: false
            }
        );

        getBacklogs();

        function getBacklogs() {
            if($stateParams.id){
                backlogsServ.getBacklogsById($stateParams.id)
                .then(function(res) {
                    $scope.backlogDetail = res.data;
                    $scope.detail = true;
                    $scope.backlogDetail.sons = _.orderBy($scope.backlogDetail.sons,['fecha_creacion'],['desc'])
                    _.forEach(res.data.sons, setUser);
                })
            } else {
                backlogsServ.getBacklogs()
                .then(function(res) {
                    $scope.detail = false;
                    $scope.backlogs = res.data;
                })
            } 
        }

        function setUser(user, i) {
            usuariosServ.getUsuarioById(user.id_usuario)
                .then(function (res) {
                    $scope.backlogDetail.sons[i].username = res.data.name;
                })
        }

        $scope.crearSugerencia = function(backlog) {
            backlog.id_usuario = Number(window.localStorage['id_user']);
            backlogsServ.postBacklogs(backlog)
                .then(function(res){
                    getBacklogs();
                    $('#feedModal').modal('close');
                    $scope.backlog = null;
                })
        }

        $scope.vote = function(b) {
            b.likes ++
            backlogsServ.putBacklogs(b)
                .then(function(res){})
        }

        $scope.saveComment = function(id, comment) {
            $scope.comment = '';
            var bd = {};
            bd.id_usuario = window.localStorage['id_user'];
            bd.id_backlog = id;
            bd.descripcion = comment;
            backlogsServ.postBacklogs(bd)
                .then(function(res){
                    getBacklogs();
                })
        }

        $scope.cancelar = function() {
            $('#feedModal').modal('close');
            $scope.backlog = null;
        }

        $scope.cerrarSession = function(){
            generalFunc.cerrarSesion()
        }

    }
])
.controller('adminBacklogCtrl', ['$scope', '$state', '$stateParams', 'backlogsServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, $stateParams, backlogsServ, usuariosServ, generalFunc){

        $('.modal').modal(
            {
                dismissible: false
            }
        );

        if (window.localStorage['rol'] == "admin") {
            //$state.go("perfil_admin");
        } else {
            $state.go("home");
        }

        getBacklogs();

        function getBacklogs() {
            if($stateParams.id){
                backlogsServ.getBacklogsById($stateParams.id)
                .then(function(res) {
                    $scope.backlogDetail = res.data;
                    $scope.detail = true;
                    $scope.backlogDetail.sons = _.orderBy($scope.backlogDetail.sons,['fecha_creacion'],['desc'])
                    _.forEach(res.data.sons, setUser);
                })
            } else {
                backlogsServ.getBacklogs()
                .then(function(res) {
                    $scope.detail = false;
                    $scope.backlogs = res.data;
                })
            } 
        }

        function setUser(user, i) {
            usuariosServ.getUsuarioById(user.id_usuario)
                .then(function (res) {
                    $scope.backlogDetail.sons[i].username = res.data.name;
                })
        }

        $scope.crearSugerencia = function(backlog) {
            backlog.id_usuario = Number(window.localStorage['id_user']);
            backlogsServ.postBacklogs(backlog)
                .then(function(res){
                    getBacklogs();
                    $('#feedModal').modal('close');
                    $scope.backlog = null;
                })

        }

        $scope.vote = function(b) {
            b.likes ++
            backlogsServ.putBacklogs(b)
                .then(function(res){})
        }

        $scope.saveComment = function(id, comment) {
            $scope.comment = '';
            var bd = {};
            bd.id_usuario = window.localStorage['id_user'];
            bd.id_backlog = id;
            bd.descripcion = comment;
            backlogsServ.postBacklogs(bd)
                .then(function(res){
                    getBacklogs();
                })
        }

        $scope.viewDetail = function(b) {
            $scope.priorities = [1, 2, 3, 4, 5];
            $scope.feedDetail = b;
            $('#editModal').modal('open');
        }

        $scope.updateFeedback = function (fd) {
            backlogsServ.putBacklogs(fd)
                .then(function(res){})
            $scope.cancelEdit();  
        }

        $scope.deleteFeedback = function(id) {
            $scope.id = id;
            swal({
                title: 'Atención!',
                text: '¿Está seguro de eliminar la sugerencia?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).
            then(function(res) {
                if(res) {
                    backlogsServ.deleteSonsBacklogs(id)
                    .then(function(res){
                        backlogsServ.deleteBacklog($scope.id)
                            .then(function(res){
                                getBacklogs();
                            })
                    })
                }
            })
        }

        $scope.cancelar = function() {
            $('#feedModal').modal('close');
            $scope.backlog = null;
        }

        $scope.cancelEdit = function() {
            $('#editModal').modal('close');
            $scope.feedDetail = null;
        }

        $scope.cerrarSession = function(){
            generalFunc.cerrarSesion()
        }

    }
])
.controller('homeAdminCtrl', ['$scope', '$state', 'usuariosServ', 'generalFunc', 'programasServ',
  function($scope, $state, usuariosServ, generalFunc, programasServ) {

  loadScopes();

  function loadScopes() {
    generalFunc.routeAdmin();
    loadNumeroUsuarios();
    loadProyectosCerrados();
  }


    function loadNumeroUsuarios() {
      usuariosServ.getNumeroUsuarios()
        .then(function(res){
            $scope.numeroUsuarios = res.data.count;
        });
    }

    function loadProyectosCerrados(res) {
        programasServ.countProyectoCerrados(3)
            .then(function(res){
                $scope.proyectos = res.data.count;
            })
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion()
    }

  }
])

.controller('adminProyectosCerrados', ['$scope','$state', 'generalFunc', 'programasServ',
    function($scope, $state, generalFunc, programasServ) {

        $('.modal').modal();
        $('.collapsible').collapsible();
        $('select').material_select();

        //Definición Ui-Grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:110},
                {field:'nombre', displayName:'Nombre', minWidth:280},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:100},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:170},
                {field:'fecha_cierre', displayName:'Fecha Cierre', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\'', minWidth:170},
                { name: 'Asignar', width:160, headerCellTemplate: '<br><div class ="center">Editar<br> Obras</div>',
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openAsignarObrasModal(row.entity)"><i class="material-icons right">delete_forever</i>Eliminar</button>' }
            ]
        };

        generalFunc.routeAdmin();
        loadClosedProyecto();

        //Cargue info ui-grid
        function loadClosedProyecto() {
            programasServ.getProyectoCerrados(3, 'proyecto')
                .then(function(res){
                    $scope.tablaOp.data = res.data
                });
        }

    }
])
.controller('adminResetPassCtrl', ['$scope', '$state', 'usuariosServ',
    function($scope, $state, usuariosServ){

        $('.modal').modal({
            dismissible: false
        });

        $('#newPass').modal('open');

        $scope.formSend = true;

        if (window.location.href.slice('51') == "") {
            $scope.request = true;
        } else {
            $scope.request = false;
            window.localStorage.setItem('token', window.location.href.slice('51'));
        }

        $scope.resetPassword = function(user){
            usuariosServ.resetPasswd(user)
                .then(success, error)
        }

        function success(res) {
            if (res.status == 204){
                $scope.formSend = false;
            } else {
                var $toastContent = $('<span>La dirección de correo electrónico no está registrada en nuestra base de datos.</span>');
                Materialize.toast($toastContent, 3000);
                $scope.cancelar();
            }
        }

        function error(res){
            var $toastContent = $('<span>La dirección de correo electrónico no está registrada en nuestra base de datos.</span>');
            Materialize.toast($toastContent, 3000);
            $scope.cancelar();
        }

        $scope.changePassword = function(new_passwd) {
            usuariosServ.changePasswd(new_passwd)
                .then(function(res){
                    var $toastContent = $('<span>Contraseña actualizada éxitosamente!</span>');
                    Materialize.toast($toastContent, 3000);
                    $scope.cancelar()
                })
        }

        $scope.cancelar = function(){
            $('#newPass').modal('close');
            $state.go('home');
        }
    }
])
.controller('adminUsuariosCtrl', ['$scope', '$state', 'usuariosServ', 'generalFunc', 'generalServ',
  function($scope, $state, usuariosServ, generalFunc, generalServ){

    $('.modal').modal(
      {
        dismissible: false
      }
    );
    $('.collapsible').collapsible();
    $('select').material_select();

    loadScopes();

    function loadScopes() {
      generalFunc.routeAdmin();
      loadlocaciones();
      loadRoles();
      loadContratistasSiver();
    }

    function loadUsers(){
        usuariosServ.getUsuarios()
            .then(function (res){
                $scope.usuarios = res.data;
                _.forEach(res.data, setLocacion)
            })
    }

    function setLocacion(loc, i){
        var locacion =  _.find($scope.locaciones, ['id', loc.id_locacion]);
        $scope.usuarios[i].locacion = locacion.nombre;
    }

    //load roles
    function loadRoles() {
        usuariosServ.getRoles()
            .then(function (res){
                $scope.roles = res.data;
            })
    }

    //load locaciones
    function loadlocaciones(){
        generalServ.getLocaciones()
            .then(function(res) {
                loadUsers();
                $scope.locaciones = res.data;
            });
    }

    //load contratistas Siver
    function loadContratistasSiver() {
        generalServ.getContratistas()
            .then(function(res){
                $scope.contratistas = res.data;
            })
    }

    //Crear usuario
    $scope.createUser = function(usuario) {
        usuariosServ.postUsuario(usuario)
          .then(successUsers, errorUsers)
    }

    function successUsers(res) {
        loadUsers();
        var $toastContent = $('<span>Cuenta creada éxitosamente.</span>');
        Materialize.toast($toastContent, 5000);
        $scope.closeUsuarioModal();
    }

    function errorUsers(res){
        var $toastContent = $('<span>Se generó un error, inténtelo de nuevo</span>');
        Materialize.toast($toastContent, 5000);
    }

    //Delete User
    $scope.deleteUser = function(id) {
        swal({
            title: 'Atención!',
            text: '¿Está seguro de eliminar el Usuario?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).
            then(function(res) {
               if(res) {
                    usuariosServ.deleteUser(id)
                        .then(function(res){
                            var $toastContent = $('<span>Usuario eliminado exitosamente!</span>');
                            Materialize.toast($toastContent, 3000);
                            loadUsers();
                        })
                }
            })
    }

    //update User
      $scope.updateUser = function(user) {
        usuariosServ.updateUser(user)
            .then(function(res){
                var $toastContent = $('<span>Usuario actualizado exitosamente!</span>');
                Materialize.toast($toastContent, 3000);
                $scope.closeUsuarioModal();
                loadUsers();
            })
      }

    $scope.viewUser = function(user) {
        $scope.usuario = user;
        $scope.edit = false;
        $('#newUsuarios').modal('open');
    }


    //close crear usuario modal
    $scope.closeUsuarioModal = function () {
        delete $scope.usuario;
      $('#newUsuarios').modal('close');
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion();
    }
  }
])
