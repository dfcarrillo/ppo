.controller('jsCambiarGPCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, programasServ, usuariosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        loadInfo();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:120},
                {field:'nombre', displayName:'Nombre', minWidth:300},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:150},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"',minWidth:190},
                {name: 'Cambiar', width:160, headerCellTemplate: '<br><div class ="center">Cambiar<br>Supervisor</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openCambiarGP(row.entity)"><i class="material-icons right">mode_edit</i>Cambiar</button>' },
            ]
        };

        //Cargue info necesaria
        function loadInfo() {
            generalFunc.routeJs()
            loadProyectos();
            loadGPs();
        }

        //LoadProyectos sin GP Asignado
        function loadProyectos() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo(js, 2, 'proyecto')
                .then(function(res){
                    $scope.tablaOp.data = res.data
                })
        }

        //Load GPs disponibles
        function loadGPs() {
            usuariosServ.getRolByName('gp')
                .then(function(res) {
                    $scope.GPs = res.data[0].user;
                })
        }

        //Modal
        $scope.openCambiarGP = function(proyecto) {
            $scope.proyecto = proyecto;
            loadScopes(proyecto)
            $('#cambiarGP').modal('open');
        }

        function loadScopes(proyecto) {
            usuariosServ.getUsuarioById(proyecto.id_gp_asignado)
                .then(function(res) {
                    $scope.gp_asignado = res.data.name;
                })
        }

        $scope.closeCambiarGP = function() {
            $scope.proyecto = [];
            $('#cambiarGP').modal('close');
        }

        //Cambiar Gestor de Proyecto
        $scope.cambiarGP = function(proyecto) {
            _.forEach(proyecto.sons, updateObrasGP)
            programasServ.putPrograma(proyecto)
                .then(successGP)
        }

        function updateObrasGP(obra){
            obra.id_gp_asignado = $scope.proyecto.id_gp_asignado;
            programasServ.putPrograma(obra)
                .then(function(res){})
        }

        function successGP(res) {
            $scope.proyecto = []
            $('#cambiarGP').modal('close');
            var $toastContent = $('<span>Supervisor Actualizado éxitosamente!</span>');
            Materialize.toast($toastContent, 3000);
        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }
    }
])