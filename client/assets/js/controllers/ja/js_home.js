.controller('jsHomeCtrl', ['$scope', '$state', 'usuariosServ', 'programasServ', 'generalServ', 'generalFunc',
  function($scope, $state, usuariosServ, programasServ, generalServ, generalFunc){

    generalFunc.routeJs()
    countObras();
    loadUser();

    //Contador de las obras en sus diferentes estados
    function countObras(){
        var js = window.localStorage['id_user']
        programasServ.countProgramaByJSEstadoTipo(js, 1, 'proyecto')
            .then(function(res){
                $scope.countSinGP = res.data.count;
            })
        /*programasServ.countProgramaByJSEstadoTipo(js, 1, 'obra')
            .then(function(res){
                $scope.countSinPropuesta = res.data.count;
            })*/
        programasServ.countProgramaByJSEstadoTipo(js, 1, 'obra')
            .then(function(res){
                $scope.countSinTrabajos = res.data.count;
            })
        programasServ.countProgramaByJSEstadoTipo(js, 3, 'obra')
            .then(function(res){
                $scope.countDetalleObras = res.data.count;
            })
        programasServ.countProgramaByJSEstadoTipo(js, 3, 'fase')
            .then(function(res){
                $scope.countCompromiso = res.data.count;
            })
        programasServ.countProgramaByJSEstadoTipo(js, 6, 'fase')
            .then(function(res){
                $scope.countValidar = res.data.count;
            })
    }

    //Carga info usuario
    function loadUser(){
        var js = window.localStorage['id_user']
        usuariosServ.getUsuarioById(js)
            .then(setZona)
    }

    function setZona(res){
        $scope.usuario = res.data;
        generalServ.getLocacionesbyId(res.data.id_locacion)
            .then(function(res){
                $scope.usuario.zona = res.data.nombre;
            })
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion()
    }

  }
])