.controller('jsCompromisoCtrl', ['$scope', '$state', 'programasServ', 'pasosServ', 'generalFunc', 'generalServ',
    function($scope, $state, programasServ, pasosServ, generalFunc, generalServ) {

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs()
        loadTrabajos();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'trabajos.xlsx',
            exporterExcelSheetName: 'trabajos',
            columnDefs:[
                {field:'parent.nombre', displayName:'Nombre', minWidth:250},
                {field:'parent.nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'parent.area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'parent.uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'Contratista', minWidth:140},
                {field:'parent.fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'dd-MM-yyyy  h:mma\':"UTC"', minWidth:190},
                { name: 'Compromiso', width:160, headerCellTemplate: '<br><div class ="center">Generar<br>Compromiso</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openCrearCompromiso(row.entity)"><i class="material-icons right">input</i>Generar</button>' },
            ]
        };

        //Cargue info necesaria
        function loadTrabajos() {
            var userId = window.localStorage['id_user'];
            programasServ.getProgramaByJSEstadoTipo3(userId, 3, 'fase')
                .then(function(res) {
                    $scope.tablaOp.data = res.data;
                })
            moment.locale('es');
            $scope.hoy = moment();
        }

        //Modal
        $scope.openCrearCompromiso = function(fase) {
            $scope.fase = fase;
            $scope.fase.compromiso = {};
            getPropuestaTrabajo(fase.parent.id);
            getProyecto(fase.parent.id_raiz);
            if(!_.isEmpty(fase.pasos) && fase.nombre == 'Ejecución'){
                $scope.contrapropPrevia = true;
                $scope.validacionPrevia = false;
                //$scope.fase.propuesta = _.find(fase.pasos, {'tipo': 'propuesta'});
                var contrapropuestas = _.filter(fase.pasos, {'tipo': 'contrapropuesta'});
                $scope.fase.contrapropuesta = _.maxBy(contrapropuestas, 'fecha_creacion');
                generalServ.getBrigade(fase.contrapropuesta.id_brigada)
                    .then(function (r) { $scope.fase.contrapropuesta.brigada = r.data.nombre })
                $scope.fase.compromiso.fecha_inicio = new Date($scope.fase.contrapropuesta.fecha_inicio);
                $scope.fase.compromiso.fecha_fin = new Date($scope.fase.contrapropuesta.fecha_fin);
                var validacion_supervisor = _.filter(fase.pasos, {'tipo': 'validacions'});
                if(validacion_supervisor.length != 0){
                    $scope.contrapropPrevia = false;
                    $scope.validacionPrevia = true;
                    $scope.fase.validacion_supervisor = validacion_supervisor[0];
                }
            }else{
                $scope.contrapropPrevia = false;
                $scope.validacionPrevia = false;
            }
            $('#compromiso').modal('open');
        }

        function getPropuestaTrabajo(id_trabajo) {
            pasosServ.getPasosByIdPrograma(id_trabajo)
                .then(function(res){
                    $scope.propTrabajo = res.data[0];
                })
        }

        function getProyecto(id_obra) {
            programasServ.getProgramasById(id_obra)
                .then(function (res) {
                    $scope.proyecto = res.data[0].parent;
                })
        }

        $scope.closeCompromiso = function() {
            $('#compromiso').modal('close');
            $scope.fase = [];
        }

        //Crear Compromiso
        $scope.crearCompromiso = function(fase) {
            fase.compromiso.fecha_inicio = fase.compromiso.fecha_inicio.toDate()
            fase.compromiso.fecha_fin = fase.compromiso.fecha_fin.toDate()
            // Si se aprueba compromiso
            if(fase.compromiso.estado == "1" || fase.compromiso.estado == undefined){
                // Si es ejecucion se asigna brigada de la contrapropuesta
                if(fase.nombre == 'Ejecución'){
                    fase.compromiso.id_brigada = fase.contrapropuesta.id_brigada;
                }
                fase.compromiso.id_programa = fase.id;
                fase.compromiso.estado = 0;
                fase.compromiso.tipo = 'compromiso';
                // Se crea el compromiso
                pasosServ.postPaso(fase.compromiso)
                    .then(successCompromiso)
                // Se actualiza estado de la fase
                updateEstadoFase(fase, 4);
                $('#compromiso').modal('close');
            }else {
                updateEstadoFase(fase, 2);
            }
        }

        function successCompromiso(res) {
            $scope.fase = []
            $('#compromiso').modal('close');
            var $toastContent = $('<span>Compromiso generado éxitosamente!</span>');
            Materialize.toast($toastContent, 3000);
        }

        function updateEstadoFase(fase, est){
            fase.estado_programa = est;
            programasServ.putPrograma(fase)
                .then(function(res){
                    loadTrabajos()
                    $('#compromiso').modal('close');
                    if(est == 2){
                        var $toastContent = $('<span>Contrapropuesta rechazada!</span>');
                        Materialize.toast($toastContent, 3000);
                    }
                })
        }

        $scope.setFechasCompromiso = function(res) {
            if(res == 1) {
                $scope.fase.compromiso.fecha_inicio = new Date($scope.fase.contrapropuesta.fecha_inicio);
                $scope.fase.compromiso.fecha_fin = new Date($scope.fase.contrapropuesta.fecha_fin);
            } else {
                $scope.fase.compromiso.fecha_inicio = null;
                $scope.fase.compromiso.fecha_fin = null;
            }
        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])