.controller('jsPropuestaCtrl', ['$state', '$scope', 'usuariosServ', 'programasServ', 'pasosServ', 'generalFunc',
    function($state, $scope, usuariosServ, programasServ, pasosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs();
        loadObras();
        setHoy();

        //Definición ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'obras.xlsx',
            exporterExcelSheetName: 'obras',
            columnDefs:[
                {field:'nombre', displayName:'Nombre', minWidth:250},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'Contratista', minWidth:140},
                {field:'fecha_limite', displayName:'Fecha Límite', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\'',minWidth:190},
                { name: 'Proponer', width:160, headerCellTemplate: '<br><div class ="center">Proponer<br>Fechas y Balancear</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openProponerObra(row.entity)"><i class="material-icons right">input</i>Proponer</button>' },
            ]
        };

        //Cargue info ui-grid
        function loadObras() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo(js, 1, 'obra')
                .then(function(res){
                    $scope.tablaOp.data = _.orderBy(res.data, ['fecha_limite'], ['asc']);
                })
        }

        function setHoy() {
            moment.locale('es');
            $scope.hoy = moment().format("YYYY-MM-DD");
        }

        //Modal
        $scope.openProponerObra = function(obra) {
            $scope.obra = obra;
            $('#asignarObra').modal('open');
        }

        $scope.closeProponerObra = function() {
            $scope.obra = [];
            $('#asignarObra').modal('close');
        }

        //Crear propuesta
        $scope.crearPropuesta = function(obra) {
            var propuesta = obra.propuesta;
            $scope.obra.propuesta.id_programa = obra.id;
            $scope.obra.propuesta.tipo = 'propuesta';
            propuesta.fecha_fin = generalFunc.getMaxHour(propuesta.fecha_fin);
            pasosServ.postPaso(propuesta)
                .then(successPropuesta, errorPropuesta);
            var actualizado = [{"id": obra.id, "estado_programa": 2}];
            programasServ.updatePrograma(actualizado[0])
                .then(function(res){});
            loadObras();
        }

        function successPropuesta(res) {
            var $toastContent = $('<span>Propuesta generada éxitosamente!</span>');
            Materialize.toast($toastContent, 4000);
            $scope.closeProponerObra();
            $scope.tablaOp.data = [];
            loadObras();
        }

        function errorPropuesta(res) {
            var $toastContent = $('<span>Error al generar la propuesta!</span>');
            Materialize.toast($toastContent, 4000);
        }

        //Datepicker
        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Cerrar',
            selectMonths: true,
            selectYears: true,
            container: 'body',
            format: 'yyyy-mm-dd'
        });

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])