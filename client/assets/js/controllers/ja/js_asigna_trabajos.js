.controller('jsAsignaTrabajosCtrl', ['$scope', '$state', 'programasServ', 'generalServ', 'pasosServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, programasServ, generalServ, pasosServ, usuariosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs();
        loadObras();

        //Definición ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'obras.xlsx',
            exporterExcelSheetName: 'obras',
            columnDefs:[
                {field:'nombre', displayName:'Nombre', minWidth:250},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'Contratista', minWidth:140},
                {field:'fecha_limite', displayName:'Fecha Límite', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\'', minWidth:190},
                { name: 'AsignarTrabajos', width:160, headerCellTemplate: '<br><div class ="center">Asignar<br>Trabajos a Obra</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openAsignarTrabajo(row.entity)"><i class="material-icons right">input</i>Asignar</button>' },
            ]
        };

        //Cargue info ui-grid
        function loadObras() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo2(js, 1, 'obra')
                .then(function(res){
                    $scope.tablaOp.data = _.orderBy(res.data, ['fecha_limite'], ['asc']);
                })
            moment.locale('es');
            $scope.hoy = moment().format("YYYY-MM-DD");
        }

        //Modal
        $scope.openAsignarTrabajo = function(obra) {
            $scope.obra = obra;
            $scope.obra.trabajos = [{}];
            $scope.obra.trabajos[0].nombre = 'TRABAJO ' + obra.nombre;
            $scope.obra.trabajos[0].uucc = Number(obra.uucc);
            $scope.obra.trabajos[0].fecha_inicio = new Date();
            $scope.obra.trabajos[0].fecha_fin = new Date(obra.fecha_limite);
            $scope.obra.trabajos[0].comentarios = obra.comentarios;
            $('#obraTrabajos').modal('open');
        };

        $scope.closeAsignarTrabajoModal = function() {
            $('#obraTrabajos').modal('close');
        };

        //Crear Trabajos
        $scope.crearTrabajos = function(obra) {
            var uucc_totales = _.sumBy($scope.obra.trabajos, 'uucc');
            if(uucc_totales == Number($scope.obra.uucc)){
                //Se construye json trabajos
                _.forEach(obra.trabajos, buildJSONTrabajos);
                //Se crean trabajos
                programasServ.postPrograma($scope.obra.trabajos)
                    .then(propuestaTrabajo)
                //Se actualiza estado de obra
                obra.estado_programa = 3;
                programasServ.putPrograma(obra)
                    .then(function (res) {loadObras()})
                $('#obraTrabajos').modal('close');
            }else{
                var $toastContent = $('<span>Las UUCC deben corresponder con las UUCC Obra</span>');
                Materialize.toast($toastContent, 3000);
            }
        }

        function buildJSONTrabajos(trabajo, i) {
            $scope.obra.trabajos[i].tipo = 'trabajo';
            $scope.obra.trabajos[i].area_servicio = $scope.obra.area_servicio;
            $scope.obra.trabajos[i].id_raiz = $scope.obra.id;
            $scope.obra.trabajos[i].id_locacion = $scope.obra.id_locacion;
            $scope.obra.trabajos[i].nombre_locacion = $scope.obra.nombre_locacion;
            $scope.obra.trabajos[i].id_usuario_creacion = window.localStorage['id_user'];
            $scope.obra.trabajos[i].id_js_asignado = $scope.obra.id_js_asignado;
            $scope.obra.trabajos[i].id_gp_asignado = $scope.obra.id_gp_asignado;
            $scope.obra.trabajos[i].id_eecc_asignado = $scope.obra.id_eecc_asignado;
            $scope.obra.trabajos[i].estado_programa = 1;
        }

        //Propuesta de cada trabajo creado
        function propuestaTrabajo(res) {
            $scope.propTrabajo = [];
            //Se contruye json de propuesta y se crean fases
            _.forEach(res.data, buildPropuestaYPostFases);
            // Se crean propuestas de trabajos
            pasosServ.postPaso($scope.propTrabajo)
                .then(function(res) {})
        }

        //Construye Json propuesta y crea fases del trabajo
        function buildPropuestaYPostFases(trabajo, i) {
            $scope.propTrabajo = [{}];
            $scope.propTrabajo[i].id_programa = trabajo.id;
            $scope.propTrabajo[i].tipo = 'propuesta';
            $scope.propTrabajo[i].fecha_inicio = trabajo.fecha_inicio;
            $scope.propTrabajo[i].fecha_fin = generalFunc.getMaxHour(trabajo.fecha_fin);
            $scope.propTrabajo[i].comentario = trabajo.comentarios;
            // Se construyen fases para el trabajo
            var fases = generalFunc.buildJsonFases(trabajo)
            // Se crean fases para trabajo
            programasServ.postPrograma(fases)
                .then(function(res) {
                    var $toastContent = $('<span>Trabajos Asignados existosamente</span>');
                    Materialize.toast($toastContent, 3000);
                })
        }

        $scope.validarUUCC = function(obra){
            var uucc_totales = _.sumBy(obra.trabajos, 'uucc');
            $scope.obra.trabajos[obra.trabajos.length-1].uucc = Number(obra.uucc) - uucc_totales;

        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion();
        }
    }
])