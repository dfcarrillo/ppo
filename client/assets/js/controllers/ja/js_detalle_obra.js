.controller('jsDetalleObrasCtrl', ['$scope', '$state', 'programasServ', 'generalServ', 'usuariosServ', 'generalFunc',
  function($scope, $state, programasServ, generalServ, usuariosServ, generalFunc) {

    $('.modal').modal({});
    $('.collapsible').collapsible();

    generalFunc.routeJs()
    loadObras();

    //Definicion ui-grid
    $scope.tablaOp={
        enableFiltering: true,
        enableRowSelection: true,
        multiSelect: false,
        enableColumnResizing:true,
        enableGridMenu:true,
        showColumnFooter:true,
        showGridFooter: true,
        enableColumnMoving:true,
        exporterExcelFilename: 'proyectos.xlsx',
        exporterExcelSheetName: 'proyectos',
        columnDefs:[
            {field:'nombre', displayName:'Nombre', minWidth:300},
            {field:'nombre_locacion', displayName:'Ubicación', minWidth:130},
            {field:'eecc.name', displayName:'Contratista', minWidth:130},
            {field:'uucc', displayName:'UUCC', minWidth:100},
            {field:'fecha_ingreso', displayName:'Fecha Creación', minWidth:180, cellFilter: 'date:\'dd-MM-yyyy  h:mma\':"UTC"'},
            {field:'fecha_limite', displayName:'Fecha Límite', minWidth:180, cellFilter: 'date:\'dd-MM-yyyy  h:mma\''},
        ]
    };

    function loadObras() {
        var js = window.localStorage['id_user']
        programasServ.getProgramaByJSEstadoTipo2(js, 3, 'obra')
            .then(function (res){
                $scope.tablaOp.data = res.data;
            })
    }

    //Seleccion fila ui-grid
    $scope.tablaOp.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(row) {
            $scope.detalleObra = row.entity;
            $scope.detalleTrabajos = row.entity.sons;
            _.forEach($scope.detalleTrabajos, function (trabajo, i){
                $scope.detalleTrabajos[i].estado_programa = generalFunc.getEstadoTrabajo(trabajo.estado_programa)
            })
            $('#detalleObra').modal('open');
            var propuesta = _.filter($scope.detalleObra.pasos, {'tipo': 'propuesta'});
            $scope.detalleObra.propuesta = propuesta[0];
        })
    }

    $scope.cerrarSession = function() {
        generalFunc.cerrarSesion()
    }

  }
])