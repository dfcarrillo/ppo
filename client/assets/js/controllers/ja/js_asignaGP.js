.controller('jsAsignarGPCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, programasServ, usuariosServ, generalFunc) {

        $('.modal').modal({
            dismissible: false
        });

        loadInfo();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:120},
                {field:'nombre', displayName:'Nombre', minWidth:300},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:150},
                {field:'uucc', displayName:'UUCC', minWidth:90},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:190},
                {name: 'GP', width:160, headerCellTemplate: '<br><div class ="center">Asignar<br>Supervisor</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openAsignarGP(row.entity)"><i class="material-icons right">input</i>Asignar</button>' },
            ]
        };

        //Cargue info necesaria
        function loadInfo() {
            generalFunc.routeJs()
            loadProyectos()
            loadGPs()
        }

        //LoadProyectos sin GP Asignado
        function loadProyectos() {
            var js = window.localStorage.id_user;
            programasServ.getProgramaByJSEstadoTipo(js, 1, 'proyecto')
                .then(function(res){
                    $scope.tablaOp.data = res.data
                })
        }

        //Load GPs disponibles
        function loadGPs() {
            usuariosServ.getRolByName('gp')
                .then(function(res) {
                    $scope.GPs = res.data[0].user;
                })
        }

        //Modal
        $scope.openAsignarGP = function(proyecto) {
            $scope.proyecto = proyecto;
            $('#asignarGP').modal('open');
        }

        $scope.closeAsignarGP = function() {
            $scope.proyecto = [];
            $('#asignarGP').modal('close');
        }

        //Asignar Gestor de Proyecto
        $scope.asignarGP = function(proyecto) {
            proyecto.estado_programa = 2;
            _.forEach(proyecto.sons, updateObras)
            programasServ.putPrograma(proyecto)
                .then(successGP)
        }
        //Actualiza GP a las obras
        function updateObras(obra){
            obra.id_gp_asignado = $scope.proyecto.id_gp_asignado;
            obra.estado_programa = 1;
            programasServ.putPrograma(obra)
                .then(function(res){})
        }

        function successGP(res) {
            loadProyectos();
            $scope.proyecto = []
            $('#asignarGP').modal('close');
            var $toastContent = $('<span>Supervisor Asignado éxitosamente!</span>');
            Materialize.toast($toastContent, 3000);
        }

        $scope.cerrarSession = function(){
            generalFunc.cerrarSesion()
        }
    }
])