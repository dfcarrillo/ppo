.controller('jsValidarCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'pasosServ', 'generalFunc',
    function($scope, $state, programasServ, usuariosServ, pasosServ, generalFunc){

        $('.modal').modal({
            dismissible: false
        });

        generalFunc.routeJs();
        loadTrabajos();

        //Definicion ui-grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'trabajos.xlsx',
            exporterExcelSheetName: 'trabajos',
            columnDefs:[
                {field:'parent.nombre', displayName:'Nombre', minWidth:250},
                {field:'parent.nombre_locacion', displayName:'Ubicación', minWidth:130},
                {field:'parent.area_servicio', displayName:'Área Servicio', minWidth:140},
                {field:'parent.uucc', displayName:'UUCC', minWidth:90},
                {field:'eecc.name', displayName:'EECC', minWidth:140},
                {field:'parent.fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:190},
                { name: 'Validar', width:160, headerCellTemplate: '<br><div class ="center">Validar<br>Fase</div>',
                    cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openValidar(row.entity)"><i class="material-icons right">input</i>Validar</button>' },
            ]
        };

        //Cargue info ui-grid
        function loadTrabajos(){
            var userId = window.localStorage['id_user'];
            programasServ.getProgramaByJSEstadoTipo3(userId, 6, 'fase')
                .then(function(res){
                    $scope.tablaOp.data = res.data;
                })
        }

        //Modal
        $scope.openValidar = function(fase) {
            $scope.trabajo = fase.parent;
            programasServ.getProgramasById(fase.parent.id_raiz)
                .then(function (res) {
                    $scope.proyecto = res.data[0].parent
                })
            $scope.fase = fase;
            //setSiguienteTrabajo(fase.parent.id_raiz, fase.parent.num_reprog);
            var compromisos = _.filter(fase.pasos, {'tipo': 'compromiso'});
            $scope.fase.compromiso = _.maxBy(compromisos, 'fecha_creacion');
            var validacion_supervisor = _.filter(fase.pasos, {'tipo': 'validacions'});
            $scope.fase.validacion_supervisor = _.maxBy(validacion_supervisor, 'fecha_creacion');
            $('#validar').modal('open');
            getObras();
        }

        /*function setSiguienteTrabajo(id_raiz, orden) {
            programasServ.getProgramasByIdRaiz(id_raiz)
                .then(function(res){
                    $scope.siguienteTrabajo = _.find(res.data, {'num_reprog': orden + 1 })
                })
        }*/

        function getObras(){
            programasServ.getProgramasById($scope.trabajo.id_raiz)
                .then(function(res){
                    $scope.obra = res.data;
                    programasServ.getProgramasByIdRaiz(res.data[0].parent.id)
                        .then(function (res){
                            $scope.obras = res.data;
                        })
                })
        }

        $scope.closeValidar = function() {
            $('#validar').modal('close');
            $scope.fase = [];
        }

        //Crear validacion
        $scope.crearValidacion = function(fase) {
            if(fase.validacion.estado == "1"){
                fase.validacion.id_programa = fase.id;
                fase.validacion.tipo = 'validacion';
                fase.validacion.id_compromiso = $scope.fase.compromiso.id;
                pasosServ.postPaso(fase.validacion)
                    .then(successValidacion)
                programasServ.getProgramasByIdRaiz(fase.parent.id)
                    .then(updateEstados)

                updateEstadoFase(fase, 7);
            }else{
                updateEstadoFase(fase.id, 3);
            }
        }

        function successValidacion(res) {
            loadTrabajos()
            $('#validar').modal('close');
            var $toastContent = $('<span>Validación exitosa!</span>');
            Materialize.toast($toastContent, 4000);
        }

        function updateEstados(fases){
            var ejecucion = _.find(fases.data, {'nombre': 'Ejecución'});
            var materiales = _.find(fases.data, {'nombre': 'Cuadratura de Materiales'});
            var pago = _.find(fases.data, {'nombre': 'Estado de Pago'});
            //var facturacion = _.find(fases.data, {'nombre': 'Facturación'});
            if($scope.fase.nombre == ejecucion.nombre && $scope.fase.nombre=='Ejecución'){
                //Se activa siguiente trabajo
                /*if($scope.siguienteTrabajo){
                    updateEstadoTrabajo($scope.siguienteTrabajo.id, 1)
                    var fase = _.find($scope.siguienteTrabajo.sons,{'nombre': 'Ejecución' })
                    updateEstadoFase(fase.id, 1)
                }*/
                // Si tiene liquidacion
                if(!_.isEmpty(materiales)){
                    updateEstadoTrabajo(materiales.parent, 2)
                    updateEstadoFase(materiales, 3)
                }else{
                    updateEstadoTrabajo(pago.parent, 3)
                    updateEstadoFase(pago, 3)
                }
            }/*else if($scope.fase.nombre==materiales.nombre){
                updateEstadoTrabajo(facturacion.id_raiz, 3)
                updateEstadoFase(facturacion.id, 3)
            } else if($scope.fase.nombre==pago.nombre){

            }
            else if($scope.fase.nombre == facturacion.nombre && $scope.fase.nombre=='Facturación'){
                updateEstadoTrabajo(facturacion.id_raiz, 4)
                if(!$scope.siguienteTrabajo){
                    updateEstadoObra($scope.obra[0].id, 4)
                    validarUltimaObra();
                }
            }*/
        }

        function updateEstadoFase(fase, est){
            fase.estado_programa = est;
            programasServ.updatePrograma(fase)
                .then(function(res){})
        }

        function updateEstadoTrabajo(trabajo, est){
            trabajo.estado_programa = est;
            programasServ.updatePrograma(trabajo)
                .then(function(res){})
        }

        /*function updateEstadoObra(id, est){
            var trabajo = [{"id": id, "estado_programa": est}];
            programasServ.updatePrograma(trabajo[0])
                .then(function(res){})
        }

        function validarUltimaObra(){
            var ejecutadas = 0;
            _.forEach($scope.obras, function(obra){
                if(obra.estado_programa == 4){
                    ejecutadas = ejecutadas + 1;
                }
            })
            if(ejecutadas == $scope.obras.length - 1){
                var id_proyecto = $scope.obra[0].parent.id;
                var trabajo = [{"id": id_proyecto, "estado_programa": 3, "fecha_cierre": moment().toDate()}];
                programasServ.updatePrograma(trabajo[0])
                    .then(function(res){})
            }
        }*/

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])