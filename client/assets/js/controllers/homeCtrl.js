.controller('homeCtrl', ['$scope', '$state', '$stateParams', 'homeServ', 'usuariosServ', '$http',
  function($scope, $state, $stateParams, homeServ, usuariosServ){

    $scope.login= function(user, passwd){
      usuariosServ.login(user, passwd)
        .then(loginSuccess, loginError)
    };

    function loginError(res){
        var $toastContent = $('<span>La cuenta o la contraseña es incorrecta.</span>');
        Materialize.toast($toastContent, 3000);
    }

    function loginSuccess(res){
        $scope.id_user = res.data.userId;
        $scope.token = res.data.id;
        window.localStorage.setItem('token', $scope.token);
        usuariosServ.getUsuarioById($scope.id_user)
            .then(setPerfil);
    }

    function setPerfil(res) {
      $scope.role = res.data.role;
      window.localStorage.setItem('id_user', $scope.id_user);
      window.localStorage.setItem('id_locacion', res.data.id_locacion);
      if($scope.role.name == 'admin'){
        $state.go('perfil_admin');
        window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'cdc'){
        $state.go('perfil_cdc');
        window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'js'){
          $state.go('perfil_js');
          window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'gp'){
          $state.go('perfil_gp');
          window.localStorage.setItem('rol', $scope.role.name);
      }else if($scope.role.name == 'eecc'){
          $state.go('perfil_ec');
          window.localStorage.setItem('rol', $scope.role.name);
      }
    }

    /*function loadRoles(){
      $http.get('files/data.json')
        .then(function(res){
          homeServ.addModel("roles", res.data.roles)
            .then(function(res){
              console.log("http code Roles" + res.status);
            });

          homeServ.addModel("usuarios", res.data.users)
            .then(function(res){
              console.log("http code Users" + res.status);
            })
        })
    }

    loadRoles();*/
  }
])
