.controller('backlogCtrl', ['$scope', '$stateParams', 'backlogsServ', 'usuariosServ', 'generalFunc',
    function($scope, $stateParams, backlogsServ, usuariosServ, generalFunc){

        $('.modal').modal(
            {
                dismissible: false
            }
        );

        getBacklogs();

        function getBacklogs() {
            if($stateParams.id){
                backlogsServ.getBacklogsById($stateParams.id)
                .then(function(res) {
                    $scope.backlogDetail = res.data;
                    $scope.detail = true;
                    $scope.backlogDetail.sons = _.orderBy($scope.backlogDetail.sons,['fecha_creacion'],['desc'])
                    _.forEach(res.data.sons, setUser);
                })
            } else {
                backlogsServ.getBacklogs()
                .then(function(res) {
                    $scope.detail = false;
                    $scope.backlogs = res.data;
                })
            } 
        }

        function setUser(user, i) {
            usuariosServ.getUsuarioById(user.id_usuario)
                .then(function (res) {
                    $scope.backlogDetail.sons[i].username = res.data.name;
                })
        }

        $scope.crearSugerencia = function(backlog) {
            backlog.id_usuario = Number(window.localStorage['id_user']);
            backlogsServ.postBacklogs(backlog)
                .then(function(res){
                    getBacklogs();
                    $('#feedModal').modal('close');
                    $scope.backlog = null;
                })
        }

        $scope.vote = function(b) {
            b.likes ++
            backlogsServ.putBacklogs(b)
                .then(function(res){})
        }

        $scope.saveComment = function(id, comment) {
            $scope.comment = '';
            var bd = {};
            bd.id_usuario = window.localStorage['id_user'];
            bd.id_backlog = id;
            bd.descripcion = comment;
            backlogsServ.postBacklogs(bd)
                .then(function(res){
                    getBacklogs();
                })
        }

        $scope.cancelar = function() {
            $('#feedModal').modal('close');
            $scope.backlog = null;
        }

        $scope.cerrarSession = function(){
            generalFunc.cerrarSesion()
        }

    }
])