.controller('cdcCrearCtrl', ['$scope', '$state', '$filter','usuariosServ', 'programasServ', 'generalServ', 'generalFunc', 'pasosServ',
    function($scope, $state, $filter, usuariosServ, programasServ, generalServ, generalFunc, pasosServ) {

        $('.modal').modal({
            dismissible: false
        });

        load();

        //Definición Ui-Grid
        $scope.tablaOp = {
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing: true,
            enableGridMenu: true,
            showColumnFooter: true,
            showGridFooter: true,
            enableColumnMoving: true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs: [
                {field: 'Supervisor', displayName: 'Responsable', width: '*'},
                {field: 'Area_servicio', displayName: 'Area Servicio', width: '*'},
                {field: 'Zona', displayName: 'Zona', width: '*'},
                {field: 'Codigo', displayName: 'Codigo', width: '*'},
                {field: 'Nombre', displayName: 'Nombre', width: '*'},
                {field: 'Fecha_Fin', displayName: 'Fecha Fin', width: '*'},
                {field: 'Comentarios', displayName: 'Comentarios', width: '*'}
            ]
        };

        //Cargue Info necesaria
        function load() {
            generalFunc.routeCdc();
            loadInfo();
            setHoy();
        }

        function loadInfo() {
            $scope.loading = true;
            //load eecc
            usuariosServ.getRolByName("eecc")
                .then(function(res) {
                    $scope.eeccs = res.data[0].user;
                });
            //load js
            usuariosServ.getRolByName("js")
                .then(function(res) {
                    $scope.jefesServicio = res.data[0].user;
                })
            //load gp
            usuariosServ.getRolByName("gp")
                .then(function(res) {
                    $scope.geos = res.data[0].user;
                })
            //load locaciones
            generalServ.getLocaciones()
                .then(function(res) {
                    $scope.locaciones = res.data;
                });
        }

        function setHoy() {
            moment.locale('es');
            $scope.hoy = moment().format("YYYY-MM-DD");
        }

        //Crear Proyecto Individual
        $scope.crearProyecto = function(proyecto) {
            var maxUUCC = _.maxBy(proyecto.obras, 'uucc');
            proyecto.tipo = 'proyecto';
            proyecto.id_usuario_creacion = window.localStorage['id_user'];
            proyecto.id_raiz = '0';
            proyecto.uucc = _.sumBy(proyecto.obras, 'uucc');
            proyecto.id_locacion = proyecto.locacion.id;
            proyecto.nombre_locacion = proyecto.locacion.nombre;
            //if ($scope.proyecto.obras[0].nombre) {
            proyecto.id_js_asignado = maxUUCC.id_js_asignado; //Js lider = asignado en obra con mayor uucc
            proyecto.estado_programa = '1'; //estado de proyecto con obras sin gp
            /*} else {
                proyecto.estado_programa = '0'; //estado de proyecto sin obras
            }*/ //ESTE IF ES VALIDO SE SE PERMITE CREACION DE PROYECTO SIN OBRAS
            programasServ.postPrograma(proyecto)
                .then(getProyecto);
            $('#proyecto').modal('close');
        }

        function getProyecto(res) {
            $scope.id_raiz_obras = res.data.id;
            //if ($scope.proyecto.obras[0].nombre) {
            _.forEach($scope.proyecto.obras, buildJSONObras);
            crearObras($scope.proyecto.obras);
            /*} else { //ESTE IF ES NECESARIO SI PERMITE CREAR PROYECTO SIN OBRAS
                $scope.proyecto = [];
                $scope.proyecto.obras = [{}];
                console.log("No se agregaron obras al proyecto");
            }*/
        }

        function buildJSONObras(obra, i) {
            $scope.proyecto.obras[i].tipo = 'obra';
            $scope.proyecto.obras[i].fecha_limite = generalFunc.getMaxHour($scope.proyecto.obras[i].fecha_limite);
            $scope.proyecto.obras[i].id_raiz = $scope.id_raiz_obras;
            $scope.proyecto.obras[i].id_usuario_creacion = window.localStorage['id_user'];
            if ($scope.tipoLocacionProyecto == "Establecimiento") {
                $scope.proyecto.obras[i].id_locacion = $scope.proyecto.id_locacion;
                $scope.proyecto.obras[i].nombre_locacion = $scope.proyecto.nombre_locacion;
            }else{
                var locacion = _.find($scope.locaciones, {'id': Number($scope.proyecto.obras[i].id_locacion)})
                $scope.proyecto.obras[i].nombre_locacion = locacion.nombre;
            }
        }

        function crearObras(obras) {
            programasServ.postPrograma(obras)
                .then(function(res) {
                    var $toastContent = $('<span>Obra creada éxitosamente!</span>');
                    Materialize.toast($toastContent, 6000);
                })
            $scope.proyecto = [];
            $scope.proyecto.obras = [{}];
        }

        //Validar Locación Proyecto
        $scope.validateSetLocacion = function(locacion) {
            $scope.tipoLocacionProyecto = locacion.tipo
            $scope.proyecto.nombre_locacion = locacion.nombre
            if ($scope.tipoLocacionProyecto == "Zona") {
                $scope.establecimientos = _.filter($scope.locaciones, {'id_zona': locacion.id})
                $scope.showLocacion = true;
            } else if ($scope.tipoLocacionProyecto == "Establecimiento") {
                $scope.showLocacion = false;
            }
        }

        //Cerrar modal creacion Individual
        $scope.closeCrearProyectoModal = function() {
            $scope.proyecto = [];
            $scope.proyecto.obras = [{}];
            $('#proyecto').modal('close');
        };

        //Creación Masiva de Proyectos
        $scope.uploadMasivo = function() {
            $scope.por_uucc = $scope.tablaOp.data.uucc
            delete $scope.tablaOp.data['uucc'];
            $scope.codigosExistentes = []
            $scope.codigosRegistrados = []
            $scope.zonas = _.filter($scope.locaciones, {'tipo': 'Zona'});
            $scope.proyectos = $scope.tablaOp.data;
            //$scope.proyectos = _.uniqBy($scope.tablaOp.data, 'Codigo');
            _.forEach($scope.proyectos, buildJSONProyectos);
            //$scope.proyectos = _.uniqBy($scope.proyectos, 'codigo_SAP');
            _.forEach($scope.proyectos, function(proyecto) {
                $scope.loading = false;
                programasServ.postPrograma(proyecto)
                    .then(getSuccess, getError);
            })
            $scope.tablaOp.data = [];
        }

        // Se construye Json del proyecto
        function buildJSONProyectos(proyecto) {
            proyecto.id_raiz = '0';
            proyecto.tipo = 'proyecto';
            proyecto.id_usuario_creacion = window.localStorage['id_user'];
            if ( proyecto['Codigo'].slice(-2).split('-')[1] ) {
                proyecto['codigo_SAP'] = proyecto['Codigo'].slice(0, proyecto['Codigo'].length - 2)
            } else if ( proyecto['Codigo'].slice(-2).split('_')[1] ) {
                proyecto['codigo_SAP'] = proyecto['Codigo'].split('_')[0]
            }
            else {
                proyecto['codigo_SAP'] = proyecto['Codigo']
            }
            if ( proyecto['Area_servicio'].substr(0, 1).toLowerCase() == 'm' ) {
                proyecto['area_servicio'] = 'Mantenimiento'
                //proyecto['id_js_asignado'] =  //16//SAN BERNARDO Produccion (crojo)
                //proyecto['id_js_asignado'] = 67 //BIO BIO Produccion (L_Aranguis)
                proyecto['id_js_asignado'] = 87 //ANTOFAGASTA Produccion (rlorca)
            } else if ( proyecto['Area_servicio'].substr(0, 1).toLowerCase() == 'c' ) {
                proyecto['area_servicio'] = 'Construcción'
                // proyecto['id_js_asignado'] =  //17//SAN BERNARDO Produccion (secarrascor)
                //proyecto['id_js_asignado'] = 68 //BIO BIO Produccion (R_Rivas)
                proyecto['id_js_asignado'] = 75 //ANTOFAGASTA Produccion (jfigueroa)
            }
            $scope.zonas.forEach(function (z) {
                if(proyecto['Zona'].toUpperCase().replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, "") == z.nombre.replace(/\s/g, "").substr(0,proyecto['Zona'].length)){
                    proyecto['id_locacion'] = z.id;
                    proyecto['nombre_locacion'] = z.nombre;
                    return false;
                }
            })
            $scope.geos.forEach(function (g) {
                if(proyecto['Supervisor'].split('/')[0].toUpperCase().replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, "") == g.name.toUpperCase().replace(/\s/g, "").normalize('NFD').replace(/[\u0300-\u036f]/g, "")) {
                    proyecto['id_gp_asignado'] = g.id;
                    return false;
                }
            })
            if (proyecto['id_gp_asignado']) {
                proyecto.estado_programa = '2';
            } else {
                proyecto.estado_programa = '1';
            }
            proyecto['fecha_inicio'] = proyecto['Fecha_Inicio']
            proyecto['fecha_fin'] = proyecto['Fecha_Fin']
            proyecto['fecha_limite'] = proyecto['Fecha_Fin']
            proyecto['nombre'] = proyecto['Nombre'] || 'Obra sin Nombre'
            proyecto['comentarios'] = proyecto['Comentarios']
            delete proyecto['Area_servicio']
            delete proyecto['Zona']
            delete proyecto['Supervisor']
            delete proyecto['Nombre']
            //delete proyecto['Codigo']
            delete proyecto['Fecha_Fin']
            delete proyecto['Comentarios']
            delete proyecto['x']
        }

        // Si el proyecto (obra) se carga exitosamente
        function getSuccess(res) {
            $scope.codigosRegistrados.push(res.config.data.codigo_SAP);
            //Se cargan etapas
            uploadEtapas(res.data);
            if ($scope.codigosRegistrados.length + $scope.codigosExistentes.length == $scope.proyectos.length - 1) {
                $scope.loading = true;
                $('#cargueMasivo').modal('open');
            }
        }

        // Si el proyecto (obra) ya existía
        function getError(res) {
            $scope.codigosExistentes.push(res.config.data);
            var data = res.config.data;
            programasServ.getProgramasByCodSap(res.config.data.codigo_SAP)
                .then(function(res){
                    //Se actualizan etapas
                    updateEtapas(res.data, data)
                })
            if ($scope.codigosRegistrados.length + $scope.codigosExistentes.length == $scope.proyectos.length - 1) {
                $scope.loading = true;
                $('#cargueMasivo').modal('open');
            }

        }

        function uploadEtapas(r) {
            var etapas = [];
            var ini = Number(r.Sem_Ini)
            var fin = Number(r.Sem_Fin)
            var nextWeek = moment().week() - 1;
            var i;
            var per = _.find($scope.por_uucc, ['codigo_SAP', r.Codigo])
            if(r.ec1){
                for (i=nextWeek; i<=fin; i++) {
                    var sem = 'ec1s' + i
                    if ( r[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = r.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = r.id_locacion
                        e['nombre_locacion'] = r.nombre_locacion
                        e['id_usuario_creacion'] = r.id_usuario_creacion
                        e['id_js_asignado'] = r.id_js_asignado
                        e['id_gp_asignado'] = r.id_gp_asignado
                        e['comentarios'] = r.comentarios
                        e['area_servicio'] = r.area_servicio
                        var v = (nextWeek-ini > 0)?(i - ini):1
                        e['nombre'] = r.ec1.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', r.ec1.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        if (r.codigo_SAP=='CGED-18005430'){
                            console.log(per)
                        }
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(r.ec2){
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        etapas.push(e)
                    }
                }
            }
            if(r.ec2){
                for (i=nextWeek; i<=fin; i++) {
                    var sem = 'ec2s' + i
                    if ( r[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = r.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = r.id_locacion
                        e['nombre_locacion'] = r.nombre_locacion
                        e['id_usuario_creacion'] = r.id_usuario_creacion
                        e['id_js_asignado'] = r.id_js_asignado
                        e['id_gp_asignado'] = r.id_gp_asignado
                        e['comentarios'] = r.comentarios
                        e['area_servicio'] = r.area_servicio
                        var v = (nextWeek-ini > 0)?(i - ini):1
                        e['nombre'] = r.ec2.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', r.ec2.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(r.ec1){
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(r.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        etapas.push(e)
                    }
                }
            }
            programasServ.postPrograma(etapas)
                .then(etapasSuccess, etapasError);
        }

        function etapasSuccess(res) {
            r = res.data;
        }

        function etapasError(res) {
            r = res.data;
        }

        function updateEtapas(obra, data) {
            var o = obra[0];
            var etapas = o.sons;
            var existentes = [];
            var nuevas = [];
            var ini = Number(data.Sem_Ini)
            var fin = Number(data.Sem_Fin)
            var lastWeek = moment().week() - 1;
            var i;
            var per = _.find($scope.por_uucc, ['codigo_SAP', data.Codigo])
            if(data.ec1){
                for (i=lastWeek; i<=fin; i++) {
                    var sem = 'ec1s' + i
                    if ( data[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = o.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = o.id_locacion
                        e['nombre_locacion'] = o.nombre_locacion
                        e['id_usuario_creacion'] = data.id_usuario_creacion
                        e['id_js_asignado'] = o.id_js_asignado
                        e['id_gp_asignado'] = o.id_gp_asignado
                        e['comentarios'] = o.comentarios
                        e['area_servicio'] = o.area_servicio
                        var v = ((lastWeek+2)-ini > 0)?(i - ini):1
                        e['nombre'] = data.ec1.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', data.ec1.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(data.ec2){
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        var etapa = _.find(etapas, ['nombre', e['nombre']])
                        if (etapa && etapa.id_eecc_asignado == e.id_eecc_asignado){
                            e['id'] = etapa.id
                            e['estado_programa'] = etapa.estado_programa
                            //e['estado_programa']=i<moment().week()?4:1
                            existentes.push(e)
                        } else if(etapa && etapa.id_eecc_asignado != e.id_eecc_asignado) {
                            nuevas.push(e)
                        } else if (!etapa && i>=(lastWeek+2)){
                            nuevas.push(e)
                        }
                    }
                }
            }
            if(data.ec2){
                for (i=lastWeek; i<=fin; i++) {
                    var sem = 'ec2s' + i
                    if ( data[sem]){
                        var e = {}
                        e['tipo'] = 'obra'
                        e['estado_programa'] = 1
                        e['id_raiz'] = o.id
                        e['fecha_limite'] = moment().week(i).weekday(6).set({hour:23,minute:59,second:00}).toDate()
                        e['id_locacion'] = o.id_locacion
                        e['nombre_locacion'] = o.nombre_locacion
                        e['id_usuario_creacion'] = o.id_usuario_creacion
                        e['id_js_asignado'] = o.id_js_asignado
                        e['id_gp_asignado'] = o.id_gp_asignado
                        e['comentarios'] = o.comentarios
                        e['area_servicio'] = o.area_servicio
                        var v = ((lastWeek+2)-ini > 0)?(i - ini):1
                        e['nombre'] = data.ec2.split('-')[1] + ' - ETAPA ' + v
                        ec = _.find($scope.eeccs, ['name', data.ec2.split('(')[0]])
                        e['id_eecc_asignado'] = ec.id
                        act_r = 'ruc' + i
                        ant_r = 'ruc' + (i-1)
                        act = 'uc' + i
                        ant = 'uc' + (i-1)
                        p_act = per?(per[act_r] || per[act]).slice(0,-1):100
                        p_ant = per?(per[ant_r] || per[ant]).slice(0,-1):0
                        p_diff = p_act-p_ant || 100
                        if(data.ec1){
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)/2
                        } else {
                            e['uucc'] = (((p_diff)*Number(data.uucc.replace(/\s/g, '').replace(',','')))/100)
                        }
                        var etapa = _.find(etapas, ['nombre', e['nombre']])
                        if (etapa && etapa.id_eecc_asignado == e.id_eecc_asignado){
                            e['id'] = etapa.id
                            e['estado_programa'] = etapa.estado_programa
                            //e['estado_programa']=i<moment().week()?4:1
                            existentes.push(e)
                        } else if(etapa && etapa.id_eecc_asignado != e.id_eecc_asignado) {
                            nuevas.push(e)
                        } else if (!etapa && i>=(lastWeek+2)){
                            nuevas.push(e)
                        }
                    }
                }
            }
            if(nuevas){
                programasServ.postPrograma(nuevas)
                    .then(etapasSuccess, etapasError);
            }
            _.forEach(existentes, putEtapas)
        }

        function putEtapas(etapa){
            programasServ.updatePrograma(etapa)
                .then(function(res){
                    r = res.data;
                })
        }

        $scope.closeModal = function() {
            $('#cargueMasivo').modal('close');
        }

        $scope.cerrarSession = function() {
            generalFunc.cerrarSesion()
        }

    }
])
