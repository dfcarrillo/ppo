.controller('cdcHomeCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalServ', 'generalFunc',
  function($scope, $state, programasServ, usuariosServ, generalServ, generalFunc){

    $('.modal').modal({
      dismissible: false
    });
    $('select').material_select();

    generalFunc.routeCdc();
    countCDCProjects();
    loadUser();

    //Contador de Proyectos
    function countCDCProjects() {
        var id = window.localStorage['id_user'];
        programasServ.countProyectoByCDCEstado(id, 0, 'proyecto')
            .then(function (res) {
                $scope.unassigned = res.data.count;
            })
        programasServ.countProyectoByCDCEstado(id, 1, 'proyecto')
            .then(function (res) {
                $scope.assigned = res.data.count;
            })
    }

    //Cargue info usuario
    function loadUser(){
      var cdc = window.localStorage['id_user']
      usuariosServ.getUsuarioById(cdc)
          .then(setZona)
    }

    function setZona(res){
      $scope.usuario = res.data;
      generalServ.getLocacionesbyId(res.data.id_locacion)
          .then(function(res){
              $scope.usuario.zona = res.data.nombre;
          })
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion()
    }
  }
])
