.controller('cdcAsignarCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalServ', 'generalFunc', 'usuariosServ',
  function($scope, $state, programasServ, usuariosServ, generalServ, generalFunc, usuariosServ) {

    $('.modal').modal({
        dismissible: false
    });
    $('select').material_select();

    loadInfo();

    //Definición Ui-Grid
    $scope.tablaOp={
          enableFiltering: true,
          enableRowSelection: true,
          multiSelect: false,
          enableColumnResizing:true,
          enableGridMenu:true,
          showColumnFooter:true,
          showGridFooter: true,
          enableColumnMoving:true,
          exporterExcelFilename: 'proyectos.xlsx',
          exporterExcelSheetName: 'proyectos',
          columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:130},
                {field:'nombre', displayName:'Nombre', minWidth:350},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:100},
                {field:'fecha_ingreso', displayName:'Fecha de Creación', minWidth:200, type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:190},
                { name: 'Asignar', width:180, headerCellTemplate: '<br><div class ="center">Asignar Y<br>Balancear Obras</div>',
                  cellTemplate:'<button class="waves-effect waves-light ui-grid-btn" ng-click="grid.appScope.openAsignarObrasModal(row.entity)"><i class="material-icons right">input</i>ASIGNAR</button>' }
          ]
    };

    //Cargue info necesaria
    function loadInfo() {
        generalFunc.routeCdc();
        loadProyectosAssigned();
        load();
    }

    //Carga info ui-grid
    function loadProyectosAssigned() {
        var id = window.localStorage['id_user'];
        programasServ.getProyectoByCDCEstado(id, 0, 'proyecto')
            .then(function(res){
                $scope.tablaOp.data = res.data
                $scope.detalleProyecto = [];
            });
    }

    function load() {
        usuariosServ.getRolByName("eecc")
            .then(function(res) {
                $scope.eeccs = res.data[0].user;
            });

        usuariosServ.getRolByName("js")
            .then(function(res) {
                $scope.jefesServicio = res.data[0].user;
            })

        generalServ.getLocaciones()
            .then(function(res) {
                $scope.locaciones = res.data;
            });

        moment.locale('es');
        $scope.hoy = moment().format("YYYY-MM-DD");
    }

    //Modal
    $scope.openAsignarObrasModal = function(project) {
        $scope.proyecto = project;
        $scope.validateSetLocacion(project.id_locacion)
        $scope.proyecto.obras = [{}];
        $('#proyecto').modal('open');
    };

    $scope.closeAsignarObrasModal = function() {
        $('#proyecto').modal('close');
        $scope.proyecto = [];
        $scope.proyecto.obras = [{}];
    }

    //Actualizar proyecto
    $scope.updateProyecto = function(proyecto) {
        $scope.id_raiz_obras = proyecto.id;
        var maxUUCC = _.maxBy(proyecto.obras, 'uucc');
        proyecto.uucc = _.sumBy(proyecto.obras, 'uucc');
        proyecto.id_js_asignado = maxUUCC.id_js_asignado;
        proyecto.estado_programa = proyecto.id_gp_asignado?2:1
        programasServ.putPrograma(proyecto)
            .then(postObras);
    }

    //Crear Obras con id_raiz proyecto anterior
    function postObras(res) {
        if ($scope.proyecto.obras[0].nombre) {
            _.forEach($scope.proyecto.obras, buildJSONObras);
            crearObras($scope.proyecto.obras);
        } else {
            $scope.proyecto = [];
            $scope.proyecto.obras = [{}];
            console.log("No se agregaron obras al proyecto");
        }
    }

    function buildJSONObras(obra, i) {
        $scope.proyecto.obras[i].tipo = 'obra';
        $scope.proyecto.obras[i].fecha_limite = generalFunc.getMaxHour($scope.proyecto.obras[i].fecha_limite);
        $scope.proyecto.obras[i].id_raiz = $scope.id_raiz_obras;
        $scope.proyecto.obras[i].id_usuario_creacion = window.localStorage['id_user'];
        if($scope.proyecto.id_gp_asignado) {
            $scope.proyecto.obras[i].estado_programa = 1;
            $scope.proyecto.obras[i].id_gp_asignado = $scope.proyecto.id_gp_asignado;
        } else {
            $scope.proyecto.obras[i].estado_programa = 0;
        }

        if ($scope.tipoLocacionProyecto == "Establecimiento") {
            $scope.proyecto.obras[i].id_locacion = $scope.proyecto.id_locacion;
            $scope.proyecto.obras[i].nombre_locacion = $scope.proyecto.nombre_locacion;
        }else{
            var locacion = _.find($scope.locaciones, {'id': Number($scope.proyecto.obras[i].id_locacion)})
            $scope.proyecto.obras[i].nombre_locacion = locacion.nombre;
        }
    }

    function crearObras(obras) {
        programasServ.postPrograma(obras)
            .then(function(res) {
                var $toastContent = $('<span>Etapas de obra asignadas éxitosamente!</span>');
                Materialize.toast($toastContent, 3000);
                $scope.closeAsignarObrasModal();
            })
        $scope.proyecto = [];
        $scope.proyecto.obras = [{}];
        loadProyectosAssigned()
    }

    //Validar Locacion del Proyecto
    $scope.validateSetLocacion = function(locacion) {
        var locacion = _.find($scope.locaciones, {'id': Number(locacion)});
        $scope.tipoLocacionProyecto = locacion.tipo
        $scope.proyecto.nombre_locacion = locacion.nombre
        if ($scope.tipoLocacionProyecto == "Zona") {
            $scope.establecimientos = _.filter($scope.locaciones, {'id_zona': locacion.id})
            $scope.showLocacion = true;
        } else if ($scope.tipoLocacionProyecto == "Establecimiento") {
            $scope.showLocacion = false;
        }
    }

    $scope.cerrarSession = function() {
        generalFunc.cerrarSesion()
    }

  }
])