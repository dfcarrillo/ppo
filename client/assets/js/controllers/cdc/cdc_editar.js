.controller('cdcEditarCtrl', ['$scope', '$state', 'programasServ', 'usuariosServ', 'generalServ', 'generalFunc',
  function($scope, $state, programasServ, usuariosServ, generalServ, generalFunc) {

    $('.modal').modal();
    $('.collapsible').collapsible();
    $('select').material_select();

    loadInfo();

    //Definición Ui-Grid
    $scope.tablaOp={
          enableFiltering: true,
          enableRowSelection: true,
          multiSelect: false,
          enableColumnResizing:true,
          enableGridMenu:true,
          showColumnFooter:true,
          showGridFooter: true,
          enableColumnMoving:true,
          exporterExcelFilename: 'proyectos.xlsx',
          exporterExcelSheetName: 'proyectos',
          columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:110},
                {field:'nombre', displayName:'Nombre', minWidth:280},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:100},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:170},
                {field:'porcentaje_avance', displayName:'Porcentaje Avance', minWidth:150},
                { name: 'Asignar', width:160, headerCellTemplate: '<br><div class ="center">Editar<br> Obras</div>',
                  cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openAsignarObrasModal(row.entity)"><i class="material-icons right">mode_edit</i>EDITAR</button>' }
            ]
    };

    //Cargue info necesaria
    function loadInfo() {
        generalFunc.routeCdc();
        loadProyectosAssigned();
        getLocaciones();
        loadEECC();
        loadJS();
    }

    function loadEECC() {
      usuariosServ.getRolByName("eecc")
          .then(function(res) {
              $scope.eeccs = res.data[0].user;
          });
    }

    function loadJS() {
      usuariosServ.getRolByName("js")
          .then(function(res) {
              $scope.jefesServicio = res.data[0].user;
          })
    }

    function getLocaciones() {
      generalServ.getLocaciones()
          .then(function(res) {
              $scope.locaciones = res.data;
          });
    }

   //Cargue info ui-grid
    function loadProyectosAssigned() {
        var id = window.localStorage['id_user'];
        programasServ.getProyectoByCDCEstado2(id, 1, 'proyecto')
            .then(function(res){
                $scope.tablaOp.data = res.data
                $scope.detalleProyecto = [];
            });
    }

    //Modal
    $scope.openAsignarObrasModal = function(project) {
      $scope.proyecto = project;
      $scope.validateSetLocacion(project.id_locacion)
      $scope.proyecto.obras = project.sons;
      _.forEach(project.sons, setObras)
      $('#proyecto').modal('open');
    };

    function setObras(obra,i){
      $scope.proyecto.obras[i].uucc = Number(obra.uucc)
      $scope.proyecto.obras[i].fecha_limite = generalFunc.getFormatDate(obra.fecha_limite)
    }

    $scope.closeAsignarObrasModal = function() {
      $('#proyecto').modal('close');
      $scope.proyecto = [];
      $scope.proyecto.obras = [{}];
    }

   //Aactualiza proyecto
    $scope.updateProyecto = function(proyecto){
        proyecto.uucc = _.sumBy(proyecto.obras, 'uucc');
        var maxUUCC = _.maxBy(proyecto.obras, 'uucc');
        proyecto.id_js_asignado = maxUUCC.id_js_asignado;
        programasServ.updatePrograma(proyecto)
            .then(updateObras)
    }

    function updateObras(res){
        if (res.data.count == 1){
            _.forEach($scope.proyecto.obras, function(obra){
                programasServ.updatePrograma(obra)
                    .then(function (res){
                        $scope.proyecto = [];
                        $scope.proyecto.obras = [{}];
                        $('#proyecto').modal('close');
                        var $toastContent = $('<span>Actualización Exitosa!</span>');
                        Materialize.toast($toastContent, 4000);
                    })
            })
        }
    }

    //Valida Locacion Proyecto
    $scope.validateSetLocacion = function(locacion) {
      var locacion = _.find($scope.locaciones, {'id': Number(locacion)});
      $scope.tipoLocacionProyecto = locacion.tipo
      $scope.proyecto.nombre_locacion = locacion.nombre
      if ($scope.tipoLocacionProyecto == "Zona") {
          $scope.establecimientos = _.filter($scope.locaciones, {'id_zona': locacion.id})
          $scope.showLocacion = true;
      } else if ($scope.tipoLocacionProyecto == "Establecimiento") {
          $scope.showLocacion = false;
      }
    }

    //Detalle
    $scope.tablaOp.onRegisterApi = function(gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function(row) {
            $scope.detalleProyecto = row.entity;
            usuariosServ.getUsuarioById(row.entity.id_js_asignado)
                .then(function(res){
                    $scope.detalleProyecto.js = res.data.name
                })
            $scope.detalleObras = row.entity.sons;
            _.forEach(row.entity.sons, function (obra){
                obra.estado_programa = generalFunc.evaluarEstadoObra(obra.estado_programa)
                obra.fecha_ingreso = generalFunc.getFormatDate(obra.fecha_ingreso)
                obra.fecha_limite = generalFunc.getFormatDate(obra.fecha_limite)
            })
            $('#detalleProyecto').modal('open');
        })
    }

    $scope.cerrarSession = function() {
        generalFunc.cerrarSesion()
    }

  }
])