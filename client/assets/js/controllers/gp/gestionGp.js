.controller('gestionGpCtrl', ['$scope', '$state', 'gestionServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, gestionServ, usuariosServ, generalFunc){

        $('.modal').modal(
            {
                dismissible: false
            }
        );
        $('.collapsible').collapsible();
        $('select').material_select();

        if(window.sessionStorage['rol'] == "admin"){
            // $state.go("perfil_admin");
        }else if( window.sessionStorage['rol'] == 'cdc'){
            $state.go("perfil_cdc");
        }else{
            //$state.go("home");
        }

        $scope.user = window.localStorage.getItem("id_user");
        $scope.validacion={tipo:"validacions"};
        $scope.fase={};
        //ESTADOS DE FASE SEGUN PASO ACTUAL
        $scope.statect=2;//En contrapropuesta
        $scope.stateco=3;//En compromiso
        $scope.stater=4;//En reporte
        $scope.statev=5;//En validacion de supervisor
        $scope.statevj=6;//En validacion de JA
        $scope.staterep=8;//En reprogramacion del CDC
        $scope.stateclosed=7;//Fase cerrada
        //ESTADOS DEL TRABAJO SEGUN FASE ACTUAL
        $scope.jobej=1;//En ejecucion
        $scope.jobep=3;//En estado de pago
        $scope.jobfa=4;//En facturacion
        $scope.jobclosed=5;//Trabajo cerrado
        //OTROS ESTADOS DE LA TABLA PROGRAMA
        $scope.eoclosed=4;//Etapa de obra cerrada
        $scope.oclosed=3;//Obra cerrada
        //NOMBRE ESTANDARES DE LAS FASES
        $scope.replanteo="Replanteo";
        $scope.ejecucion="Ejecución";
        $scope.cuadratura="Cuadratura de Materiales";
        $scope.estado="Estado de Pago";
        $scope.facturacion="Facturación";

        $scope.avanceparcial="Avance parcial";


        //loadInfo();
        testTemp();

        $scope.tablaOp={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosValidarS.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'ec', displayName:'Empresa', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date'},
                { name: 'Accion', width:150,
                    cellTemplate:'<button class="buttonT" ng-click="grid.appScope.openValidacion(row.entity)">Validar</button>' }
            ]
        };

        $scope.tablaOpr={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosReportar.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'ec', displayName:'Empresa', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date'}
            ]
        };

        loadInfo();

        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Aceptar',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: true, // Creates a dropdown to control year
            container: 'body',
            format: 'yyyy-mm-dd'
            //onClose: function(){
            //  $("#mdlTrainingDetail form input:eq(1)").focus();
            //}
        });

        function testTemp(){
            $scope.nombre="Andres Posada";
            $scope.perfil="Gestor Proyecto";

            $scope.previo={};
            $scope.previo.tipo="compromiso";
            $scope.previo.titulo="Reporte previo";
            $scope.previo.fecha_inicio=new Date(2018,3,20);
            $scope.previo.fecha_fin=new Date(2018,3,29);
            $scope.previo.comentario="El trabajo debe hacerse en horas de la mañana";
            $scope.previo.avance=10;
            $scope.previo.resultado="Reprogramado";
            $scope.previo.causa=$scope.avanceparcial;

        }

        function loadInfo(){

            var eje={id:1, name:"Aprobada"};
            var rep={id:2, name:"Reprobada"};
            $scope.results=[eje,rep];
            $scope.resultado=$scope.results[0];

            gestionServ.getCausas().then(function(rea){
                $scope.causas=rea.data;
                $scope.causas.splice(0,0,{id:0, causa:"Sin causa"});
            });

            gestionServ.getFasesEstadoGp($scope.stater,$scope.user).then(function(rew){
                $scope.proyectosr=rew.data;
                $scope.tablaOpr.data=$scope.proyectosr;
                _.forEach($scope.proyectosr,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.ec=pro.eecc.name;
                    prioridadC(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });


            gestionServ.getFasesEstadoGp($scope.statev,$scope.user).then(function(res){
                $scope.proyectosc=res.data;
                $scope.tablaOp.data=$scope.proyectosc;
                _.forEach($scope.proyectosc,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.ec=pro.eecc.name;
                    prioridadP(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });
            getUsuario();
        }

        function getUsuario(){
            usuariosServ.getUsuarioById($scope.user).then(function(res){
                $scope.nombre=res.data.name;
            });
        }

        function prioridadC(fase){
            fase.fecha_limite=new Date();
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            if (compromiso.id>0){
                fase.fecha_limite=new Date(compromiso.fecha_fin);
            }
        }

        function prioridadP(fase){
            fase.fecha_limite=new Date();
            gestionServ.getPasosPrograma(fase.parent.id).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];
                    fase.fecha_limite=new Date(propuesta.fecha_fin);
                }
            });
        }

        function obraProyecto(fase){
            gestionServ.getProgramaMedioId(fase.parent.id_raiz).then(function(res){
                var obra=res.data;
                console.log(obra);
                fase.obra=obra.nombre;
                fase.codigo_SAP=obra.parent.codigo_SAP;
                fase.description=obra.parent.nombre;
            });
        }

        $scope.openValidacion=function(fase){
            $('#validacion').modal('open');
            previoValidacion(fase);
            $scope.validacion.id_programa=fase.id;
            $scope.validacion.tipo="validacions";
            $scope.fase=copiarPrograma(fase);
            $scope.previo.trabajo=fase.parent;
            $scope.previo.num_reprog=fase.num_reprog;
            $scope.job={};
            $scope.job.codigo_SAP=fase.codigo_SAP;
            $scope.job.description=fase.description;
            $scope.job.nom_tra=fase.parent.nombre;
            propuestaTrabajo(fase.parent.id);
        };

        function previoValidacion(fase){
            $scope.previo={};
            var reporte=ultimoPasoTipo(fase.pasos, "reporte");
            if (reporte.id>0){
                leerPrevio(reporte);
                $scope.previo.role="EECC";
                $scope.previo.quien=fase.ec;
            }else{
                previoError();
            }
        }

        function leerPrevio(paso){
            $scope.previo.titulo="Reporte previo";
            $scope.previo.tipo=paso.tipo;
            var ini=new Date(paso.fecha_inicio);
            var fin=new Date(paso.fecha_fin);
            $scope.previo.fecha_inicio=ini;
            $scope.previo.fecha_fin=fin;
            $scope.previo.comentario=paso.comentario;
            if (paso.estado==1){
                $scope.previo.resultado="Ejecutado";
            }else{
                $scope.previo.resultado="Reprogramado";
            }
            if (typeof paso.id_causa=="undefined"){
                paso.id_causa=0;
            }
            var causa=buscarId($scope.causas,paso.id_causa);
            $scope.previo.causa=causa.causa;
            $scope.previo.avance=paso.avance;
        }

        function buscarId(lista,id){

            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.id==id){
                    obj=objeto;
                }

            });

            return obj;
        }

        function previoError(){
            $scope.previo={};
            $scope.previo.titulo="Paso previo";
            $scope.previo.tipo="Otro";
            $scope.previo.fecha_inicio="Error";
            $scope.previo.fecha_fin="Error";
            $scope.previo.comentario="Error: paso previo no encontrado";
        }

        function ultimoPasoTipo(pasos, tipo){
            var pasostipo=_.filter(pasos,function(step){
                return step.tipo==tipo;
            });
            var ultimo={id:0};
            if (pasostipo.length>0){
                ultimo=pasostipo[pasostipo.length-1];
            }
            return ultimo;
        }

        function copiarPrograma(program){
            var nuevo={id:program.id};
            if (typeof program.tipo != "undefined"){
                nuevo.tipo=program.tipo;
            }
            if (typeof program.nombre != "undefined"){
                nuevo.nombre=program.nombre;
            }
            if (typeof program.estado_programa != "undefined"){
                nuevo.estado_programa=program.estado_programa;
            }
            if (typeof program.codigo_SAP != "undefined"){
                nuevo.codigo_SAP=program.codigo_SAP;
            }
            if (typeof program.uucc != "undefined"){
                nuevo.uucc=program.uucc;
            }
            if (typeof program.porcentaje_avance != "undefined"){
                nuevo.porcentaje_avance=program.porcentaje_avance;
            }
            if (typeof program.comentarios != "undefined"){
                nuevo.comentarios=program.comentarios;
            }
            if (typeof program.area_servicio != "undefined"){
                nuevo.area_servicio=program.area_servicio;
            }
            if (typeof program.num_reprog != "undefined"){
                nuevo.num_reprog=program.num_reprog;
            }

            if (typeof program.id_raiz != "undefined"){
                nuevo.id_raiz=program.id_raiz;
            }
            if (typeof program.id_pasos != "undefined"){
                nuevo.id_pasos=program.id_pasos;
            }
            if (typeof program.id_locacion != "undefined"){
                nuevo.id_locacion=program.id_locacion;
            }
            if (typeof program.fecha_limite != "undefined"){
                nuevo.fecha_limite=program.fecha_limite;
            }
            if (typeof program.fecha_cierre != "undefined"){
                nuevo.fecha_cierre=program.fecha_cierre;
            }
            if (typeof program.fecha_ingreso != "undefined"){
                nuevo.fecha_ingreso=program.fecha_ingreso;
            }

            if (typeof program.id_usuario_creacion != "undefined"){
                nuevo.id_usuario_creacion=program.id_usuario_creacion;
            }
            if (typeof program.id_js_asignado != "undefined"){
                nuevo.id_js_asignado=program.id_js_asignado;
            }
            if (typeof program.id_gp_asignado != "undefined"){
                nuevo.id_gp_asignado=program.id_gp_asignado;
            }
            if (typeof program.id_eecc_asignado != "undefined"){
                nuevo.id_eecc_asignado=program.id_eecc_asignado;
            }
            if (typeof program.id_brigada_asignada != "undefined"){
                nuevo.id_brigada_asignada=program.id_brigada_asignada;
            }
            if (typeof program.nombre_locacion != "undefined"){
                nuevo.nombre_locacion=program.nombre_locacion;
            }
            return nuevo;
        }

        function propuestaTrabajo(idt){

            $scope.job.inicio=new Date(2010,1,1);
            $scope.job.fin=new Date(2058,1,1);

            gestionServ.getPasosPrograma(idt).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];

                    $scope.job.inicio=new Date(propuesta.fecha_inicio);
                    $scope.job.fin=new Date(propuesta.fecha_fin);
                    $scope.job.comentario=propuesta.comentario;
                }
                console.log($scope.job);
            });
        }

        function deleteByIdd(lista,id){

            _.each(lista, function(tra,index){

                if(index<lista.length){
                    if(tra.id==id){

                        lista.splice(index,1);
                    }
                }
            });
        }

        //DatePicker
        $scope.modal1=function(){
            if ($scope.previo.causa==$scope.avanceparcial){
                $scope.previo.causa="Clima";
            }else{
                $scope.previo.causa=$scope.avanceparcial;
            }
        };

        $scope.modal2=function(){
            $('#validacion').modal('open');
        };





        $scope.cancelarValidacion=function(){
            $('#validacion').modal('close');
            $scope.validacion={tipo:"validacions"};
            $scope.fase={};
            $scope.previo={};
            $scope.resultado=$scope.results[0];
        };

        function reloadInfo(){
            $scope.validacion={tipo:"validacions"};
            $scope.fase={};
            $scope.previo={};
            $scope.resultado=$scope.results[0];
            loadInfo();
        }

        $scope.sendValidar= function(){

            validar();

            if ($scope.errores.length==0){
                senddummy();
            }else{
                $('#validacion').modal('close');
                $('#errores').modal('open');
            }

        };

        $scope.closeErrores=function (){

            $('#errores').modal('close');
            $('#validacion').modal('open');

        };

        function validar(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.validacion.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.validacion.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.validacion.fecha_fin;
                res1=validarCampo($scope.validacion.fecha_inicio,"less",info);
                if (res1=="ok"){
                    info.max=$scope.job.fin;
                    info.min=$scope.job.inicio;
                    res1=validarCampo($scope.validacion.fecha_inicio,"inrange",info);
                    res2=validarCampo($scope.validacion.fecha_fin,"inrange",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    }
                    info.max=new Date();
                    res1=validarCampo($scope.validacion.fecha_inicio,"less",info);
                    res2=validarCampo($scope.validacion.fecha_fin,"less",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser anteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.validacion.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.validacion.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

        }

        function validarCampo(valor,tipo,info){

            var resul="Error de tipo de validacion";

            if (tipo=="undefined"){
                if (typeof valor!="undefined"){
                    resul="ok";
                }else{
                    resul="El campo no puede ser vacio";
                }
            }else if(tipo=="emptystring"){
                if (valor==""){
                    resul="El campo no puede ser vacio";
                }else{
                    resul="ok";
                }
            }else if(tipo=="different"){
                if (valor==info.igual){
                    resul="El campo no puede ser igual a "+info.igual;
                }else{
                    resul="ok";
                }
            }else if(tipo=="equal"){
                if (valor==info.igual){
                    resul="ok";
                }else{
                    resul="El campo debe ser igual a "+info.igual;
                }
            }else if(tipo=="greater"){
                if(valor>=info.min){
                    resul="ok";
                }else{
                    resul="El campo debe ser mayor a "+info.min;
                }
            }else if(tipo=="less"){
                if(valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe ser menor a "+info.max;
                }
            }else if(tipo=="inrange"){
                if(info.min<=valor && valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe estar entre "+info.min+" y "+info.max;
                }
            }else if(tipo=="datatype"){
                if(typeof valor==info.tipo){
                    resul="ok";
                }else{
                    resul="Se espera que el valor sea tipo "+info.tipo;
                }
            }
            return resul;
        }

        function verificarEstados(lista,estado){
            var resp=true;
            _.forEach(lista,function(program){
                if(program.estado_programa!=estado){
                    resp=false;
                }
            });
            return resp;
        }

        function nextFase(actual){

            var resp=$scope.facturacion;
            if(actual==$scope.replanteo){
                resp=$scope.ejecucion;
            }else if(actual==$scope.ejecucion){
                resp=$scope.cuadratura;
            }else if(actual==$scope.cuadratura){
                resp=$scope.estado;
            }else if(actual==$scope.estado){
                resp=$scope.facturacion;
            }

            return resp;
        }

        function startFase(fase){
            var resp=$scope.stateco;
            if (fase==$scope.ejecucion){
                resp=$scope.statect;
            }
            return resp;
        }

        function buscarNombre(lista,nombre){
            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.nombre==nombre){
                    obj=objeto;
                }
            });
            return obj;
        }

        function estadoTrabajo(fase){

            var resp=$scope.jobej
            if (fase==$scope.ejecucion){
                resp=$scope.jobej;
            }else if(fase==$scope.estado){
                resp=$scope.jobep;
            }else if(fase==$scope.facturacion){
                resp=$scope.jobfa;
            }
            return resp;
        }

        function senddummy(){
            $('#validacion').modal('close');
            $scope.validacion.fecha_creacion=new Date();
            var ms=$scope.validacion.fecha_inicio._d;
            var msf=$scope.validacion.fecha_fin._d;
            $scope.validacion.fecha_inicio=ms;
            $scope.validacion.fecha_fin=msf;
            $scope.validacion.estado=$scope.resultado.id;
            $scope.fase.codigo_SAP=undefined;
            gestionServ.postPaso($scope.validacion).then(function(ree){
                if ($scope.fase.nombre==$scope.ejecucion){
                    if ($scope.validacion.estado==2){//Salio mal
                        $scope.fase.estado_programa=$scope.stateco;
                        $scope.fase.num_reprog=$scope.fase.num_reprog+1;
                        gestionServ.putPrograma($scope.fase).then(reloadInfo);
                    }else{ //Salio bien
                        $scope.fase.estado_programa=$scope.statevj;
                        gestionServ.putPrograma($scope.fase).then(reloadInfo);
                    }

                }else{
                    if ($scope.validacion.estado==2){
                        $scope.fase.estado_programa=$scope.stater;
                        $scope.fase.num_reprog=$scope.fase.num_reprog+1;
                        gestionServ.putPrograma($scope.fase).then(reloadInfo);
                    }else{
                        if ($scope.fase.nombre==$scope.facturacion){
                            //PROCESO DE CIERRE DE TRABAJO, ETAPA DE OBRA Y OBRA

                            $scope.fase.estado_programa=$scope.stateclosed;//Fase cerrada
                            gestionServ.putPrograma($scope.fase).then(function(reb){
                                //cierro la fase
                                $scope.previo.trabajo.estado_programa=$scope.jobclosed;//Trabajo cerrado
                                gestionServ.putPrograma($scope.previo.trabajo).then(function(reba){
                                    //cierro el trabajo
                                    gestionServ.getProgramaMedioId($scope.previo.trabajo.id_raiz).then(function(rew){
                                        var obra=rew.data;//Obra es padre de trabajo: Etapa de obra
                                        //miro si todos los trabajos estan cerrrados
                                        var tcerra=verificarEstados(obra.sons,$scope.jobclosed);
                                        if (tcerra==true){
                                            var obrac=copiarPrograma(obra);
                                            obrac.estado_programa=$scope.eoclosed;//Etapa de obra cerrada
                                            gestionServ.putPrograma(obrac).then(function(reb){
                                                //cierro la etapa obra
                                                gestionServ.getProyecto(obrac.id_raiz).then(function(rez){
                                                    var proyecto=rez.data;//Proyecto es obra ahora
                                                    //miro si todas las etapas obras estan cerradas
                                                    var ocerra=verificarEstados(proyecto.sons,$scope.eoclosed);
                                                    if(ocerra==true){
                                                        var project=copiarPrograma(proyecto);
                                                        project.estado_programa=$scope.oclosed;//Obra cerrada
                                                        gestionServ.putPrograma(project).then(function(reb){
                                                            //cierro la obra
                                                            reloadInfo();
                                                        });
                                                    }else{
                                                        //quedan etapas de obra
                                                        reloadInfo();
                                                    }
                                                });

                                            });
                                        }else{
                                            //quedan trabajos
                                            reloadInfo();
                                        }
                                    });
                                });
                            });

                        }else{
                            $scope.fase.estado_programa=$scope.stateclosed;//Fase cerrada
                            gestionServ.putPrograma($scope.fase).then(function(rec){
                                gestionServ.getProgramaMedioId($scope.fase.id_raiz).then(function(res){
                                    var trabajo=res.data;
                                    var nextf=nextFase($scope.fase.nombre);
                                    var startf=startFase(nextf);
                                    var nfase=buscarNombre(trabajo.sons,nextf);
                                    if(nfase.id>0){
                                        var estj=estadoTrabajo(nextf);
                                        $scope.previo.trabajo.estado_programa=estj;//Trabajo estado nuevo
                                        gestionServ.putPrograma($scope.previo.trabajo).then(function(rexa){
                                            nfase.estado_programa=startf;
                                            gestionServ.putPrograma(nfase).then(function(rea){
                                                reloadInfo();
                                            });
                                        });
                                    }else{
                                        alert("Siguiente fase no encontrada, pide soporte");
                                        reloadInfo();
                                    }
                                });
                            });
                        }
                    }
                }
            });

        }

    }
])

