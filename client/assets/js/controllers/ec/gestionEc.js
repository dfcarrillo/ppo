.controller('gestionEcCtrl', ['$scope', '$state', 'gestionServ', 'usuariosServ','generalServ', 'generalFunc', 'uiGridConstants',
    function($scope, $state, gestionServ, usuariosServ, generalServ, generalFunc, uiGridConstants){

        $('.modal').modal(
            {
                dismissible: false
            }
        );
        $('.collapsible').collapsible();
        $('select').material_select();

        if(window.sessionStorage['rol'] == "admin"){
            // $state.go("perfil_admin");
        }else if( window.sessionStorage['rol'] == 'cdc'){
            $state.go("perfil_cdc");
        }else{
            //$state.go("home");
        }

        $scope.user = window.localStorage.getItem("id_user");
        $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
        $scope.reporte={tipo:"reporte", avance:0};
        $scope.fase={};
        $scope.ejecucion="Ejecución";
        $scope.liquidacion="Liquidación";
        $scope.facturacion="Facturación";
        $scope.avanceparcial="Avance parcial";
        $scope.brigas=false;

        //loadInfo();
        testTemp();

        $scope.tablaOp={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosContraproponer.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'geo', displayName:'Supervisor', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date', sort: {direction: uiGridConstants.ASC}},
                { name: 'Accion', width:150,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openContra(row.entity)">Contraproponer</button>' }
            ],
            onRegisterApi: function( gridApi ) {
                $scope.gridcApi = gridApi;
            }
        };

        $scope.tablaOpr={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectosReportar.xlsx',
            exporterExcelSheetName: 'proyectos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'obra', displayName:'Etapa obra', minWidth:160},
                {field:'trabajo', displayName:'Trabajo', minWidth:160},
                {field:'jefe', displayName:'Jefe Area', minWidth:160},
                {field:'geo', displayName:'Supervisor', minWidth:160},
                {field:'nombre', displayName:'Fase', minWidth:160},
                {field:'fecha_limite', displayName:'Prioridad', minWidth:150, type:"date", cellFilter: 'date', sort: {direction: uiGridConstants.ASC}},
                { name: 'Accion', width:150,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openReporte(row.entity)">Reportar</button>' }
            ],
            onRegisterApi: function( gridApi ) {
                $scope.gridrApi = gridApi;
            }
        };

        $scope.tablaBri={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'brigadasContratista.xlsx',
            exporterExcelSheetName: 'brigadas',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'id', displayName:'Id', minWidth:70},
                {field:'nombre', displayName:'Nombre', minWidth:160},
                {field:'sigla', displayName:'Servicio', minWidth:100},
                { name: 'Añadir', width:80,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.addBrigada(row.entity)">Elegir</button>' }
            ]
        };

        loadInfo();

        function loadInfo(){
            getUsuario();
            var eje={id:1, name:"Ejecutado"};
            var rep={id:2, name:"Reprogramado"};
            $scope.results=[eje,rep];
            $scope.resultado=$scope.results[0];

            gestionServ.getCausas().then(function(rea){
                $scope.causas=rea.data;
                $scope.causas.splice(0,0,{id:0, causa:"Sin causa"});
                _.forEach($scope.causas,function(cauz,index){
                    cauz.disa=false;
                });
                habilitarCausas();
            });

            gestionServ.getFasesEstadoEc(2,$scope.user).then(function(res){
                $scope.proyectosc=res.data;
                $scope.tablaOp.data=$scope.proyectosc;
                _.forEach($scope.proyectosc,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.geo=pro.gp.name;
                    prioridadP(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });
            gestionServ.getFasesEstadoEc(4,$scope.user).then(function(res){
                $scope.proyectosr=res.data;
                $scope.tablaOpr.data=$scope.proyectosr;
                _.forEach($scope.proyectosr,function(pro){
                    //pro.fecha_limite=new Date(pro.fecha_limite);
                    pro.trabajo=pro.parent.nombre;
                    pro.jefe=pro.js.name;
                    pro.geo=pro.gp.name;
                    prioridadC(pro);
                    console.log(pro);
                    obraProyecto(pro);
                });
            });
        }

        function getUsuario(){
            usuariosServ.getUsuarioById($scope.user).then(function(res){
                $scope.nombre=res.data.name;
                $scope.contratista=res.data.id_contratista;
                loadBrigadas($scope.contratista);
            });
        }

        function loadBrigadas(idc){
            generalServ.getBrigadesContratista(idc).then(function(res){
                $scope.brigadas=res.data;
                $scope.tablaBri.data=$scope.brigadas;
                _.forEach($scope.brigadas,function(bri){
                    bri.sigla=bri.servicio.sigla;
                });
            });
        }

        function prioridadC(fase){
            fase.fecha_limite=new Date();
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            if (compromiso.id>0){
                fase.fecha_limite=new Date(compromiso.fecha_fin);
            }
            $scope.gridrApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
            filtroSemanas($scope.proyectosr,fase);
        }

        function prioridadP(fase){
            fase.fecha_limite=new Date();
            gestionServ.getPasosPrograma(fase.parent.id).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];
                    fase.fecha_limite=new Date(propuesta.fecha_fin);
                }
                $scope.gridcApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
                filtroSemanas($scope.proyectosc,fase);
            });
        }

        function filtroSemanas(lista,fase){
            var fecha = new Date();
            fecha.setDate(fecha.getDate()+15);
            if(fase.fecha_limite>fecha){
                deleteByIdd(lista,fase.id);
            }
        }

        function obraProyecto(fase){
            gestionServ.getProgramaMedioId(fase.parent.id_raiz).then(function(res){
                var obra=res.data;
                console.log(obra);
                fase.obra=obra.nombre;
                fase.codigo_SAP=obra.parent.codigo_SAP;
                fase.description=obra.parent.nombre;
            });
        }

        function habilitarCausas(){
            if ($scope.resultado.id==1){
                $scope.causa=$scope.causas[0];
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=true;
                    }else{
                        cauz.disa=false;
                    }
                });
            }else{
                $scope.causa=$scope.causas[1];
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=false;
                    }else{
                        cauz.disa=true;
                    }
                });
            }
        }

        $scope.habCausas=function(){
            if ($scope.resultado.id==1){
                $scope.causa=$scope.causas[0];
                $scope.reporte.avance=100;
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=true;
                    }else{
                        cauz.disa=false;
                    }
                });
            }else{
                if($scope.causa.id==0){
                    $scope.causa=$scope.causas[1];
                }
                $scope.avance();
                _.forEach($scope.causas,function(cauz,index){
                    if(index>0){
                        cauz.disa=false;
                    }else{
                        cauz.disa=true;
                    }
                    if(cauz.causa==$scope.avanceparcial && $scope.fase.nombre!=$scope.ejecucion){
                        cauz.disa=true;
                    }
                });
            }
        };

        $scope.avance=function(){
            if($scope.causa.causa!=$scope.avanceparcial){
                $scope.reporte.avance=0;
            }
        };

        function ultimoPasoTipo(pasos, tipo){
            var pasostipo=_.filter(pasos,function(step){
                return step.tipo==tipo;
            });
            var ultimo={id:0};
            if (pasostipo.length>0){
                ultimo=pasostipo[pasostipo.length-1];
            }
            return ultimo;
        }

        function leerPrevio(paso){
            $scope.previo.titulo="Propuesta previa";
            $scope.previo.tipo=paso.tipo;
            var ini=new Date(paso.fecha_inicio);
            var fin=new Date(paso.fecha_fin);
            $scope.previo.fecha_inicio=ini;
            $scope.previo.fecha_fin=fin;
            $scope.previo.comentario=paso.comentario;
            if (paso.tipo=="validacions"){
                $scope.reporte.id_compromiso=paso.id;
                $scope.previo.titulo="Validacion previa";
            }
            if (paso.tipo=="compromiso"){
                $scope.reporte.id_compromiso=paso.id;
                $scope.previo.titulo="Compromiso previo";
                $scope.previo.id_brigada=paso.id_brigada;
                leerBrigada(paso.id_brigada);
            }
        }

        function leerBrigada(idb){
            generalServ.getBrigade(idb).then(function(res){
                $scope.previo.nombre_brigada=res.data.nombre;
                $scope.previo.servicio_brigada=res.data.servicio.sigla;
            });
        }

        function previoError(){
            $scope.previo={};
            $scope.previo.titulo="Paso previo";
            $scope.previo.tipo="Otro";
            $scope.reporte.id_compromiso=0;
            $scope.previo.fecha_inicio="Error";
            $scope.previo.fecha_fin="Error";
            $scope.previo.comentario="Error: paso previo no encontrado";
        }

        function previoContrapropuesta(fase){
            $scope.previo={};
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            if (compromiso.id>0){
                leerPrevio(compromiso);
            }else{
                var propuesta=ultimoPasoTipo(fase.pasos, "propuesta");
                if (propuesta.id>0){
                    leerPrevio(propuesta);
                }else{
                    previoError();
                }
            }
        }

        function previoReporte(fase){
            $scope.previo={};
            var compromiso=ultimoPasoTipo(fase.pasos, "compromiso");
            var validacion=ultimoPasoTipo(fase.pasos, "validacions");
            if (validacion.id>0){
                leerPrevio(validacion);
                $scope.previo.role="Supervisor";
                $scope.previo.quien=fase.geo;
            }else if(compromiso.id>0){
                leerPrevio(compromiso);
                $scope.previo.role="JA";
                $scope.previo.quien=fase.jefe;
            }else{
                previoError();
            }

        }

        function deleteByIdd(lista,id){

            _.each(lista, function(tra,index){

                if(index<lista.length){
                    if(tra.id==id){

                        lista.splice(index,1);
                    }
                }
            });
        }

        function copiarPrograma(program){
            var nuevo={id:program.id};
            if (typeof program.tipo != "undefined"){
                nuevo.tipo=program.tipo;
            }
            if (typeof program.nombre != "undefined"){
                nuevo.nombre=program.nombre;
            }
            if (typeof program.estado_programa != "undefined"){
                nuevo.estado_programa=program.estado_programa;
            }
            if (typeof program.codigo_SAP != "undefined"){
                nuevo.codigo_SAP=program.codigo_SAP;
            }
            if (typeof program.uucc != "undefined"){
                nuevo.uucc=program.uucc;
            }
            if (typeof program.porcentaje_avance != "undefined"){
                nuevo.porcentaje_avance=program.porcentaje_avance;
            }
            if (typeof program.comentarios != "undefined"){
                nuevo.comentarios=program.comentarios;
            }
            if (typeof program.area_servicio != "undefined"){
                nuevo.area_servicio=program.area_servicio;
            }
            if (typeof program.num_reprog != "undefined"){
                nuevo.num_reprog=program.num_reprog;
            }else{
                nuevo.num_reprog=0;
            }

            if (typeof program.id_raiz != "undefined"){
                nuevo.id_raiz=program.id_raiz;
            }
            if (typeof program.id_pasos != "undefined"){
                nuevo.id_pasos=program.id_pasos;
            }
            if (typeof program.id_locacion != "undefined"){
                nuevo.id_locacion=program.id_locacion;
            }
            if (typeof program.fecha_limite != "undefined"){
                nuevo.fecha_limite=program.fecha_limite;
            }
            if (typeof program.fecha_cierre != "undefined"){
                nuevo.fecha_cierre=program.fecha_cierre;
            }
            if (typeof program.fecha_ingreso != "undefined"){
                nuevo.fecha_ingreso=program.fecha_ingreso;
            }

            if (typeof program.id_usuario_creacion != "undefined"){
                nuevo.id_usuario_creacion=program.id_usuario_creacion;
            }
            if (typeof program.id_js_asignado != "undefined"){
                nuevo.id_js_asignado=program.id_js_asignado;
            }
            if (typeof program.id_gp_asignado != "undefined"){
                nuevo.id_gp_asignado=program.id_gp_asignado;
            }
            if (typeof program.id_eecc_asignado != "undefined"){
                nuevo.id_eecc_asignado=program.id_eecc_asignado;
            }
            if (typeof program.id_brigada_asignada != "undefined"){
                nuevo.id_brigada_asignada=program.id_brigada_asignada;
            }
            if (typeof program.nombre_locacion != "undefined"){
                nuevo.nombre_locacion=program.nombre_locacion;
            }
            return nuevo;
        }

        //DatePicker
        $('.datepicker').pickadate({
            monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
            weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            today: 'Hoy',
            clear: 'Borrar',
            close: 'Aceptar',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: true, // Creates a dropdown to control year
            selectTime: true,
            container: 'body',
            format: 'yyyy-mm-dd'
            //onClose: function(){
            //  $("#mdlTrainingDetail form input:eq(1)").focus();
            //}
        });

        //$('.timepicker').pickatime({});

        function testTemp(){
            $scope.nombre="Felipe Ascencio";
            $scope.perfil="Empresa Contratista";

            $scope.previo={};
            $scope.previo.fecha_inicio=new Date(2018,3,20);
            $scope.previo.fecha_fin=new Date(2018,3,29);
            $scope.previo.comentario="El trabajo debe hacerse en horas de la mañana";
            $scope.previo.id_brigada=10;
            $scope.previo.nombre_brigada="Lineas Sendas 1";
            $scope.previo.servicio_brigada="Solicitudes comerciales e internas (nuevas conexiones y otros)";
        }

        $scope.modal1=function(){
            $('#contrapropuesta').modal('open');
        };

        $scope.modal2=function(){
            $('#reporte').modal('open');
        };

        $scope.openContra=function(fase){
            $('#contrapropuesta').modal('open');
            previoContrapropuesta(fase);
            $scope.paso.id_programa=fase.id;
            $scope.paso.tipo="contrapropuesta";
            $scope.fase=copiarPrograma(fase);
            $scope.job={};
            $scope.job.codigo_SAP=fase.codigo_SAP;
            $scope.job.description=fase.description;
            $scope.job.nom_tra=fase.parent.nombre;
            propuestaTrabajo(fase.parent.id);
        };

        function propuestaTrabajo(idt){

            $scope.job.inicio=new Date(2010,1,1);
            $scope.job.fin=new Date(2058,1,1);

            gestionServ.getPasosPrograma(idt).then(function(res){
                if (res.data.length>0){
                    var propuesta=res.data[0];

                    $scope.job.inicio=new Date(propuesta.fecha_inicio);
                    $scope.job.fin=new Date(propuesta.fecha_fin);
                    $scope.job.comentario=propuesta.comentario;
                }
                console.log($scope.job);
            });
        }

        $scope.openReporte=function(fase){
            $('#reporte').modal('open');
            //$scope.gridrApi.core.notifyDataChange( uiGridConstants.dataChange.EDIT );
            previoReporte(fase);
            $scope.reporte.id_programa=fase.id;
            $scope.reporte.tipo="reporte";
            $scope.fase=copiarPrograma(fase);
            $scope.previo.trabajo=fase.parent;
            $scope.previo.num_reprog=fase.num_reprog;
            $scope.job={};
            $scope.job.codigo_SAP=fase.codigo_SAP;
            $scope.job.description=fase.description;
            $scope.job.nom_tra=fase.parent.nombre;
            propuestaTrabajo(fase.parent.id);
            $scope.habCausas();
        };

        $scope.cancelarC=function(){
            $('#contrapropuesta').modal('close');
            $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
            $scope.fase={};
            $scope.previo={};
        };

        $scope.cancelarReporte=function(){
            $('#reporte').modal('close');
            $scope.reporte={tipo:"reporte", avance:0};
            $scope.fase={};
            $scope.previo={};
            $scope.resultado=$scope.results[0];
            habilitarCausas();
        };

        $scope.contraproponer= function(){

            validarC();

            if ($scope.errores.length==0){
                $('#contrapropuesta').modal('close');
                $scope.paso.fecha_creacion=new Date();
                var ms=$scope.paso.fecha_inicio._d;
                var msf=$scope.paso.fecha_fin._d;
                $scope.paso.fecha_inicio=ms;
                $scope.paso.fecha_fin=msf;
                $scope.fase.codigo_SAP=undefined;
                console.log($scope.paso.fecha_inicio);
                gestionServ.postPaso($scope.paso).then(function(res){
                    $scope.fase.estado_programa=3;
                    gestionServ.putPrograma($scope.fase).then(function(rea){
                        $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
                        $scope.fase={};
                        $scope.previo={};
                        loadInfo();
                    });
                });
            }else{
                $('#contrapropuesta').modal('close');
                $('#errores').modal('open');
            }

        };

        function buscarNombre(lista,nombre){
            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.nombre==nombre){
                    obj=objeto;
                }
            });
            return obj;
        }

        function verificarEstados(lista,estado){
            var resp=true;
            _.forEach(lista,function(program){
                if(program.estado_programa!=estado){
                    resp=false;
                }
            });
            return resp;
        }

        function reloadInfo(){
            $scope.reporte={tipo:"reporte", avance:0};
            $scope.resultado=$scope.results[0];
            $scope.fase={};
            $scope.previo={};
            loadInfo();
        }

        function validarC(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.paso.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.paso.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.paso.fecha_fin;
                res1=validarCampo($scope.paso.fecha_inicio,"less",info);
                if (res1=="ok"){
                    info.max=$scope.job.fin;
                    info.min=$scope.job.inicio;
                    res1=validarCampo($scope.paso.fecha_inicio,"inrange",info);
                    res2=validarCampo($scope.paso.fecha_fin,"inrange",info);
                    if (res1=="ok" && res2=="ok"){

                    }else{
                        //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    }
                    info.min=new Date();
                    res1=validarCampo($scope.paso.fecha_inicio,"greater",info);
                    res2=validarCampo($scope.paso.fecha_fin,"greater",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser posteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.paso.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.paso.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

            res=validarCampo($scope.paso.id_brigada,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
            }else{
                info.min=1;
                res=validarCampo($scope.paso.id_brigada,"greater",info);
                if (res!="ok"){
                    $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
                }
            }

            res=validarCampo($scope.paso.supervisor,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Supervisor: "+res);
            }else{
                res=validarCampo($scope.paso.supervisor,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Supervisor: "+res);
                }
            }

        }

        function validarR(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.reporte.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.reporte.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.reporte.fecha_fin;
                res1=validarCampo($scope.reporte.fecha_inicio,"less",info);
                if (res1=="ok"){
                    info.max=$scope.job.fin;
                    info.min=$scope.job.inicio;
                    res1=validarCampo($scope.reporte.fecha_inicio,"inrange",info);
                    res2=validarCampo($scope.reporte.fecha_fin,"inrange",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    }
                    info.max=new Date();
                    res1=validarCampo($scope.reporte.fecha_inicio,"less",info);
                    res2=validarCampo($scope.reporte.fecha_fin,"less",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser anteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.reporte.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.reporte.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

            res=validarCampo($scope.reporte.avance,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Avance: "+res);
            }else{
                if($scope.causa.causa==$scope.avanceparcial){
                    info.min=1;
                    info.max=95;
                    res=validarCampo($scope.reporte.avance,"inrange",info);
                    if (res!="ok"){
                        $scope.errores.push("Avance: "+res);
                    }
                }

            }

        }

        function validarCampo(valor,tipo,info){

            var resul="Error de tipo de validacion";

            if (tipo=="undefined"){
                if (typeof valor!="undefined"){
                    resul="ok";
                }else{
                    resul="El campo no puede ser vacio";
                }
            }else if(tipo=="emptystring"){
                if (valor==""){
                    resul="El campo no puede ser vacio";
                }else{
                    resul="ok";
                }
            }else if(tipo=="different"){
                if (valor==info.igual){
                    resul="El campo no puede ser igual a "+info.igual;
                }else{
                    resul="ok";
                }
            }else if(tipo=="equal"){
                if (valor==info.igual){
                    resul="ok";
                }else{
                    resul="El campo debe ser igual a "+info.igual;
                }
            }else if(tipo=="greater"){
                if(valor>=info.min){
                    resul="ok";
                }else{
                    resul="El campo debe ser mayor a "+info.min;
                }
            }else if(tipo=="less"){
                if(valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe ser menor a "+info.max;
                }
            }else if(tipo=="inrange"){
                if(info.min<=valor && valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe estar entre "+info.min+" y "+info.max;
                }
            }else if(tipo=="datatype"){
                if(typeof valor==info.tipo){
                    resul="ok";
                }else{
                    resul="Se espera que el valor sea tipo "+info.tipo;
                }
            }
            return resul;
        }

        $scope.closeErrores=function (){

            $('#errores').modal('close');
            if ($scope.fase.estado_programa==2){
                $('#contrapropuesta').modal('open');
            }else if($scope.fase.estado_programa==4){
                $('#reporte').modal('open');
            }
        };

        function reportard(){

            $('#reporte').modal('close');
            $scope.reporte.fecha_creacion=new Date();
            var ms=$scope.reporte.fecha_inicio._d;
            var msf=$scope.reporte.fecha_fin._d;
            $scope.reporte.fecha_inicio=ms;
            $scope.reporte.fecha_fin=msf;
            $scope.reporte.estado=$scope.resultado.id;
            $scope.fase.codigo_SAP=undefined;
            if($scope.causa.id>0){
                $scope.reporte.id_causa=$scope.causa.id;
            }
            //guardo el reporte
            gestionServ.postPaso($scope.reporte).then(function(ree){

                if($scope.reporte.estado==2){
                    if($scope.fase.nombre==$scope.ejecucion){
                        $scope.fase.estado_programa=3;
                        $scope.fase.num_reprog=$scope.fase.num_reprog+1;
                    }else{
                        $scope.fase.estado_programa=5;
                    }
                    //si es reprogamado siempre vamos a compromiso
                    gestionServ.putPrograma($scope.fase).then(function(rea){
                        reloadInfo();
                    });
                }else{
                    if($scope.fase.nombre==$scope.ejecucion){
                        $scope.fase.estado_programa=5;
                        gestionServ.putPrograma($scope.fase).then(function(reb){
                            reloadInfo();
                        });
                    }else{
                        //Siempre hay validacion!
                        $scope.fase.estado_programa=5;
                        gestionServ.putPrograma($scope.fase).then(function(reb){
                            reloadInfo();
                        });
                    }
                }
            });
        }

        $scope.reportar= function(){

            validarR();
            if ($scope.errores.length==0){
                reportard();
            }else{
                $('#reporte').modal('close');
                $('#errores').modal('open');
            }

        };

        $scope.logAlgo= function(objeto){
            console.log(objeto);
        };

        $scope.cancelarB=function(){
            $('#brigadas').modal('close');
            $('#contrapropuesta').modal('open');
            $scope.brigas=false;
        };

        $scope.openBrigadas=function(){
            $('#contrapropuesta').modal('close');
            $('#brigadas').modal('open');
            $scope.brigas=true;
        };

        $scope.addBrigada=function(briga){
            $('#brigadas').modal('close');
            $('#contrapropuesta').modal('open');
            $scope.paso.id_brigada=briga.id;
            $scope.previo.name=briga.nombre;
            $scope.previo.sigla=briga.sigla;
            $scope.brigas=false;
        };

    }
])