.controller('programarEcCtrl', ['$scope', '$state', 'gestionServ', 'usuariosServ','generalServ', 'generalFunc', 'uiGridConstants',
    function($scope, $state, gestionServ, usuariosServ, generalServ, generalFunc, uiGridConstants){

        $('.modal').modal(
            {
                dismissible: false
            }
        );
        $('.collapsible').collapsible();
        $('select').material_select();

        if(window.sessionStorage['rol'] == "admin"){
            // $state.go("perfil_admin");
        }else if( window.sessionStorage['rol'] == 'cdc'){
            $state.go("perfil_cdc");
        }else{
            //$state.go("home");
        }

        $scope.user = window.localStorage.getItem("id_user");
        $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
        $scope.job={};
        $scope.previo={};
        $scope.etapa={id:0};
        $scope.brigas=false;
        //NOMBRE ESTANDARES DE LAS FASES
        $scope.replanteo="Replanteo";
        $scope.ejecucion="Ejecución";
        $scope.cuadratura="Cuadratura de Materiales";
        $scope.estado="Estado de Pago";
        $scope.facturacion="Facturación";
        //OTROS ESTADOS DE LA TABLA PROGRAMA
        $scope.eoclosed=4;//Etapa de obra cerrada
        $scope.oclosed=3;//Obra cerrada
        //ESTADOS DEL TRABAJO SEGUN FASE ACTUAL
        $scope.jobej=1;//En ejecucion
        $scope.jobep=3;//En estado de pago
        $scope.jobfa=4;//En facturacion
        $scope.jobclosed=5;//Trabajo cerrado

        $scope.tablaEta={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'EtapasEC.xlsx',
            exporterExcelSheetName: 'etapas',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'codigo_SAP', displayName:'Codigo', minWidth:160},
                {field:'numtra', displayName:'# Trabajos', minWidth:160},
                {field:'nombre_locacion', displayName:'Localizacion', minWidth:160},
                {field:'servicio', displayName:'Servicio', minWidth:160},
                {field:'uucc', displayName:'UUCC', minWidth:160},
                {field:'fecha_limite', displayName:'Plazo', minWidth:160, type:"date", cellFilter: 'date'},
                {field:'semana', displayName:'Semana', minWidth:150},
                { name: 'Ver Trabajos', width:150,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.verTrabajos(row.entity)">Ver</button>' }
            ]
        };

        $scope.tablaTra={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'Trabajos.xlsx',
            exporterExcelSheetName: 'trabajos',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'nombre', displayName:'Descripcion', minWidth:160},
                {field:'uucc', displayName:'UUCC', minWidth:160},
                {field:'fase', displayName:'Fase', minWidth:160},
                {field:'paso', displayName:'Paso actual', minWidth:160},
                {field:'inicio', displayName:'Fecha inicio', minWidth:160, type:"date", cellFilter: 'date:\'dd-MM-yyyy  h:mma\''},
                {field:'fin', displayName:'Fecha fin', minWidth:160, type:"date", cellFilter: 'date:\'dd-MM-yyyy  h:mma\''},
                {field:'comment', displayName:'Comentario', minWidth:150}
            ]
        };

        $scope.tablaBri={
            enableFiltering: true,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:false,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'brigadasContratista.xlsx',
            exporterExcelSheetName: 'brigadas',
            //data:$scope.vehiculos,
            columnDefs:[
                {field:'id', displayName:'Id', minWidth:70},
                {field:'nombre', displayName:'Nombre', minWidth:160},
                {field:'sigla', displayName:'Servicio', minWidth:100},
                { name: 'Añadir', width:80,
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.addBrigada(row.entity)">Elegir</button>' }
            ]
        };

        loadData();

        function loadData(){
            getUsuario();
            gestionServ.getFasesEc($scope.user).then(function(res){
                $scope.fases=res.data;
                gestionServ.getEtapasEc($scope.user).then(function(rea){
                    $scope.etapas=_.filter(rea.data,filtroEtapas);
                    $scope.tablaEta.data=$scope.etapas;
                    _.forEach($scope.etapas,completarEtapa);
                    reloadEtapa();
                });
            });
        }

        function reloadEtapa(){
            if($scope.etapa.id>0){
                var stage=buscarId($scope.etapas,$scope.etapa.id);
                $scope.verTrabajos(stage);
            }
        }

        function getUsuario(){
            usuariosServ.getUsuarioById($scope.user).then(function(res){
                $scope.nombre=res.data.name;
                $scope.contratista=res.data.id_contratista;
                loadBrigadas($scope.contratista);
            });
        }

        function loadBrigadas(idc){
            generalServ.getBrigadesContratista(idc).then(function(res){
                $scope.brigadas=res.data;
                $scope.tablaBri.data=$scope.brigadas;
                _.forEach($scope.brigadas,function(bri){
                    bri.sigla=bri.servicio.sigla;
                });
            });
        }

        function completarEtapa(etap){
            if (etap.parent.tipo=="proyecto"){
                etap.codigo_SAP=etap.parent.codigo_SAP;
            }else{
                gestionServ.getProgramaMedioId(etap.parent.id_raiz).then(function(res){
                    etap.codigo_SAP=res.data.codigo_SAP;
                });
            }
            etap.numtra=etap.sons.length;
            etap.servicio="OTRO"; //revisar --> Leo la relacion con el servicio de la brigada cuando esta exista
            etap.semana=semanaFecha(etap.fecha_limite); //revisar --> Como se la semana de la etapa actual
            _.forEach(etap.sons,completarTrabajo);

        }

        function completarTrabajo(traba){
            traba.fases=_.filter($scope.fases,function(fase){
                return fase.id_raiz==traba.id;
            });
            if(traba.estado_programa==0 || traba.estado_programa==1){
                traba.fase=$scope.ejecucion;
            }else if(traba.estado_programa==2){
                traba.fase=$scope.cuadratura;
            }
            else if(traba.estado_programa==3){
                traba.fase=$scope.estado;
            }else{
                traba.fase=$scope.facturacion;
            }
            var current=buscarNombre(traba.fases,traba.fase);
            console.log(current);
            if (current.id>0){
                if(current.pasos.length>0){
                    var paso=current.pasos[current.pasos.length-1];
                    traba.paso=paso.tipo;
                    traba.inicio=paso.fecha_inicio;
                    traba.fin=paso.fecha_fin;
                    traba.comment=paso.comentario;
                }else{
                    traba.paso="Sin pasos";
                    traba.comment="Sin programacion";
                }
            }else{
                traba.paso="Error: Fase acual no encontrada";
            }
        }

        function buscarNombre(lista,nombre){
            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.nombre==nombre){
                    obj=objeto;
                }
            });
            return obj;
        }

        function buscarId(lista,id){

            var obj={id:0};
            _.forEach(lista,function (objeto){

                if(objeto.id==id){
                    obj=objeto;
                }

            });

            return obj;
        }

        function semanaFecha(fecha){
            var temp=new Date(fecha);
            var year = temp.getFullYear();
            var month = temp.getMonth();
            var date = temp.getDate();
            var d = new Date(Date.UTC(year,month,date));
            var dayNum = d.getUTCDay() || 7;
            d.setUTCDate(d.getUTCDate() + 4 - dayNum);
            var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
            var week = Math.ceil((((d - yearStart) / 86400000) + 1)/7);
            return week;
        }

        function filtroEtapas(etap){
            var res=true;
            if (etap.estado_programa==0){
                res=false;
            }
            if (etap.estado_programa==$scope.eoclosed){
                res=false;
            }
            //revisar --> Aca mirar si tenemos etapa grande o pequeña y cual queremos
            return res;
        }

        $scope.verTrabajos=function(etap){

            $scope.trabajos=etap.sons;
            $scope.tablaTra.data=$scope.trabajos;
            $scope.etapa=etap;
            $scope.etapa.idaux=etap.id;
        };

        $scope.nuevo=function(){
            $('#trabajo').modal('open');
        }

        $scope.cancelar=function(){
            $('#trabajo').modal('close');
            $scope.job={};
            $scope.previo={};
            $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};

        };

        $scope.openBrigadas=function(){
            $('#trabajo').modal('close');
            $('#brigadas').modal('open');
            $scope.brigas=true;
        };

        $scope.addBrigada=function(briga){
            $('#brigadas').modal('close');
            $('#trabajo').modal('open');
            $scope.paso.id_brigada=briga.id;
            $scope.previo.name=briga.nombre;
            $scope.previo.sigla=briga.sigla;
            $scope.brigas=false;
        };

        $scope.cancelarB=function(){
            $('#brigadas').modal('close');
            $('#trabajo').modal('open');
            $scope.brigas=false;
        };

        $scope.setFin=function(){
            $scope.paso.fecha_fin=moment($scope.paso.fecha_inicio);
            console.log($scope.paso.fecha_inicio);
            console.log($scope.paso.fecha_fin);
        };

        function buildTrabajo() {

            $scope.job.tipo = 'trabajo';
            $scope.job.comentarios = "Trabajo creado por la EC";
            $scope.job.estado_programa = 1;
            $scope.job.porcentaje_avance = 0;
            $scope.job.fecha_ingreso = new Date();
            $scope.job.id_usuario_creacion = $scope.user;
            var fecha=new Date($scope.paso.fecha_fin._d);
            fecha.setDate(fecha.getDate()+30);
            $scope.job.fecha_limite =fecha;

            $scope.job.id_raiz = $scope.etapa.id;
            $scope.job.id_locacion = $scope.etapa.id_locacion;
            $scope.job.nombre_locacion = $scope.etapa.nombre_locacion;
            $scope.job.id_js_asignado = $scope.etapa.id_js_asignado;
            $scope.job.id_gp_asignado = $scope.etapa.id_gp_asignado;
            $scope.job.id_eecc_asignado = $scope.etapa.id_eecc_asignado;
            $scope.job.area_servicio = $scope.etapa.area_servicio;

        }

        function buildFases(trabajo){
            var fases = [], ejecucion = materiales = pago = facturacion = {};
            ejecucion = {"nombre": $scope.ejecucion, "estado_programa": "3"}; materiales = {"nombre": $scope.cuadratura};
            pago = {"nombre": $scope.estado}; facturacion = {"nombre": $scope.facturacion};
            ejecucion.tipo = materiales.tipo = pago.tipo = facturacion.tipo = 'fase';
            ejecucion.id_raiz = materiales.id_raiz = pago.id_raiz = facturacion.id_raiz = trabajo.id;
            ejecucion.id_locacion = materiales.id_locacion = pago.id_locacion = facturacion.id_locacion = trabajo.id_locacion;
            ejecucion.nombre_locacion = materiales.nombre_locacion = pago.nombre_locacion = facturacion.nombre_locacion = trabajo.nombre_locacion;
            ejecucion.id_usuario_creacion = materiales.id_usuario_creacion = pago.id_usuario_creacion = facturacion.id_usuario_creacion = trabajo.id_usuario_creacion;
            ejecucion.id_js_asignado = materiales.id_js_asignado = pago.id_js_asignado = facturacion.id_js_asignado = trabajo.id_js_asignado;
            ejecucion.id_gp_asignado = materiales.id_gp_asignado = pago.id_gp_asignado = facturacion.id_gp_asignado = trabajo.id_gp_asignado;
            ejecucion.id_eecc_asignado = materiales.id_eecc_asignado = pago.id_eecc_asignado = facturacion.id_eecc_asignado = trabajo.id_eecc_asignado;
            ejecucion.area_servicio = materiales.area_servicio = pago.area_servicio = facturacion.area_servicio = trabajo.area_servicio;
            fases.push(ejecucion); fases.push(facturacion); fases.push(pago);
            fases.push(materiales);

            return fases;
        }

        $scope.crear=function(){
            validarC(); //pendiente
            if ($scope.errores.length==0){
                creard();
            }else{
                $('#trabajo').modal('close');
                $('#errores').modal('open');
            }
        };

        function buildPaso(fase){
            $scope.paso.id_programa=fase.id;
            $scope.paso.tipo="contrapropuesta";
            $scope.paso.fecha_creacion=new Date();
            var ms=$scope.paso.fecha_inicio._d;
            var msf=$scope.paso.fecha_fin._d;
            $scope.paso.fecha_inicio=ms;
            $scope.paso.fecha_fin=msf;
        }

        function creard(){
            buildTrabajo();
            $scope.total=3;
            $scope.count=0;
            gestionServ.postPrograma($scope.job).then(function(res){
                var fases=buildFases(res.data);
                var propuesta={id_programa:res.data.id, tipo:"propuesta"}
                propuesta.fecha_creacion=new Date();
                propuesta.fecha_inicio=$scope.paso.fecha_inicio._d;
                propuesta.fecha_fin=$scope.paso.fecha_fin._d;
                propuesta.comentario=$scope.paso.comentario;
                gestionServ.postPaso(propuesta).then(function(rea){
                    _.forEach(fases,crearFase);
                });
            });
        }

        function crearFase(fase){
            gestionServ.postPrograma(fase).then(function(res){
                if (res.data.nombre==$scope.ejecucion){
                    buildPaso(res.data);
                    gestionServ.postPaso($scope.paso).then(saveDetalles);
                }else{
                    saveDetalles(1);
                }
            });
        }

        function saveDetalles(res){
            $scope.count=$scope.count+1;
            if($scope.count==$scope.total){
                $scope.job={};
                $scope.previo={};
                $scope.paso={tipo:"contrapropuesta", replanteo:false, permiso:false, materiales:false, poda:false, descargo:false, tarjeta_amarilla:false};
                loadData();
            }
        }

        function copiarPrograma(program){
            var nuevo={id:program.id};
            if (typeof program.tipo != "undefined"){
                nuevo.tipo=program.tipo;
            }
            if (typeof program.nombre != "undefined"){
                nuevo.nombre=program.nombre;
            }
            if (typeof program.estado_programa != "undefined"){
                nuevo.estado_programa=program.estado_programa;
            }
            if (typeof program.codigo_SAP != "undefined"){
                nuevo.codigo_SAP=program.codigo_SAP;
            }
            if (typeof program.uucc != "undefined"){
                nuevo.uucc=program.uucc;
            }
            if (typeof program.porcentaje_avance != "undefined"){
                nuevo.porcentaje_avance=program.porcentaje_avance;
            }
            if (typeof program.comentarios != "undefined"){
                nuevo.comentarios=program.comentarios;
            }
            if (typeof program.area_servicio != "undefined"){
                nuevo.area_servicio=program.area_servicio;
            }
            if (typeof program.num_reprog != "undefined"){
                nuevo.num_reprog=program.num_reprog;
            }else{
                nuevo.num_reprog=0;
            }

            if (typeof program.id_raiz != "undefined"){
                nuevo.id_raiz=program.id_raiz;
            }
            if (typeof program.id_pasos != "undefined"){
                nuevo.id_pasos=program.id_pasos;
            }
            if (typeof program.id_locacion != "undefined"){
                nuevo.id_locacion=program.id_locacion;
            }
            if (typeof program.fecha_limite != "undefined"){
                nuevo.fecha_limite=program.fecha_limite;
            }
            if (typeof program.fecha_cierre != "undefined"){
                nuevo.fecha_cierre=program.fecha_cierre;
            }
            if (typeof program.fecha_ingreso != "undefined"){
                nuevo.fecha_ingreso=program.fecha_ingreso;
            }

            if (typeof program.id_usuario_creacion != "undefined"){
                nuevo.id_usuario_creacion=program.id_usuario_creacion;
            }
            if (typeof program.id_js_asignado != "undefined"){
                nuevo.id_js_asignado=program.id_js_asignado;
            }
            if (typeof program.id_gp_asignado != "undefined"){
                nuevo.id_gp_asignado=program.id_gp_asignado;
            }
            if (typeof program.id_eecc_asignado != "undefined"){
                nuevo.id_eecc_asignado=program.id_eecc_asignado;
            }
            if (typeof program.id_brigada_asignada != "undefined"){
                nuevo.id_brigada_asignada=program.id_brigada_asignada;
            }
            if (typeof program.nombre_locacion != "undefined"){
                nuevo.nombre_locacion=program.nombre_locacion;
            }
            return nuevo;
        }

        function validarC(){
            $scope.errores=[];
            var info={};
            var res1=validarCampo($scope.paso.fecha_inicio,"undefined",info);
            var res2=validarCampo($scope.paso.fecha_fin,"undefined",info);
            if (res1=="ok" && res2=="ok"){
                info.max=$scope.paso.fecha_fin;
                res1=validarCampo($scope.paso.fecha_inicio,"less",info);
                if (res1=="ok"){
                    // info.max=$scope.job.fin;
                    // info.min=$scope.job.inicio;
                    // res1=validarCampo($scope.paso.fecha_inicio,"inrange",info);
                    // res2=validarCampo($scope.paso.fecha_fin,"inrange",info);
                    // if (res1=="ok" && res2=="ok"){

                    // }else{
                    //   //$scope.errores.push("Las fechas definidas deben estar contenidas por las fechas propuestas para el trabajo");
                    // }
                    info.min=new Date();
                    res1=validarCampo($scope.paso.fecha_inicio,"greater",info);
                    res2=validarCampo($scope.paso.fecha_fin,"greater",info);
                    if (res1=="ok" && res2=="ok"){
                    }else{
                        $scope.errores.push("Las fechas definidas deben ser posteriores a la fecha de hoy");
                    }
                }else{
                    $scope.errores.push("La fecha fin debe ser posterior a la fecha inicio");
                }
            }else{
                $scope.errores.push("Se debe definir una fecha inicio y una fecha fin");
            }

            var res=validarCampo($scope.paso.comentario,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Comentario: "+res);
            }else{
                res=validarCampo($scope.paso.comentario,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Comentario: "+res);
                }
            }

            res=validarCampo($scope.paso.id_brigada,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
            }else{
                info.min=1;
                res=validarCampo($scope.paso.id_brigada,"greater",info);
                if (res!="ok"){
                    $scope.errores.push("Brigada Siver: No se ha seleecionado una brigada");
                }
            }

            res=validarCampo($scope.paso.supervisor,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Supervisor: "+res);
            }else{
                res=validarCampo($scope.paso.supervisor,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Supervisor: "+res);
                }
            }

            res=validarCampo($scope.job.nombre,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("Nombre trabajo: "+res);
            }else{
                res=validarCampo($scope.job.nombre,"emptystring",info);
                if (res!="ok"){
                    $scope.errores.push("Nombre trabajo: "+res);
                }
            }

            res=validarCampo($scope.job.uucc,"undefined",info);
            if (res!="ok"){
                $scope.errores.push("UUCC: "+res);
            }else{
                info.min=1;
                res=validarCampo($scope.job.uucc,"greater",info);
                if (res!="ok"){
                    $scope.errores.push("UUCC: "+res);
                }
            }

        }

        function validarCampo(valor,tipo,info){

            var resul="Error de tipo de validacion";

            if (tipo=="undefined"){
                if (typeof valor!="undefined"){
                    resul="ok";
                }else{
                    resul="El campo no puede ser vacio";
                }
            }else if(tipo=="emptystring"){
                if (valor==""){
                    resul="El campo no puede ser vacio";
                }else{
                    resul="ok";
                }
            }else if(tipo=="different"){
                if (valor==info.igual){
                    resul="El campo no puede ser igual a "+info.igual;
                }else{
                    resul="ok";
                }
            }else if(tipo=="equal"){
                if (valor==info.igual){
                    resul="ok";
                }else{
                    resul="El campo debe ser igual a "+info.igual;
                }
            }else if(tipo=="greater"){
                if(valor>=info.min){
                    resul="ok";
                }else{
                    resul="El campo debe ser mayor a "+info.min;
                }
            }else if(tipo=="less"){
                if(valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe ser menor a "+info.max;
                }
            }else if(tipo=="inrange"){
                if(info.min<=valor && valor<=info.max){
                    resul="ok";
                }else{
                    resul="El campo debe estar entre "+info.min+" y "+info.max;
                }
            }else if(tipo=="datatype"){
                if(typeof valor==info.tipo){
                    resul="ok";
                }else{
                    resul="Se espera que el valor sea tipo "+info.tipo;
                }
            }
            return resul;
        }

        $scope.closeErrores=function (){
            $('#errores').modal('close');
            $('#trabajo').modal('open');
        };

        $scope.logAlgo= function(objeto){
            console.log(objeto);
        };

        //------------------------------------
        //Fin nuevo controlador
        //------------------------------------

    }
])

