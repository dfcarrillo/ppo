.controller('adminResetPassCtrl', ['$scope', '$state', 'usuariosServ',
    function($scope, $state, usuariosServ){

        $('.modal').modal({
            dismissible: false
        });

        $('#newPass').modal('open');

        $scope.formSend = true;

        if (window.location.href.slice('51') == "") {
            $scope.request = true;
        } else {
            $scope.request = false;
            window.localStorage.setItem('token', window.location.href.slice('51'));
        }

        $scope.resetPassword = function(user){
            usuariosServ.resetPasswd(user)
                .then(success, error)
        }

        function success(res) {
            if (res.status == 204){
                $scope.formSend = false;
            } else {
                var $toastContent = $('<span>La dirección de correo electrónico no está registrada en nuestra base de datos.</span>');
                Materialize.toast($toastContent, 3000);
                $scope.cancelar();
            }
        }

        function error(res){
            var $toastContent = $('<span>La dirección de correo electrónico no está registrada en nuestra base de datos.</span>');
            Materialize.toast($toastContent, 3000);
            $scope.cancelar();
        }

        $scope.changePassword = function(new_passwd) {
            usuariosServ.changePasswd(new_passwd)
                .then(function(res){
                    var $toastContent = $('<span>Contraseña actualizada éxitosamente!</span>');
                    Materialize.toast($toastContent, 3000);
                    $scope.cancelar()
                })
        }

        $scope.cancelar = function(){
            $('#newPass').modal('close');
            $state.go('home');
        }
    }
])