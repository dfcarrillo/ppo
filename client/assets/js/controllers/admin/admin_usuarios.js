.controller('adminUsuariosCtrl', ['$scope', '$state', 'usuariosServ', 'generalFunc', 'generalServ',
  function($scope, $state, usuariosServ, generalFunc, generalServ){

    $('.modal').modal(
      {
        dismissible: false
      }
    );
    $('.collapsible').collapsible();
    $('select').material_select();

    loadScopes();

    function loadScopes() {
      generalFunc.routeAdmin();
      loadlocaciones();
      loadRoles();
      loadContratistasSiver();
    }

    function loadUsers(){
        usuariosServ.getUsuarios()
            .then(function (res){
                $scope.usuarios = res.data;
                _.forEach(res.data, setLocacion)
            })
    }

    function setLocacion(loc, i){
        var locacion =  _.find($scope.locaciones, ['id', loc.id_locacion]);
        $scope.usuarios[i].locacion = locacion.nombre;
    }

    //load roles
    function loadRoles() {
        usuariosServ.getRoles()
            .then(function (res){
                $scope.roles = res.data;
            })
    }

    //load locaciones
    function loadlocaciones(){
        generalServ.getLocaciones()
            .then(function(res) {
                loadUsers();
                $scope.locaciones = res.data;
            });
    }

    //load contratistas Siver
    function loadContratistasSiver() {
        generalServ.getContratistas()
            .then(function(res){
                $scope.contratistas = res.data;
            })
    }

    //Crear usuario
    $scope.createUser = function(usuario) {
        usuariosServ.postUsuario(usuario)
          .then(successUsers, errorUsers)
    }

    function successUsers(res) {
        loadUsers();
        var $toastContent = $('<span>Cuenta creada éxitosamente.</span>');
        Materialize.toast($toastContent, 5000);
        $scope.closeUsuarioModal();
    }

    function errorUsers(res){
        var $toastContent = $('<span>Se generó un error, inténtelo de nuevo</span>');
        Materialize.toast($toastContent, 5000);
    }

    //Delete User
    $scope.deleteUser = function(id) {
        swal({
            title: 'Atención!',
            text: '¿Está seguro de eliminar el Usuario?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).
            then(function(res) {
               if(res) {
                    usuariosServ.deleteUser(id)
                        .then(function(res){
                            var $toastContent = $('<span>Usuario eliminado exitosamente!</span>');
                            Materialize.toast($toastContent, 3000);
                            loadUsers();
                        })
                }
            })
    }

    //update User
      $scope.updateUser = function(user) {
        usuariosServ.updateUser(user)
            .then(function(res){
                var $toastContent = $('<span>Usuario actualizado exitosamente!</span>');
                Materialize.toast($toastContent, 3000);
                $scope.closeUsuarioModal();
                loadUsers();
            })
      }

    $scope.viewUser = function(user) {
        $scope.usuario = user;
        $scope.edit = false;
        $('#newUsuarios').modal('open');
    }


    //close crear usuario modal
    $scope.closeUsuarioModal = function () {
        delete $scope.usuario;
      $('#newUsuarios').modal('close');
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion();
    }
  }
])
