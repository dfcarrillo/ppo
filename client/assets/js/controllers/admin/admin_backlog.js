.controller('adminBacklogCtrl', ['$scope', '$state', '$stateParams', 'backlogsServ', 'usuariosServ', 'generalFunc',
    function($scope, $state, $stateParams, backlogsServ, usuariosServ, generalFunc){

        $('.modal').modal(
            {
                dismissible: false
            }
        );

        if (window.localStorage['rol'] == "admin") {
            //$state.go("perfil_admin");
        } else {
            $state.go("home");
        }

        getBacklogs();

        function getBacklogs() {
            if($stateParams.id){
                backlogsServ.getBacklogsById($stateParams.id)
                .then(function(res) {
                    $scope.backlogDetail = res.data;
                    $scope.detail = true;
                    $scope.backlogDetail.sons = _.orderBy($scope.backlogDetail.sons,['fecha_creacion'],['desc'])
                    _.forEach(res.data.sons, setUser);
                })
            } else {
                backlogsServ.getBacklogs()
                .then(function(res) {
                    $scope.detail = false;
                    $scope.backlogs = res.data;
                })
            } 
        }

        function setUser(user, i) {
            usuariosServ.getUsuarioById(user.id_usuario)
                .then(function (res) {
                    $scope.backlogDetail.sons[i].username = res.data.name;
                })
        }

        $scope.crearSugerencia = function(backlog) {
            backlog.id_usuario = Number(window.localStorage['id_user']);
            backlogsServ.postBacklogs(backlog)
                .then(function(res){
                    getBacklogs();
                    $('#feedModal').modal('close');
                    $scope.backlog = null;
                })

        }

        $scope.vote = function(b) {
            b.likes ++
            backlogsServ.putBacklogs(b)
                .then(function(res){})
        }

        $scope.saveComment = function(id, comment) {
            $scope.comment = '';
            var bd = {};
            bd.id_usuario = window.localStorage['id_user'];
            bd.id_backlog = id;
            bd.descripcion = comment;
            backlogsServ.postBacklogs(bd)
                .then(function(res){
                    getBacklogs();
                })
        }

        $scope.viewDetail = function(b) {
            $scope.priorities = [1, 2, 3, 4, 5];
            $scope.feedDetail = b;
            $('#editModal').modal('open');
        }

        $scope.updateFeedback = function (fd) {
            backlogsServ.putBacklogs(fd)
                .then(function(res){})
            $scope.cancelEdit();  
        }

        $scope.deleteFeedback = function(id) {
            $scope.id = id;
            swal({
                title: 'Atención!',
                text: '¿Está seguro de eliminar la sugerencia?',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            }).
            then(function(res) {
                if(res) {
                    backlogsServ.deleteSonsBacklogs(id)
                    .then(function(res){
                        backlogsServ.deleteBacklog($scope.id)
                            .then(function(res){
                                getBacklogs();
                            })
                    })
                }
            })
        }

        $scope.cancelar = function() {
            $('#feedModal').modal('close');
            $scope.backlog = null;
        }

        $scope.cancelEdit = function() {
            $('#editModal').modal('close');
            $scope.feedDetail = null;
        }

        $scope.cerrarSession = function(){
            generalFunc.cerrarSesion()
        }

    }
])