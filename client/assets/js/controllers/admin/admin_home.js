.controller('homeAdminCtrl', ['$scope', '$state', 'usuariosServ', 'generalFunc', 'programasServ',
  function($scope, $state, usuariosServ, generalFunc, programasServ) {

  loadScopes();

  function loadScopes() {
    generalFunc.routeAdmin();
    loadNumeroUsuarios();
    loadProyectosCerrados();
  }


    function loadNumeroUsuarios() {
      usuariosServ.getNumeroUsuarios()
        .then(function(res){
            $scope.numeroUsuarios = res.data.count;
        });
    }

    function loadProyectosCerrados(res) {
        programasServ.countProyectoCerrados(3)
            .then(function(res){
                $scope.proyectos = res.data.count;
            })
    }

    $scope.cerrarSession = function(){
        generalFunc.cerrarSesion()
    }

  }
])
