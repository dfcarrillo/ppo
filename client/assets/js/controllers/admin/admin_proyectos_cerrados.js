.controller('adminProyectosCerrados', ['$scope','$state', 'generalFunc', 'programasServ',
    function($scope, $state, generalFunc, programasServ) {

        $('.modal').modal();
        $('.collapsible').collapsible();
        $('select').material_select();

        //Definición Ui-Grid
        $scope.tablaOp={
            enableFiltering: true,
            enableRowSelection: true,
            multiSelect: false,
            enableColumnResizing:true,
            enableGridMenu:true,
            showColumnFooter:true,
            showGridFooter: true,
            enableColumnMoving:true,
            exporterExcelFilename: 'proyectos.xlsx',
            exporterExcelSheetName: 'proyectos',
            columnDefs:[
                {field:'codigo_SAP', displayName:'Código SAP', minWidth:110},
                {field:'nombre', displayName:'Nombre', minWidth:280},
                {field:'nombre_locacion', displayName:'Ubicación', minWidth:140},
                {field:'uucc', displayName:'UUCC', minWidth:100},
                {field:'fecha_ingreso', displayName:'Fecha Creación', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\':"UTC"', minWidth:170},
                {field:'fecha_cierre', displayName:'Fecha Cierre', type: 'date', cellFilter: 'date:\'yyyy-MM-dd  h:mma\'', minWidth:170},
                { name: 'Asignar', width:160, headerCellTemplate: '<br><div class ="center">Editar<br> Obras</div>',
                    cellTemplate:'<button class="ui-grid-btn" ng-click="grid.appScope.openAsignarObrasModal(row.entity)"><i class="material-icons right">delete_forever</i>Eliminar</button>' }
            ]
        };

        generalFunc.routeAdmin();
        loadClosedProyecto();

        //Cargue info ui-grid
        function loadClosedProyecto() {
            programasServ.getProyectoCerrados(3, 'proyecto')
                .then(function(res){
                    $scope.tablaOp.data = res.data
                });
        }

    }
])