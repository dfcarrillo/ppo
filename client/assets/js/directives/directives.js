angular.module('app.directives', [])

  .directive('footerHome', [function() {
    return {
      templateUrl: 'templates/footer.html',
    };
  }])

  .directive('headerAdmin', [function() {
    return {
      templateUrl: 'templates/admin/adminHeader.html',
    };
  }])

  .directive('headerCdc', [function() {
    return {
      templateUrl: 'templates/cdc/header_cdc.html',
    };
  }])

  .directive('headerJs', [function() {
    return {
      templateUrl: 'templates/ja/ja_header.html',
    };
  }])

  .directive('contratistaHeader', [function () {
    return {
      templateUrl: 'templates/ec/header_contratista.html'
    };
  }])

  .directive("datepicker", function () {

      function link(scope, element) {
          element.datepicker({
              dateFormat: "yy-mm-dd"
          });
      }
      return {
          require: 'ngModel',
          link: link
      };
  })

  .directive('fileread', [function() {
    return {
      scope: {
        opts: '=',
      },
      link: function($scope, $elm, $attrs) {
        $elm.on('change', function(changeEvent) {
          var reader = new FileReader();

          reader.onload = function(evt) {
            $scope.$apply(function() {
              var data = evt.target.result;

              var workbook = XLSX.read(data, {type: 'binary'});

              workbook.Sheets["Programacion"]['!ref'] = "C20:ET250"; //Formato zona Metropolitana
              workbook.Sheets["Seguimiento"]['!ref'] = "F8:FS250"; // Formato zona metropolitana

              var headerNames = ['Supervisor', 'x', 'Area_servicio', 'x', 'x', 'x', 'Zona', 'x', 'x', 'Codigo',
                'Nombre', 'x', 'x', 'Fecha_Fin', 'x', 'x', 'x', 'x', 'Comentarios', 'x', 'x', 'x', 'x',
                'uucc', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'Sem_Ini', 'Sem_Fin', 'ec1', 'x', 'ec2', 'x',
                'x', 'x', 'x', 'Estado', 'x', 'x', 'x', 'ec1s1', 'ec2s1', 'ec1s2', 'ec2s2',
                'ec1s3', 'ec2s3', 'ec1s4', 'ec2s4', 'ec1s5', 'ec2s5', 'ec1s6', 'ec2s6', 'ec1s7', 'ec2s7',
                'ec1s8', 'ec2s8', 'ec1s9', 'ec2s9', 'ec1s10', 'ec2s10', 'ec1s11', 'ec2s11', 'ec1s12', 'ec2s12',
                'ec1s13', 'ec2s13', 'ec1s14', 'ec2s14', 'ec1s15', 'ec2s15', 'ec1s16', 'ec2s16', 'ec1s17', 'ec2s17',
                'ec1s18', 'ec2s18', 'ec1s19', 'ec2s19', 'ec1s20', 'ec2s20', 'ec1s21', 'ec2s21', 'ec1s22', 'ec2s22',
                'ec1s23', 'ec2s23', 'ec1s24', 'ec2s24', 'ec1s25', 'ec2s25', 'ec1s26', 'ec2s26', 'ec1s27', 'ec2s27',
                'ec1s28', 'ec2s28', 'ec1s29', 'ec2s29', 'ec1s30', 'ec2s30', 'ec1s31', 'ec2s31', 'ec1s32', 'ec2s32',
                'ec1s33', 'ec2s33', 'ec1s34', 'ec2s34', 'ec1s35', 'ec2s35', 'ec1s36', 'ec2s36', 'ec1s37', 'ec2s37',
                'ec1s38', 'ec2s38', 'ec1s39', 'ec2s39', 'ec1s40', 'ec2s40', 'ec1s41', 'ec2s41', 'ec1s42', 'ec2s42',
                'ec1s43', 'ec2s43', 'ec1s44', 'ec2s44', 'ec1s45', 'ec2s45', 'ec1s46', 'ec2s46', 'ec1s47', 'ec2s47',
                'ec1s48', 'ec2s48', 'ec1s49', 'ec2s49', 'ec1s50', 'ec2s50', 'ec1s51', 'ec2s51', 'ec1s52', 'ec2s52'];

              var headersUucc = ['codigo_SAP', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
                'uc1', 'ruc1', 'x', 'uc2', 'ruc2', 'x', 'uc3', 'ruc3', 'x', 'uc4', 'ruc4', 'x', 'uc5', 'ruc5', 'x',
                'uc6', 'ruc6', 'x', 'uc7', 'ruc7', 'x', 'uc8', 'ruc8', 'x', 'uc9', 'ruc9', 'x', 'uc10', 'ruc10', 'x',
                'uc11', 'ruc11', 'x', 'uc12', 'ruc12', 'x', 'uc13', 'ruc13', 'x', 'uc14', 'ruc14', 'x', 'uc15', 'ruc15',
                'x', 'uc16', 'ruc16', 'x', 'uc17', 'ruc17', 'x', 'uc18', 'ruc18', 'x', 'uc19', 'ruc19', 'x', 'uc20',
                'ruc20', 'x', 'uc21', 'ruc21', 'x', 'uc22', 'ruc22', 'x', 'uc23', 'ruc23', 'x', 'uc24', 'ruc24', 'x',
                'uc25', 'ruc25', 'x', 'uc26', 'ruc26', 'x', 'uc27', 'ruc27', 'x', 'uc28', 'ruc28', 'x', 'uc29', 'ruc29',
                'x', 'uc30', 'ruc30', 'x', 'uc31', 'ruc31', 'x', 'uc32', 'ruc32', 'x', 'uc33', 'ruc33', 'x', 'uc34',
                'ruc34', 'x', 'uc35', 'ruc35', 'x', 'uc36', 'ruc36', 'x', 'uc37', 'ruc37', 'x', 'uc38', 'ruc38', 'x',
                'uc39', 'ruc39', 'x', 'uc40', 'ruc40', 'x', 'uc41', 'ruc41', 'x', 'uc42', 'ruc42', 'x', 'uc43', 'ruc43',
                'x', 'uc44', 'ruc44', 'x', 'uc45', 'ruc45', 'x', 'uc46', 'ruc46', 'x', 'uc47', 'ruc47', 'x', 'uc48',
                'ruc48', 'x', 'uc49', 'ruc49', 'x', 'uc50', 'ruc50', 'x', 'uc51', 'ruc51', 'x', 'uc52', 'ruc52', 'x'];

              //Formato zona Bio Bio
              /*workbook.Sheets["Programacion"]['!ref'] = "B21:EO250";
              workbook.Sheets["Seguimiento"]['!ref'] = "F8:FS250";

                var headerNames = ['Supervisor', 'x', 'Area_servicio', 'x', 'x', 'x', 'Zona', 'x', 'x', 'Codigo',
                    'Nombre', 'x', 'x', 'Fecha_Fin', 'x', 'x', 'x', 'x', 'Comentarios', 'x', 'x', 'x', 'x',
                    'uucc', 'x', 'x', 'x', 'x', 'x', 'Sem_Ini', 'Sem_Fin', 'ec1', 'x', 'ec2', 'x',
                    'Estado', 'x', 'x', 'x', 'x', 'ec1s1', 'ec2s1', 'ec1s2', 'ec2s2',
                    'ec1s3', 'ec2s3', 'ec1s4', 'ec2s4', 'ec1s5', 'ec2s5', 'ec1s6', 'ec2s6', 'ec1s7', 'ec2s7',
                    'ec1s8', 'ec2s8', 'ec1s9', 'ec2s9', 'ec1s10', 'ec2s10', 'ec1s11', 'ec2s11', 'ec1s12', 'ec2s12',
                    'ec1s13', 'ec2s13', 'ec1s14', 'ec2s14', 'ec1s15', 'ec2s15', 'ec1s16', 'ec2s16', 'ec1s17', 'ec2s17',
                    'ec1s18', 'ec2s18', 'ec1s19', 'ec2s19', 'ec1s20', 'ec2s20', 'ec1s21', 'ec2s21', 'ec1s22', 'ec2s22',
                    'ec1s23', 'ec2s23', 'ec1s24', 'ec2s24', 'ec1s25', 'ec2s25', 'ec1s26', 'ec2s26', 'ec1s27', 'ec2s27',
                    'ec1s28', 'ec2s28', 'ec1s29', 'ec2s29', 'ec1s30', 'ec2s30', 'ec1s31', 'ec2s31', 'ec1s32', 'ec2s32',
                    'ec1s33', 'ec2s33', 'ec1s34', 'ec2s34', 'ec1s35', 'ec2s35', 'ec1s36', 'ec2s36', 'ec1s37', 'ec2s37',
                    'ec1s38', 'ec2s38', 'ec1s39', 'ec2s39', 'ec1s40', 'ec2s40', 'ec1s41', 'ec2s41', 'ec1s42', 'ec2s42',
                    'ec1s43', 'ec2s43', 'ec1s44', 'ec2s44', 'ec1s45', 'ec2s45', 'ec1s46', 'ec2s46', 'ec1s47', 'ec2s47',
                    'ec1s48', 'ec2s48', 'ec1s49', 'ec2s49', 'ec1s50', 'ec2s50', 'ec1s51', 'ec2s51', 'ec1s52', 'ec2s52'];

                var headersUucc = ['codigo_SAP', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x',
                    'uc1', 'ruc1', 'x', 'uc2', 'ruc2', 'x', 'uc3', 'ruc3', 'x', 'uc4', 'ruc4', 'x', 'uc5', 'ruc5', 'x',
                    'uc6', 'ruc6', 'x', 'uc7', 'ruc7', 'x', 'uc8', 'ruc8', 'x', 'uc9', 'ruc9', 'x', 'uc10', 'ruc10', 'x',
                    'uc11', 'ruc11', 'x', 'uc12', 'ruc12', 'x', 'uc13', 'ruc13', 'x', 'uc14', 'ruc14', 'x', 'uc15', 'ruc15',
                    'x', 'uc16', 'ruc16', 'x', 'uc17', 'ruc17', 'x', 'uc18', 'ruc18', 'x', 'uc19', 'ruc19', 'x', 'uc20',
                    'ruc20', 'x', 'uc21', 'ruc21', 'x', 'uc22', 'ruc22', 'x', 'uc23', 'ruc23', 'x', 'uc24', 'ruc24', 'x',
                    'uc25', 'ruc25', 'x', 'uc26', 'ruc26', 'x', 'uc27', 'ruc27', 'x', 'uc28', 'ruc28', 'x', 'uc29', 'ruc29',
                    'x', 'uc30', 'ruc30', 'x', 'uc31', 'ruc31', 'x', 'uc32', 'ruc32', 'x', 'uc33', 'ruc33', 'x', 'uc34',
                    'ruc34', 'x', 'uc35', 'ruc35', 'x', 'uc36', 'ruc36', 'x', 'uc37', 'ruc37', 'x', 'uc38', 'ruc38', 'x',
                    'uc39', 'ruc39', 'x', 'uc40', 'ruc40', 'x', 'uc41', 'ruc41', 'x', 'uc42', 'ruc42', 'x', 'uc43', 'ruc43',
                    'x', 'uc44', 'ruc44', 'x', 'uc45', 'ruc45', 'x', 'uc46', 'ruc46', 'x', 'uc47', 'ruc47', 'x', 'uc48',
                    'ruc48', 'x', 'uc49', 'ruc49', 'x', 'uc50', 'ruc50', 'x', 'uc51', 'ruc51', 'x', 'uc52', 'ruc52', 'x'];*/

              var data = XLSX.utils.sheet_to_json(workbook.Sheets["Programacion"],  {header: headerNames});
              var uucc = XLSX.utils.sheet_to_json(workbook.Sheets["Seguimiento"],  {header: headersUucc});

              //data.forEach(function(v) { delete v.x; });

              //data = _.filter(data, {'Estado': 'PROGRAMADO'}) //BIO BIO
              data = _.filter(data, {'Estado': 'Programación_completa'}) // San Bernardo
              data = _.filter(data, function(s) { return s.Sem_Fin >= (moment().week() - 1); });
              //data = _.filter(data, function(s) { return s.Sem_Ini <= /*(moment().week() - 1)*/26; });

              $scope.opts.data = data;
              $scope.opts.data.uucc = uucc;

              $elm.val(null);
            });
          };

          reader.readAsBinaryString(changeEvent.target.files[0]);
        });
      },
    };
  }]);
