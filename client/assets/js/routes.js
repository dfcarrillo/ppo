angular.module('app')

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('home', {
        url: '/',
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl',
      })
      .state('perfil_admin', {
        url: '/admin',
        templateUrl: 'templates/admin/home_admin.html',
        controller: 'homeAdminCtrl',
      })
      .state('perfil_admin_usuarios', {
        url: '/admin/usuarios',
        templateUrl: 'templates/admin/usuarios_admin.html',
        controller: 'adminUsuariosCtrl',
      })
      .state('perfil_admin_proyectos_cerrados', {
        url: '/admin/obrascerrados',
        templateUrl: 'templates/admin/proyectos_cerrados.html',
        controller: 'adminProyectosCerrados',
      })
      .state('perfil_admin_backlog', {
        url: '/admin/backlog/:id',
        templateUrl: 'templates/admin/admin_backlog.html',
        controller: 'adminBacklogCtrl',
      })
      .state('cambiar_passwd', {
        url: '/cambiar_contrasena?access_token',
        templateUrl: 'templates/admin/admin_reset_pass.html',
        controller: 'adminResetPassCtrl',
      })
      .state('perfil_cdc', {
        url: '/cdc',
        templateUrl: 'templates/cdc/cdc_home.html',
        controller: 'cdcHomeCtrl',
      })
      .state('perfil_cdc_crear', {
        url: '/cdc/CrearProyectos',
        templateUrl: 'templates/cdc/cdc_crear.html',
        controller: 'cdcCrearCtrl',
      })
      .state('perfil_cdc_asignar', {
        url: '/cdc/AsignarProyectos',
        templateUrl: 'templates/cdc/cdc_asignar.html',
        controller: 'cdcAsignarCtrl',
      })
      .state('perfil_cdc_editar', {
        url: '/cdc/EditarProyectos',
        templateUrl: 'templates/cdc/cdc_editar.html',
        controller: 'cdcEditarCtrl',
      })
      .state('perfil_js', {
        url: '/ja',
        templateUrl: 'templates/ja/ja_home.html',
        controller: 'jsHomeCtrl',
      })
      .state('perfil_js_propuesta', {
        url: '/ja/propuesta',
        templateUrl: 'templates/ja/ja_propuesta.html',
        controller: 'jsPropuestaCtrl',
      })
      .state('perfil_js_asignaGP', {
        url: '/ja/asignarGP',
        templateUrl: 'templates/ja/ja_asignaGP.html',
        controller: 'jsAsignarGPCtrl',
      })
      .state('perfil_js_cambiaGP', {
        url: '/ja/cambiarGP',
        templateUrl: 'templates/ja/ja_cambiaGP.html',
        controller: 'jsCambiarGPCtrl',
      })
      .state('perfil_js_trabajos', {
        url: '/ja/trabajos',
        templateUrl: 'templates/ja/ja_asigna_trabajos.html',
        controller: 'jsAsignaTrabajosCtrl',
      })
      .state('perfil_js_obras', {
        url: '/ja/obras',
        templateUrl: 'templates/ja/ja_detalle_obra.html',
        controller: 'jsDetalleObrasCtrl',
      })
      .state('perfil_js_compromiso', {
        url: '/ja/compromisos',
        templateUrl: 'templates/ja/ja_crea_compromiso.html',
        controller: 'jsCompromisoCtrl',
      })
      .state('perfil_js_valida', {
        url: '/ja/validar',
        templateUrl: 'templates/ja/ja_valida_compromiso.html',
        controller: 'jsValidarCtrl',
      })
      .state('perfil_gp', {
        url: '/gp',
        templateUrl: 'templates/gp/gestion_gp.html',
        controller: 'gestionGpCtrl',
      })
      .state('perfil_ec', {
        url: '/ec',
        templateUrl: 'templates/ec/gestion_ec.html',
        controller: 'gestionEcCtrl',
      })
      .state('programador_ec', {
          url: '/ec/programador',
          templateUrl: 'templates/ec/programador_ec.html',
          controller: 'programarEcCtrl'
      })
      .state('programador_cdc', {
          url: '/cdc/programador',
          templateUrl: 'templates/cdc/programador_cdc.html',
          controller: 'programarCdcCtrl'
      })
      .state('backlog', {
        url: '/backlog/:id',
        templateUrl: 'templates/general/backlog.html',
        controller: 'backlogCtrl',
      });

    $urlRouterProvider.otherwise('/');
  });
