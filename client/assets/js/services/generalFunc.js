.factory('generalFunc', ['$state', 'usuariosServ',
    function($state, usuariosServ){

        //-------------------------Funciones Generales Retornos sincronos---------------------------//

        function routeAdmin() {
            if (window.localStorage['rol'] == "admin") {
                //$state.go("perfil_admin");
            } else if (window.localStorage['rol'] == 'cdc') {
                $state.go("perfil_cdc");
            } else if (window.localStorage['rol'] == 'js') {
                $state.go("perfil_js");
            } else if (window.localStorage['rol'] == 'gp'){
                $state.go("perfil_gp");
            } else if (window.localStorage['rol'] == 'eecc'){
                $state.go("perfil_ec");
            } else {
                $state.go("home");
            }
        }

        function routeCdc() {
            if (window.localStorage['rol'] == "admin") {
                $state.go("perfil_admin");
            } else if (window.localStorage['rol'] == 'cdc') {
                //$state.go("perfil_cdc");
            } else if (window.localStorage['rol'] == 'js') {
                $state.go("perfil_js");
            } else if (window.localStorage['rol'] == 'gp'){
                $state.go("perfil_gp");
            } else if (window.localStorage['rol'] == 'eecc'){
                $state.go("perfil_ec");
            } else {
                $state.go("home");
            }
        }

        function routeJs() {
            if (window.localStorage['rol'] == "admin") {
                $state.go("perfil_admin");
            } else if (window.localStorage['rol'] == 'cdc') {
                $state.go("perfil_cdc");
            } else if (window.localStorage['rol'] == 'js') {
                //$state.go("perfil_js");
            } else if (window.localStorage['rol'] == 'gp'){
                $state.go("perfil_gp");
            } else if (window.localStorage['rol'] == 'eecc'){
                $state.go("perfil_ec");
            } else {
                $state.go("home");
            }
        }

        //Evaluar el estado de un proyecto dado las fechas de propuesta
        function evaluarEstadoObra(est) {
            var estado;
            if (est == 0){
                estado = 'Sin Propuesta';
            }else if(est == 1){
                estado = 'Proyecto Sin GO';
            }else if(est == 2){
                estado = 'Con Propuesta';
            }else if(est == 3){
                estado = 'En Ejecución';
            }else{
                estado = 'Ejecutada';
            }
            return estado;
        }

        //Retorna el nombre del estado de una obra
        function getEstadoObra(estado){
            var nombre;
            if (estado == 0) {
                nombre = 'Sin Programar';
            }else if(estado == 1) {
                nombre = 'Programada';
            }else if(estado == 2) {
                nombre = 'Contrapropuesta';
            }else if(estado == 3) {
                nombre = 'Por Ejecutar'
            }else if(estado == 4){
                nombre = 'En Ejecución';
            }else{
                nombre = 'Ejecutada'
            }
            return nombre;
        }

        //Retorna el nombre del estado de una obra
        function getEstadoTrabajo(estado){
            var nombre;
            if (estado == 0) {
                nombre = 'Programado';
            }else if(estado == 1) {
                nombre = 'En Ejecución';
            }else if(estado == 2) {
                nombre = 'En Liquidación';
            }else if(estado == 3) {
                nombre = 'En Facturación'
            }else{
                nombre = 'Ejecutado'
            }
            return nombre;
        }

        //Funcion que retorna un string "procesable" de la fecha que retorna mysql
        function getFormatDate(fecha){
            var x = new Date(fecha);
            var anio = x.getFullYear();
            var mes = (x.getMonth()<10?'0':'') + (x.getMonth()+1)
            var day = (x.getDate()<10?'0':'') + (x.getDate())
            var hora = (x.getHours()<10?'0':'') + (x.getHours())
            var mins = (x.getMinutes()<10?'0':'') + (x.getMinutes())
            var date = anio+'-'+mes+'-'+day+' '+hora+':'+mins;

            return date
        }

        //Funcion para asignar maxima hora del dia a una fecha
        function getMaxHour(fecha){
            var date = moment(fecha).format('YYYY-MM-DD')
            var hora = " 23:59:59";
            var maxFecha = moment(date + hora).toDate();
            return maxFecha;
        }

        //funcion que construye el JSON de las fases de un determinado trabajo
        function buildJsonFases(trabajo){
            var fases = [], ejecucion = materiales = pago = facturacion = {};
            ejecucion = {"nombre": "Ejecución", "estado_programa": "2"}; materiales = {"nombre": "Cuadratura de Materiales"};
            pago = {"nombre": "Estado de Pago"}; facturacion = {"nombre": "Facturación"};
            ejecucion.tipo = materiales.tipo = pago.tipo = facturacion.tipo = 'fase';
            ejecucion.id_raiz = materiales.id_raiz = pago.id_raiz = facturacion.id_raiz = trabajo.id;
            ejecucion.id_locacion = materiales.id_locacion = pago.id_locacion = facturacion.id_locacion = trabajo.id_locacion;
            ejecucion.nombre_locacion = materiales.nombre_locacion = pago.nombre_locacion = facturacion.nombre_locacion = trabajo.nombre_locacion;
            ejecucion.id_usuario_creacion = materiales.id_usuario_creacion = pago.id_usuario_creacion = facturacion.id_usuario_creacion = trabajo.id_usuario_creacion;
            ejecucion.id_js_asignado = materiales.id_js_asignado = pago.id_js_asignado = facturacion.id_js_asignado = trabajo.id_js_asignado;
            ejecucion.id_gp_asignado = materiales.id_gp_asignado = pago.id_gp_asignado = facturacion.id_gp_asignado = trabajo.id_gp_asignado;
            ejecucion.id_eecc_asignado = materiales.id_eecc_asignado = pago.id_eecc_asignado = facturacion.id_eecc_asignado = trabajo.id_eecc_asignado;
            ejecucion.area_servicio = materiales.area_servicio = pago.area_servicio = facturacion.area_servicio = trabajo.area_servicio;
            fases.push(ejecucion); fases.push(facturacion); fases.push(pago);
            if(trabajo.materiales == '0'){
                fases.push(materiales)
            }
            return fases;
        }

        function cerrarSesion(){
            usuariosServ.logout()
                .then(function(res) {
                    $state.go("home");
                })
        }

        return{
            routeAdmin: routeAdmin,
            routeCdc: routeCdc,
            routeJs: routeJs,
            evaluarEstadoObra: evaluarEstadoObra,
            getEstadoObra: getEstadoObra,
            getEstadoTrabajo: getEstadoTrabajo,
            getFormatDate: getFormatDate,
            getMaxHour: getMaxHour,
            buildJsonFases: buildJsonFases,
            cerrarSesion: cerrarSesion
        }

    }])
