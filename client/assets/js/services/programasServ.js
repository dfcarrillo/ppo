.factory('programasServ', function ($http){
  var urlProgramas =  "/api/programas";

  //Get todos los programas por id_raiz (programas pertenecientes a un nivel superior)
  function getProgramasById(id){
      var token = window.localStorage.token;
      var header = {headers: {'Authorization': token}};
      var url = urlProgramas + '?filter={"where":{"id":' + id + '},"include":["sons","parent"]}';
      return $http.get(url, header);
  }

  //Get todos los programas por id_raiz (programas pertenecientes a un nivel superior)
  function getProgramasByIdRaiz(id_raiz){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"id_raiz":' + id_raiz + '},"include":["sons","parent"]}';
    return $http.get(url, header);
  }

  //Get todos los programas por id_raiz (programas pertenecientes a un nivel superior)
  function getProgramasByCodSap(cod_sap){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"codigo_SAP":"' + cod_sap + '"},"include":["sons"]}';
    return $http.get(url, header);
  }

    /**
     * SERVICIOS PERFIL ADMIN
     */
  //Get Proyectos cerrados
  function getProyectoCerrados (est){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"estado_programa":'+est+'},{"tipo":"proyecto"}]},"include":["sons"]}';
    return $http.get(url, header)
  }

  //Count proyectos cerrados
  function countProyectoCerrados (est) {
      var token = window.localStorage.token;
      var header = {headers: {'Authorization': token}};
      var url = urlProgramas + '/count?where={"estado_programa":'+est+',"tipo":"proyecto"}';
      return $http.get(url, header);
  }
    /**
     * SERVICIOS PERFIL CDC
     */
  /*Count proyecto en general por id_user_creacion, estado_programa.
  Aplica para CDC porque filtra por user de creacion
  PERFIL:CDC Cuenta unicamente proyecto creados por el cdc 'user'
  */
  function countProyectoByCDCEstado (cdc, est) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '/count?where={"id_usuario_creacion":'+cdc+',"estado_programa":'+est+',"tipo":"proyecto"}';
    return $http.get(url, header);
  }

  //get  Programa by id_usuario_creacion, estado_programa. Include sons
  //PERFIL:CDC Obtiene el proyecto del cdc 'usr' con estado 'est'
  function getProyectoByCDCEstado (cdc, est){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_usuario_creacion":'+cdc+'},{"estado_programa":'+est+'},{"tipo":"proyecto"}]},"include":["sons"]}';
    return $http.get(url, header)
  }

  //get  Programa by id_usuario_creacion, estado_programa, tipo. Include js y sons:js sons:ec
  //PERFIL:CDC Obtiene el proyecto del cdc 'usr' con estado 'est'
  function getProyectoByCDCEstado2 (cdc, est){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter[where][id_usuario_creacion]='+cdc+'&filter[where][estado_programa]='+est+'&filter[where][tipo]=proyecto&filter[include][sons]=js&filter[include][sons]=eecc';
    return $http.get(url, header)
  }

    /**
     * SERVICIOS PERFIL JS
     */
  //Count programa en general dado el id_js, estado y tipo
  function countProgramaByJSEstadoTipo (js, est, tipo) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '/count?where={"id_js_asignado":'+js+',"estado_programa":'+est+',"tipo":"'+tipo+'"}';
    return $http.get(url, header);
  }

  //getPrograma dado js, estado, tipo incluye eecc
  function getProgramaByJSEstadoTipo(js, est, tipo){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_js_asignado":'+js+'},{"estado_programa":'+est+'},{"tipo":"'+tipo+'"}]},"include":["eecc","sons"]}';
    return $http.get(url, header)
  }

  //getPrograma dado js, estado, tipo, incluye pasos, sons y eecc
  function getProgramaByJSEstadoTipo2 (js, est, tipo){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_js_asignado":'+js+'},{"estado_programa":'+est+'},{"tipo":"'+tipo+'"}]},"include":["parent","sons","pasos","eecc","js"]}';
    return $http.get(url, header)
  }

    //getPrograma dado js, estado, tipo, incluye pasos, parent y eecc
  function getProgramaByJSEstadoTipo3(id_js, estado, tipo){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '?filter={"where":{"and":[{"id_js_asignado":'+id_js+'},{"estado_programa":'+estado+'},{"tipo":"'+tipo+'"}]},"include":["parent","pasos","eecc","gp"]}';
    return $http.get(url, header)
  }

    /**
     * SERVICIOS COMUNES CDC Y JS
     */

  //Post un programa, Función general para crear un programa (proyecto, obra, trabajo o fase, etc)
  function postPrograma(programa){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.post(urlProgramas, programa, header)
  }

  //put programa
  function putPrograma(programa){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.put(urlProgramas, programa, header)
  }
  //Update un programa. Funcion para actualizar un programa dado el id
  function updatePrograma(programa){
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = urlProgramas + '/update?where={"id": '+ programa.id+ '}';
    return $http.post(url, programa, header)
  }


  return{
    getProgramasById: getProgramasById,
    getProgramasByIdRaiz: getProgramasByIdRaiz,
    getProgramasByCodSap: getProgramasByCodSap,
    getProyectoCerrados: getProyectoCerrados,
    countProyectoCerrados: countProyectoCerrados,
    countProyectoByCDCEstado: countProyectoByCDCEstado,
    getProyectoByCDCEstado: getProyectoByCDCEstado,
    getProyectoByCDCEstado2: getProyectoByCDCEstado2,
    countProgramaByJSEstadoTipo: countProgramaByJSEstadoTipo,
    getProgramaByJSEstadoTipo: getProgramaByJSEstadoTipo,
    getProgramaByJSEstadoTipo2: getProgramaByJSEstadoTipo2,
    getProgramaByJSEstadoTipo3: getProgramaByJSEstadoTipo3,
    postPrograma: postPrograma,
    putPrograma: putPrograma,
    updatePrograma: updatePrograma
  }

})
