.factory('homeServ', function($http){

  //función para cargar roles del JSON
  function addModel(model, objs){
    var url = "/api/" + model;
    return $http.post(url, objs);
  }
  return{
    addModel: addModel
  }
})
