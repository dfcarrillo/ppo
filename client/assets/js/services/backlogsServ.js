.factory('backlogsServ', function($http){
    var url = "/api/backlogs";

    function getBacklogs() {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_b = url + '?filter={"where":{"id_backlog":"0"},"include":["sons"]}'
        return $http.get(url_b,header);
    }

    function getBacklogsById(id) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_b = url + '/' + id + '?filter={"include":["sons","user"]}'
        return $http.get(url_b,header);
    }

    function postBacklogs(backlog) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        return $http.post(url, backlog, header);
    }

    function putBacklogs(backlog) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        return $http.put(url, backlog, header);
    }

    function deleteBacklog(id) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_d = url + '/' + id;
        return $http.delete(url_d, header);
    }

    function deleteSonsBacklogs(id) {
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url_d = url + '/' + id + '/sons';
        return $http.delete(url_d, header);
    }

    return {
        getBacklogs: getBacklogs,
        getBacklogsById: getBacklogsById,
        postBacklogs: postBacklogs,
        putBacklogs: putBacklogs,
        deleteBacklog: deleteBacklog,
        deleteSonsBacklogs: deleteSonsBacklogs
    }
})
