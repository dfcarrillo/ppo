.factory('pasosServ', function ($http) {
    var urlPasos = "/api/pasos";

    function getPasosByIdPrograma(id_programa){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url = urlPasos + "?filter[where][id_programa]=" + id_programa;
        return $http.get(url, header);
    }


    function postPaso(paso){
      var token = window.localStorage.token;
      var header = {headers: {'Authorization': token}};
      return $http.post(urlPasos, paso, header);
    }

    return {
        getPasosByIdPrograma: getPasosByIdPrograma,
        postPaso: postPaso
    }


  }
)
