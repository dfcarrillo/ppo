.factory('gestionServ', function ($http){
    var api = '/api';
    var etapatipo="obra";
    var asignatipo="asignacion";

    function getFasesEstadoEc(est,ec){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas?filter={"include":["parent","pasos","js","gp"], "where":{"tipo":"fase", "estado_programa":'+est+', "id_eecc_asignado":'+ec+'}}';
        return $http.get(url, header);
    }

    function getFasesEstadoGp(est,gp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas?filter={"include":["parent","pasos","js","eecc"], "where":{"tipo":"fase", "estado_programa":'+est+', "id_gp_asignado":'+gp+'}}';
        return $http.get(url, header);
    }

    function getFasesEc(ec){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + '/programas?filter={"include":["parent","pasos","js","gp"], "where":{"tipo":"fase", "id_eecc_asignado":'+ec+'}}';
        return $http.get(url, header);
    }

    function getEtapasEc(ec){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + '/programas?filter={"include":["parent","sons"], "where":{"tipo":"'+etapatipo+'", "id_eecc_asignado":'+ec+'}}';
        return $http.get(url, header);
    }

    function getAsignaciones(){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + '/programas?filter={"include":["parent","sons","eecc"], "where":{"tipo":"'+asignatipo+'"}}';
        return $http.get(url,header);
    }

    function postPrograma(fase){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url=api + "/programas";
        return $http.post(url, fase, header);
    }

    function putPrograma(fase){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url = api + "/programas";
        return $http.put(url, fase, header);
    }

    function postPaso(paso){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url = api + "/pasos";
        return $http.post(url,paso, header);
    }

    function getProgramaMedioId(idp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas/'+idp+'?filter={"include":["parent","pasos","sons"]}';
        return $http.get(url, header);
    }

    function getProyecto(idp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/programas/'+idp+'?filter={"include":["sons"]}';
        return $http.get(url, header);
    }

    function getCausas(){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/causas_reprogs';
        return $http.get(url, header);
    }

    function getPasosPrograma(idp){
        var token = window.localStorage.token;
        var header = {headers: {'Authorization': token}};
        var url= api + '/pasos?filter={"where":{"id_programa":'+idp+'}}';
        return $http.get(url, header);
    }

    return{
        getFasesEstadoEc: getFasesEstadoEc,
        getFasesEstadoGp: getFasesEstadoGp,
        putPrograma: putPrograma,
        postPaso: postPaso,
        getProgramaMedioId: getProgramaMedioId,
        getProyecto: getProyecto,
        getCausas: getCausas,
        getPasosPrograma: getPasosPrograma,
        getFasesEc: getFasesEc,
        getEtapasEc: getEtapasEc,
        postPrograma: postPrograma,
        getAsignaciones: getAsignaciones
    }

})

