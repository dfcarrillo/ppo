.factory('usuariosServ', function ($http, endpoint){
  var api_usuarios = "/api/users";
  var api_roles = "/api/roles"

  //----USUARIOS----//

  //función para obtener login de usuario
  function login(username, password){
      var api_user = api_usuarios + '/login';
      var login = {username: username, password: password};
      return $http.post(api_user, login);
  }

  //función para logout (elimina el token)
  function logout(){
    var token = window.localStorage.token;
    var api_logout = api_usuarios + '/logout?access_token=' + token;
    return $http.post(api_logout);
  }

  // get de todos los usuarios
  function getUsuarios() {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + "?filter[include]=role";
    return $http.get(url, header);
  }

  // get Usuario by Id (con role)
  function getUsuarioById (id) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + "/" + id + "?filter[include]=role";
    return $http.get(url, header);
  }

  //count número de usuarios creados
  function getNumeroUsuarios() {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + "/count"
    return $http.get(url, header);
  }

  // Post de un usuario
  function postUsuario(user) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.post(api_usuarios, user, header);
  }

  // Delete User
  function deleteUser(id) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_usuarios + '/' + id;
    return $http.delete(url, header);
  }

  // Update User
  function updateUser(user) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.put(api_usuarios, user, header);
  }

  //request reset passwd
  function resetPasswd(email) {
    var url = api_usuarios + '/reset'
    return $http.post(url, email)
  }

  // Change Passwd
  function changePasswd(newPassword) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var data = { 'newPassword': newPassword }
    var url = api_usuarios + '/reset-password'
    return $http.post(url, data, header)
  }

  //----ROLES----//

  //get de todos los roles
  function getRoles() {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    return $http.get(api_roles, header);
  }

  //get id del rol por su nombre include users
  function getRolByName(name) {
    var token = window.localStorage.token;
    var header = {headers: {'Authorization': token}};
    var url = api_roles + '?filter={"where":{"name": "' + name + '"}, "include":["user"]}';
    return $http.get(url, header);
  }


  return{
    login: login,
    logout: logout,
    getUsuarios: getUsuarios,
    getUsuarioById: getUsuarioById,
    getNumeroUsuarios: getNumeroUsuarios,
    postUsuario: postUsuario,
    deleteUser: deleteUser,
    updateUser: updateUser,
    resetPasswd: resetPasswd,
    changePasswd: changePasswd,
    getRoles: getRoles,
    getRolByName: getRolByName
  }
})
