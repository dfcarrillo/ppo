.factory('generalServ', function($http){
  var api_url_siver = "http://contratistassendas.us-west-2.elasticbeanstalk.com/api";

  //obtiene locaciones de SIVER
  function getLocaciones() {
    var url = api_url_siver + "/localizaciones";
    return $http.get(url);
  }

  //Obtiene locaciones de SIVER por id
  function getLocacionesbyId(id) {
    var url = api_url_siver + "/localizaciones/" + id;
    return $http.get(url);
  }

  function getBrigadesContratista(idc){

    var url=api_url_siver + '/brigades?filter={"include":["servicio","localizacion"],"where":{"id_contratista":'+idc+'}}';
    return $http.get(url);

  }

  function getContratistas() {
    var url = api_url_siver + '/contratistas';
    return $http.get(url)
  }

    function getBrigade(idb){
      var url=api_url_siver +'/brigades/'+idb+'?filter={"include":["servicio","localizacion"]}';
      return $http.get(url);
    }

  return{
    getLocaciones: getLocaciones,
    getLocacionesbyId: getLocacionesbyId,
    getBrigadesContratista: getBrigadesContratista,
    getContratistas: getContratistas,
    getBrigade: getBrigade
  }
})
